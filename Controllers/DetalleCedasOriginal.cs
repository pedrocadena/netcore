using Codium.Web.Models;

using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Codium.Web.Controllers
{
    /// <summary>
    /// test
    /// </summary>
    [ApiExplorerSettings(IgnoreApi = true)]
    public class DetalleCedasOriginal : Controller
    {
#region Variables Privadas
        /// <summary>
        /// Web Host Environment
        /// </summary>        
        private readonly IWebHostEnvironment _env;
        /// <summary>
        /// Razor View To StringRenderer
        /// </summary>
        private readonly IRazorViewToStringRenderer _renderer;
        /// <summary>
        /// Objeto para encriptar y desencriptar valores
        /// </summary>
        private readonly IDataProtector _protector;
        /// <summary>
        /// Acceso a la configuración de la aplicación
        /// </summary>
        private readonly Microsoft.Extensions.Configuration.IConfiguration _configuracion;
        /// <summary>
        /// Variable con el idioma de la aplicación
        /// </summary>
        private readonly Microsoft.Extensions.Localization.IStringLocalizer<PruebaDeExcel.RecursosCompartidos> _idioma;
        /// <summary>
        /// Opciones de Idioma
        /// </summary>
        private readonly Microsoft.Extensions.Options.IOptions<Microsoft.AspNetCore.Builder.RequestLocalizationOptions> _opcionesDeIdioma;
        /// <summary>
        /// MySQL®
        /// </summary>
        private readonly Codium.Web.Models.Mysqlr _mysqlrConexion;
        /// <summary>
        /// MySQL®
        /// </summary>
        private readonly Codium.Web.Models.IMysqlrUnitOfWork _mysqlr;
        /// <summary>
        /// SQLite
        /// </summary>
        private readonly Codium.Web.Models.Sqlite _sqliteConexion;
        /// <summary>
        /// SQLite
        /// </summary>
        private readonly Codium.Web.Models.ISqliteUnitOfWork _sqlite;
#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name = "env">Web Host Environment</param>
        /// <param name = "renderer">Razor View To String Renderer</param>
        /// <param name = "protector">Objeto para encriptar y desencriptar valores</param>
        /// <param name = "configuracion">Acceso a la configuración de la aplicación</param>
        /// <param name = "idioma">Variable con el idioma de la aplicación</param>
        /// <param name = "opcionesDeIdioma">Opciones de Idioma</param>
        /// <param name = "mysqlrConexion">MySQL®</param>
        /// <param name = "mysqlr">MySQL®</param>
        /// <param name = "sqliteConexion">SQLite</param>
        /// <param name = "sqlite">SQLite</param>
        public DetalleCedasOriginal(IWebHostEnvironment env, IRazorViewToStringRenderer renderer, IDataProtectionProvider protector, Microsoft.Extensions.Configuration.IConfiguration configuracion, Microsoft.Extensions.Localization.IStringLocalizer<PruebaDeExcel.RecursosCompartidos> idioma, Microsoft.Extensions.Options.IOptions<Microsoft.AspNetCore.Builder.RequestLocalizationOptions> opcionesDeIdioma, Codium.Web.Models.Mysqlr mysqlrConexion, Codium.Web.Models.IMysqlrUnitOfWork mysqlr, Codium.Web.Models.Sqlite sqliteConexion, Codium.Web.Models.ISqliteUnitOfWork sqlite)
        {
            this._env = env;
            this._renderer = renderer;
            this._protector = protector.CreateProtector("URLProtection");
            this._configuracion = configuracion;
            this._idioma = idioma;
            this._opcionesDeIdioma = opcionesDeIdioma;
            this._mysqlrConexion = mysqlrConexion;
            this._mysqlr = mysqlr;
            this._sqliteConexion = sqliteConexion;
            this._sqlite = sqlite;
        }

#endregion
#region Métodos
        /// <summary>
        /// Guardar Cedas Original
        /// </summary>
        /// <param name = "identificador">Identificador</param>
        /// <returns>Json</returns>
        [Route("DetalleCedasOriginal/GuardarCedasOriginal/{identificador}")]
        [HttpPost]
        public IActionResult GuardarCedasOriginal(string identificador)
        {
            try
            {
                System.Text.StringBuilder resultado = new System.Text.StringBuilder();
                resultado.Append("{");
                resultado.Append("\"guardarCedasOriginal\":{");
                Codium.Web.Models.MysqlrEntidades.CedasOriginal tablaGuardarCedasOriginal1 = null;
                using (var transaccion = new System.Transactions.TransactionScope())
                {
                    bool esNuevoTablaGuardarCedasOriginal1 = true;
                    long llavePrimaria = PruebaDeExcel.Funciones.TryParse<long>((identificador != null && identificador != "" && identificador != "_" ? _protector.Unprotect(identificador) : ""));
                    tablaGuardarCedasOriginal1 = this._mysqlrConexion.CedasOriginales.FirstOrDefault(x => x.LlavePrimaria == llavePrimaria);
                    if (tablaGuardarCedasOriginal1 == null)
                    {
                        tablaGuardarCedasOriginal1 = new Codium.Web.Models.MysqlrEntidades.CedasOriginal();
                        this._mysqlrConexion.CedasOriginales.Add(tablaGuardarCedasOriginal1);
                    }
                    else
                    {
                        esNuevoTablaGuardarCedasOriginal1 = false;
                    }

                    if (esNuevoTablaGuardarCedasOriginal1)
                    {
                        string idCedaSegalmexUnico = PruebaDeExcel.Funciones.TryParse<string>(Request.Form["editarIdCedaSegalmexCedasOriginalDetalleCedasOriginal"]);
                        if (this._mysqlrConexion.CedasOriginales.Count(x => x.IdCedaSegalmex == idCedaSegalmexUnico) > 0)
                        {
                            throw new Exception("NotUnique");
                        }
                    }

                    tablaGuardarCedasOriginal1.DapTon = PruebaDeExcel.Funciones.TryParse<long?>(Request.Form["editarDapTonCedasOriginalDetalleCedasOriginal"]);
                    tablaGuardarCedasOriginal1.UreaTon = PruebaDeExcel.Funciones.TryParse<long?>(Request.Form["editarUreaTonCedasOriginalDetalleCedasOriginal"]);
                    tablaGuardarCedasOriginal1.ClaveCedasSader = PruebaDeExcel.Funciones.TryParse<string>(Request.Form["editarClaveCedasSaderCedasOriginalDetalleCedasOriginal"]);
                    tablaGuardarCedasOriginal1.Estado = PruebaDeExcel.Funciones.TryParse<string>(Request.Form["editarEstadoCedasOriginalDetalleCedasOriginal"]);
                    tablaGuardarCedasOriginal1.IdCedaSegalmex = PruebaDeExcel.Funciones.TryParse<string>(Request.Form["editarIdCedaSegalmexCedasOriginalDetalleCedasOriginal"]);
                    tablaGuardarCedasOriginal1.RegionSegalmex = PruebaDeExcel.Funciones.TryParse<string>(Request.Form["editarRegionSegalmexCedasOriginalDetalleCedasOriginal"]);
                    if (esNuevoTablaGuardarCedasOriginal1)
                    {
                        tablaGuardarCedasOriginal1.Estatus = 1;
                    }

                    if (esNuevoTablaGuardarCedasOriginal1)
                    {
                        tablaGuardarCedasOriginal1.FechaDeCreacion = PruebaDeExcel.Funciones.TryParse<System.DateTime>(System.DateTime.Now);
                    }

                    if (!esNuevoTablaGuardarCedasOriginal1)
                    {
                        tablaGuardarCedasOriginal1.FechaDeModificacion = PruebaDeExcel.Funciones.TryParse<System.DateTime?>(System.DateTime.Now);
                    }

                    this._mysqlrConexion.SaveChanges();
                    resultado.Append("\"tablaGuardarCedasOriginal1\":");
                    resultado.Append(Newtonsoft.Json.JsonConvert.SerializeObject(tablaGuardarCedasOriginal1));
                    transaccion.Complete();
                }

                resultado.Append("}");
                resultado.Append("}");
                return Content(resultado.ToString(), "application/json");
            }
            catch (Exception ex)
            {
                return StatusCode(505, ex.Message);
            }
        }
#endregion
    }
}