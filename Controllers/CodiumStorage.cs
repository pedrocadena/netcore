using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Threading.Tasks;
namespace Codium.Web.Controllers
{
    /// <summary>
    /// codium
    /// </summary>
    [ApiExplorerSettings(IgnoreApi = true)]
    public class CodiumStorage : Controller
    {
#region Variables Privadas
        /// <summary>
        /// Web Host Environment
        /// </summary>        
        private readonly IWebHostEnvironment _env;
        /// <summary>
        /// Razor View To StringRenderer
        /// </summary>
        private readonly IRazorViewToStringRenderer _renderer;
        /// <summary>
        /// Objeto para encriptar y desencriptar valores
        /// </summary>
        private readonly IDataProtector _protector;
        /// <summary>
        /// Acceso a la configuración de la aplicación
        /// </summary>
        private readonly Microsoft.Extensions.Configuration.IConfiguration _configuracion;
        /// <summary>
        /// Variable con el idioma de la aplicación
        /// </summary>
        private readonly Microsoft.Extensions.Localization.IStringLocalizer<PruebaDeExcel.RecursosCompartidos> _idioma;
        /// <summary>
        /// Opciones de Idioma
        /// </summary>
        private readonly Microsoft.Extensions.Options.IOptions<Microsoft.AspNetCore.Builder.RequestLocalizationOptions> _opcionesDeIdioma;
#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name = "env">Web Host Environment</param>
        /// <param name = "renderer">Razor View To String Renderer</param>
        /// <param name = "protector">Objeto para encriptar y desencriptar valores</param>
        /// <param name = "configuracion">Acceso a la configuración de la aplicación</param>
        /// <param name = "idioma">Variable con el idioma de la aplicación</param>
        /// <param name = "opcionesDeIdioma">Opciones de Idioma</param>
        public CodiumStorage(IWebHostEnvironment env, IRazorViewToStringRenderer renderer, IDataProtectionProvider protector, Microsoft.Extensions.Configuration.IConfiguration configuracion, Microsoft.Extensions.Localization.IStringLocalizer<PruebaDeExcel.RecursosCompartidos> idioma, Microsoft.Extensions.Options.IOptions<Microsoft.AspNetCore.Builder.RequestLocalizationOptions> opcionesDeIdioma)
        {
            this._env = env;
            this._renderer = renderer;
            this._protector = protector.CreateProtector("URLProtection");
            this._configuracion = configuracion;
            this._idioma = idioma;
            this._opcionesDeIdioma = opcionesDeIdioma;
        }

#endregion
#region Métodos
        /// <summary>
        /// Función que recupera las subcarpetas de un folder.
        /// </summary>
        /// <param name = "carpeta">Variable con el folder a buscar</param>
        /// <param name = "codiumStorage">Conexión</param>
        /// <returns>string</returns>
        private string CarpetasAnidadas(string carpeta, Azure.Storage.Blobs.BlobContainerClient codiumStorage)
        {
            System.Text.StringBuilder resultado = new System.Text.StringBuilder();
            foreach (Azure.Page<Azure.Storage.Blobs.Models.BlobHierarchyItem> blobsByHierarchy in codiumStorage.GetBlobsByHierarchy(prefix: carpeta, delimiter: "/").AsPages())
            {
                var lista = blobsByHierarchy.Values.Where(x => x.IsPrefix).ToList();
                foreach (var elemento in lista)
                {
                    string prefix = elemento.Prefix.Substring(0, elemento.Prefix.LastIndexOf("/"));
                    resultado.Append("{");
                    resultado.Append("\"name\":" + Newtonsoft.Json.JsonConvert.SerializeObject(prefix.Substring(prefix.LastIndexOf("/") + 1)) + ",\"expanded\":true,");
                    resultado.Append("\"children\":[");
                    resultado.Append(CarpetasAnidadas(elemento.Prefix, codiumStorage));
                    resultado.Append("]");
                    resultado.Append("},");
                }

                if (lista.Count > 0)
                {
                    resultado = new System.Text.StringBuilder(resultado.ToString(0, resultado.Length - 1));
                }
            }

            return resultado.ToString();
        }

        /// <summary>
        /// Función que borrar carpetas o archivos Recursive
        /// </summary>
        /// <param name = "toDelete">Listado de archivos o carpetas a borrar</param>
        /// <param name = "codiumStorage">Conexión</param>
        public void BorrarRecursive(string toDelete, Azure.Storage.Blobs.BlobContainerClient codiumStorage)
        {
            foreach (Azure.Page<Azure.Storage.Blobs.Models.BlobHierarchyItem> blobsByHierarchy in codiumStorage.GetBlobsByHierarchy(prefix: toDelete, delimiter: "/").AsPages())
            {
                var listaBlobs = blobsByHierarchy.Values.ToList();
                foreach (var elementoBlob in listaBlobs)
                {
                    if (elementoBlob.IsPrefix)
                    {
                        BorrarRecursive(elementoBlob.Prefix, codiumStorage);
                    }

                    if (elementoBlob.IsBlob)
                    {
                        codiumStorage.DeleteBlobIfExists(elementoBlob.Blob.Name, Azure.Storage.Blobs.Models.DeleteSnapshotsOption.IncludeSnapshots);
                    }
                }
            }
        }

        /// <summary>
        /// Función que borrar carpetas o archivos
        /// </summary>
        /// <param name = "toDelete">Listado de archivos o carpetas a borrar</param>
        /// <returns>Json</returns>
        [Route("CodiumStorage/Borrar")]
        [HttpPost]
        public IActionResult Borrar(string toDelete)
        {
            Azure.Storage.Blobs.BlobServiceClient blobServiceClient = new Azure.Storage.Blobs.BlobServiceClient(this._configuracion["ConnectionStrings:CodiumStorage"]);
            Azure.Storage.Blobs.BlobContainerClient blobContainerClient = blobServiceClient.GetBlobContainerClient(this._configuracion["AppSettings:CodiumStorage:Contenedor"]);
            string[] lista = toDelete.Split('|');
            foreach (string elemento in lista)
            {
                if (elemento != "")
                {
                    foreach (Azure.Page<Azure.Storage.Blobs.Models.BlobHierarchyItem> blobsByHierarchy in blobContainerClient.GetBlobsByHierarchy(prefix: elemento + "/", delimiter: "/").AsPages())
                    {
                        var listaBlobs = blobsByHierarchy.Values.ToList();
                        foreach (var elementoBlob in listaBlobs)
                        {
                            if (elementoBlob.IsPrefix)
                            {
                                BorrarRecursive(elementoBlob.Prefix, blobContainerClient);
                            }

                            if (elementoBlob.IsBlob)
                            {
                                blobContainerClient.DeleteBlobIfExists(elementoBlob.Blob.Name, Azure.Storage.Blobs.Models.DeleteSnapshotsOption.IncludeSnapshots);
                            }
                        }
                    }

                    blobContainerClient.DeleteBlobIfExists(elemento.Replace(blobContainerClient.Uri + "/", ""), Azure.Storage.Blobs.Models.DeleteSnapshotsOption.IncludeSnapshots);
                }
            }

            return Json(true);
        }

        /// <summary>
        /// Función para subir archivos
        /// </summary>
        /// <param name = "carpeta">Variable con el folder a buscar</param>
        /// <param name = "file">Archivo a subir</param>
        /// <returns>Json</returns>
        [Route("CodiumStorage/SubirArchivo")]
        [HttpPost]
        public IActionResult SubirArchivo(string carpeta, List<IFormFile> file)
        {
            Azure.Storage.Blobs.BlobServiceClient blobServiceClient = new Azure.Storage.Blobs.BlobServiceClient(this._configuracion["ConnectionStrings:CodiumStorage"]);
            Azure.Storage.Blobs.BlobContainerClient blobContainerClient = blobServiceClient.GetBlobContainerClient(this._configuracion["AppSettings:CodiumStorage:Contenedor"]);
            foreach (var inputFile in file)
            {
                if (inputFile.Length > 0)
                {
                    System.IO.Stream stream = inputFile.OpenReadStream();
                    var blobClient = blobContainerClient.GetBlobClient(carpeta + "/" + inputFile.FileName);
                    if (blobClient == null)
                    {
                        blobContainerClient.UploadBlob(carpeta + "/" + inputFile.FileName, stream);
                    }
                    else
                    {
                        blobClient.Upload(stream);
                    }
                }
            }

            return Json(true);
        }

        /// <summary>
        /// Función que recupera los archivos del disco.
        /// </summary>
        /// <param name = "carpeta">Variable con el folder a buscar</param>
        /// <param name = "tema">Diseño que se aplicará a la vista.</param>
        /// <returns>Json</returns>
        [Route("CodiumStorage/ListaDeArchivos")]
        [HttpPost]
        public IActionResult ListaDeArchivos(string carpeta, long tema)
        {
            Azure.Storage.Blobs.BlobServiceClient blobServiceClient = new Azure.Storage.Blobs.BlobServiceClient(this._configuracion["ConnectionStrings:CodiumStorage"]);
            Azure.Storage.Blobs.BlobContainerClient blobContainerClient = blobServiceClient.GetBlobContainerClient(this._configuracion["AppSettings:CodiumStorage:Contenedor"]);
            System.Text.StringBuilder resultado = new System.Text.StringBuilder();
            // Iconos
            string imageFolder = "";
            string imagePredefinedFile = "";
            string image3Ds = "";
            string imageAi = "";
            string imageAsp = "";
            string imageAvi = "";
            string imageBin = "";
            string imageCom = "";
            string imageCss = "";
            string imageCsv = "";
            string imageDbf = "";
            string imageDll = "";
            string imageDocDocx = "";
            string imageDwg = "";
            string imageEml = "";
            string imageEps = "";
            string imageExe = "";
            string imageFla = "";
            string imageGif = "";
            string imageHtmHtml = "";
            string imageIco = "";
            string imageIni = "";
            string imageIso = "";
            string imageJar = "";
            string imageJpgJpeg = "";
            string imageJs = "";
            string imageMkv = "";
            string imageMov = "";
            string imageMp3 = "";
            string imageMp4 = "";
            string imageNfo = "";
            string imageObj = "";
            string imageOtf = "";
            string imagePdf = "";
            string imagePkg = "";
            string imagePng = "";
            string imagePptPptx = "";
            string imagePsd = "";
            string imageRtf = "";
            string imageSvg = "";
            string imageTtf = "";
            string imageTxt = "";
            string imageVcf = "";
            string imageWav = "";
            string imageWmv = "";
            string imageXlsXlsx = "";
            string imageXml = "";
            string imageZip = "";
            // Prueba
            if (tema == 1)
            {
                imageFolder = "/assets/Prueba_Closefolder.png";
                imagePredefinedFile = "/assets/Prueba_Defaulticon.png";
                image3Ds = "/assets/Prueba_3Ds.png";
                imageAi = "/assets/Prueba_Ai.png";
                imageAsp = "/assets/Prueba_Asp.png";
                imageAvi = "/assets/Prueba_Avi.png";
                imageBin = "/assets/Prueba_Bin.png";
                imageCom = "/assets/Prueba_Com.png";
                imageCss = "/assets/Prueba_Css.png";
                imageCsv = "/assets/Prueba_Csv.png";
                imageDbf = "/assets/Prueba_Dbf.png";
                imageDll = "/assets/Prueba_Dll.png";
                imageDocDocx = "/assets/Prueba_DocDocx.png";
                imageDwg = "/assets/Prueba_Dwg.png";
                imageEml = "/assets/Prueba_Eml.png";
                imageEps = "/assets/Prueba_Eps.png";
                imageExe = "/assets/Prueba_Exe.png";
                imageFla = "/assets/Prueba_Fla.png";
                imageGif = "/assets/Prueba_Gif.png";
                imageHtmHtml = "/assets/Prueba_HtmHtml.png";
                imageIco = "/assets/Prueba_Ico.png";
                imageIni = "/assets/Prueba_Ini.png";
                imageIso = "/assets/Prueba_Iso.png";
                imageJar = "/assets/Prueba_Jar.png";
                imageJpgJpeg = "/assets/Prueba_JpgJpeg.png";
                imageJs = "/assets/Prueba_Js.png";
                imageMkv = "/assets/Prueba_Mkv.png";
                imageMov = "/assets/Prueba_Mov.png";
                imageMp3 = "/assets/Prueba_Mp3.png";
                imageMp4 = "/assets/Prueba_Mp4.png";
                imageNfo = "/assets/Prueba_Nfo.png";
                imageObj = "/assets/Prueba_Obj.png";
                imageOtf = "/assets/Prueba_Otf.png";
                imagePdf = "/assets/Prueba_Pdf.png";
                imagePkg = "/assets/Prueba_Pkg.png";
                imagePng = "/assets/Prueba_Png.png";
                imagePptPptx = "/assets/Prueba_PptPptx.png";
                imagePsd = "/assets/Prueba_Psd.png";
                imageRtf = "/assets/Prueba_Rtf.png";
                imageSvg = "/assets/Prueba_Svg.png";
                imageTtf = "/assets/Prueba_Ttf.png";
                imageTxt = "/assets/Prueba_Txt.png";
                imageVcf = "/assets/Prueba_Vcf.png";
                imageWav = "/assets/Prueba_Wav.png";
                imageWmv = "/assets/Prueba_Wmv.png";
                imageXlsXlsx = "/assets/Prueba_XlsXlsx.png";
                imageXml = "/assets/Prueba_Xml.png";
                imageZip = "/assets/Prueba_Zip.png";
            }

            resultado.Append("[");
            foreach (Azure.Page<Azure.Storage.Blobs.Models.BlobHierarchyItem> blobsByHierarchy in blobContainerClient.GetBlobsByHierarchy(prefix: carpeta + "/", delimiter: "/").AsPages())
            {
                var lista = blobsByHierarchy.Values.ToList();
                foreach (var elemento in lista)
                {
                    if (elemento.IsPrefix)
                    {
                        string prefix = elemento.Prefix.Substring(0, elemento.Prefix.LastIndexOf("/"));
                        resultado.Append("{");
                        resultado.Append("\"name\":" + Newtonsoft.Json.JsonConvert.SerializeObject(prefix.Substring(prefix.LastIndexOf("/") + 1)) + ",");
                        resultado.Append("\"shortName\":" + Newtonsoft.Json.JsonConvert.SerializeObject(prefix.Substring(prefix.LastIndexOf("/") + 1).Length > 10 ? prefix.Substring(prefix.LastIndexOf("/") + 1).Substring(0, 10) + "..." : prefix.Substring(prefix.LastIndexOf("/") + 1)) + ",");
                        resultado.Append("\"fullPath\":" + Newtonsoft.Json.JsonConvert.SerializeObject(elemento.Prefix.Substring(0, elemento.Prefix.LastIndexOf('/'))) + ",");
                        resultado.Append("\"size\":0,");
                        resultado.Append("\"image\":\"" + imageFolder + "\",");
                        resultado.Append("\"isFolder\":true");
                        resultado.Append("},");
                    }
                }

                foreach (var elemento in lista)
                {
                    if (elemento.IsBlob)
                    {
                        string imageFile = imagePredefinedFile;
                        if (image3Ds != "" && (elemento.Blob.Name.Split('.').Last() == "3ds"))
                        {
                            imageFile = image3Ds;
                        }

                        if (imageAi != "" && (elemento.Blob.Name.Split('.').Last() == "ai"))
                        {
                            imageFile = imageAi;
                        }

                        if (imageAsp != "" && (elemento.Blob.Name.Split('.').Last() == "asp"))
                        {
                            imageFile = imageAsp;
                        }

                        if (imageAvi != "" && (elemento.Blob.Name.Split('.').Last() == "avi"))
                        {
                            imageFile = imageAvi;
                        }

                        if (imageBin != "" && (elemento.Blob.Name.Split('.').Last() == "bin"))
                        {
                            imageFile = imageBin;
                        }

                        if (imageCom != "" && (elemento.Blob.Name.Split('.').Last() == "com"))
                        {
                            imageFile = imageCom;
                        }

                        if (imageCss != "" && (elemento.Blob.Name.Split('.').Last() == "css"))
                        {
                            imageFile = imageCss;
                        }

                        if (imageCsv != "" && (elemento.Blob.Name.Split('.').Last() == "csv"))
                        {
                            imageFile = imageCsv;
                        }

                        if (imageDbf != "" && (elemento.Blob.Name.Split('.').Last() == "dbf"))
                        {
                            imageFile = imageDbf;
                        }

                        if (imageDll != "" && (elemento.Blob.Name.Split('.').Last() == "dll"))
                        {
                            imageFile = imageDll;
                        }

                        if (imageDocDocx != "" && (elemento.Blob.Name.Split('.').Last() == "doc" || elemento.Blob.Name.Split('.').Last() == "docx"))
                        {
                            imageFile = imageDocDocx;
                        }

                        if (imageDwg != "" && (elemento.Blob.Name.Split('.').Last() == "dwg"))
                        {
                            imageFile = imageDwg;
                        }

                        if (imageEml != "" && (elemento.Blob.Name.Split('.').Last() == "eml"))
                        {
                            imageFile = imageEml;
                        }

                        if (imageEps != "" && (elemento.Blob.Name.Split('.').Last() == "eps"))
                        {
                            imageFile = imageEps;
                        }

                        if (imageExe != "" && (elemento.Blob.Name.Split('.').Last() == "exe"))
                        {
                            imageFile = imageExe;
                        }

                        if (imageFla != "" && (elemento.Blob.Name.Split('.').Last() == "fla"))
                        {
                            imageFile = imageFla;
                        }

                        if (imageGif != "" && (elemento.Blob.Name.Split('.').Last() == "gif"))
                        {
                            imageFile = imageGif;
                        }

                        if (imageHtmHtml != "" && (elemento.Blob.Name.Split('.').Last() == "htm" || elemento.Blob.Name.Split('.').Last() == "html"))
                        {
                            imageFile = imageHtmHtml;
                        }

                        if (imageIco != "" && (elemento.Blob.Name.Split('.').Last() == "ico"))
                        {
                            imageFile = imageIco;
                        }

                        if (imageIni != "" && (elemento.Blob.Name.Split('.').Last() == "ini"))
                        {
                            imageFile = imageIni;
                        }

                        if (imageIso != "" && (elemento.Blob.Name.Split('.').Last() == "iso"))
                        {
                            imageFile = imageIso;
                        }

                        if (imageJar != "" && (elemento.Blob.Name.Split('.').Last() == "jar"))
                        {
                            imageFile = imageJar;
                        }

                        if (imageJpgJpeg != "" && (elemento.Blob.Name.Split('.').Last() == "jpg" || elemento.Blob.Name.Split('.').Last() == "jpeg"))
                        {
                            imageFile = imageJpgJpeg;
                        }

                        if (imageJs != "" && (elemento.Blob.Name.Split('.').Last() == "js"))
                        {
                            imageFile = imageJs;
                        }

                        if (imageMkv != "" && (elemento.Blob.Name.Split('.').Last() == "mkv"))
                        {
                            imageFile = imageMkv;
                        }

                        if (imageMov != "" && (elemento.Blob.Name.Split('.').Last() == "mov"))
                        {
                            imageFile = imageMov;
                        }

                        if (imageMp3 != "" && (elemento.Blob.Name.Split('.').Last() == "mp3"))
                        {
                            imageFile = imageMp3;
                        }

                        if (imageMp4 != "" && (elemento.Blob.Name.Split('.').Last() == "mp4"))
                        {
                            imageFile = imageMp4;
                        }

                        if (imageNfo != "" && (elemento.Blob.Name.Split('.').Last() == "nfo"))
                        {
                            imageFile = imageNfo;
                        }

                        if (imageObj != "" && (elemento.Blob.Name.Split('.').Last() == "obj"))
                        {
                            imageFile = imageObj;
                        }

                        if (imageOtf != "" && (elemento.Blob.Name.Split('.').Last() == "otf"))
                        {
                            imageFile = imageOtf;
                        }

                        if (imagePdf != "" && (elemento.Blob.Name.Split('.').Last() == "pdf"))
                        {
                            imageFile = imagePdf;
                        }

                        if (imagePkg != "" && (elemento.Blob.Name.Split('.').Last() == "pkg"))
                        {
                            imageFile = imagePkg;
                        }

                        if (imagePng != "" && (elemento.Blob.Name.Split('.').Last() == "png"))
                        {
                            imageFile = imagePng;
                        }

                        if (imagePptPptx != "" && (elemento.Blob.Name.Split('.').Last() == "ppt" || elemento.Blob.Name.Split('.').Last() == "pptx"))
                        {
                            imageFile = imagePptPptx;
                        }

                        if (imagePsd != "" && (elemento.Blob.Name.Split('.').Last() == "psd"))
                        {
                            imageFile = imagePsd;
                        }

                        if (imageRtf != "" && (elemento.Blob.Name.Split('.').Last() == "rtf"))
                        {
                            imageFile = imageRtf;
                        }

                        if (imageSvg != "" && (elemento.Blob.Name.Split('.').Last() == "svg"))
                        {
                            imageFile = imageSvg;
                        }

                        if (imageTtf != "" && (elemento.Blob.Name.Split('.').Last() == "ttf"))
                        {
                            imageFile = imageTtf;
                        }

                        if (imageTxt != "" && (elemento.Blob.Name.Split('.').Last() == "txt"))
                        {
                            imageFile = imageTxt;
                        }

                        if (imageVcf != "" && (elemento.Blob.Name.Split('.').Last() == "vcf"))
                        {
                            imageFile = imageVcf;
                        }

                        if (imageWav != "" && (elemento.Blob.Name.Split('.').Last() == "wav"))
                        {
                            imageFile = imageWav;
                        }

                        if (imageWmv != "" && (elemento.Blob.Name.Split('.').Last() == "wmv"))
                        {
                            imageFile = imageWmv;
                        }

                        if (imageXlsXlsx != "" && (elemento.Blob.Name.Split('.').Last() == "xls" || elemento.Blob.Name.Split('.').Last() == "xlsx"))
                        {
                            imageFile = imageXlsXlsx;
                        }

                        if (imageXml != "" && (elemento.Blob.Name.Split('.').Last() == "xml"))
                        {
                            imageFile = imageXml;
                        }

                        if (imageZip != "" && (elemento.Blob.Name.Split('.').Last() == "zip"))
                        {
                            imageFile = imageZip;
                        }

                        resultado.Append("{");
                        resultado.Append("\"name\":" + Newtonsoft.Json.JsonConvert.SerializeObject(elemento.Blob.Name.Substring(elemento.Blob.Name.LastIndexOf("/") + 1)) + ",");
                        resultado.Append("\"shortName\":" + Newtonsoft.Json.JsonConvert.SerializeObject(elemento.Blob.Name.Substring(elemento.Blob.Name.LastIndexOf("/") + 1).Length > 10 ? elemento.Blob.Name.Substring(elemento.Blob.Name.LastIndexOf("/") + 1).Substring(0, 10) + "..." : elemento.Blob.Name.Substring(elemento.Blob.Name.LastIndexOf("/") + 1)) + ",");
                        resultado.Append("\"fullPath\":" + Newtonsoft.Json.JsonConvert.SerializeObject(blobContainerClient.Uri + "/" + elemento.Blob.Name) + ",");
                        resultado.Append("\"size\":" + elemento.Blob.Properties.ContentLength + ",");
                        resultado.Append("\"image\":\"" + imageFile + "\",");
                        resultado.Append("\"isFolder\":false");
                        resultado.Append("},");
                    }
                }

                if (lista.Count > 0)
                {
                    resultado = new System.Text.StringBuilder(resultado.ToString(0, resultado.Length - 1));
                }
            }

            resultado.Append("]");
            return Content(resultado.ToString(), "application/json");
        }

        /// <summary>
        /// Función que recupera los folders del disco.
        /// </summary>
        /// <param name = "carpeta">Variable con el folder a buscar</param>
        /// <returns>Json</returns>
        [Route("CodiumStorage/ListaDeCarpetas")]
        [HttpPost]
        public IActionResult ListaDeCarpetas(string carpeta)
        {
            Azure.Storage.Blobs.BlobServiceClient blobServiceClient = new Azure.Storage.Blobs.BlobServiceClient(this._configuracion["ConnectionStrings:CodiumStorage"]);
            Azure.Storage.Blobs.BlobContainerClient blobContainerClient = blobServiceClient.GetBlobContainerClient(this._configuracion["AppSettings:CodiumStorage:Contenedor"]);
            System.Text.StringBuilder resultado = new System.Text.StringBuilder();
            resultado.Append("[");
            foreach (Azure.Page<Azure.Storage.Blobs.Models.BlobHierarchyItem> blobsByHierarchy in blobContainerClient.GetBlobsByHierarchy(prefix: carpeta + "/", delimiter: "/").AsPages())
            {
                var lista = blobsByHierarchy.Values.Where(x => x.IsPrefix).ToList();
                foreach (var elemento in lista)
                {
                    string prefix = elemento.Prefix.Substring(0, elemento.Prefix.LastIndexOf("/"));
                    resultado.Append("{");
                    resultado.Append("\"name\":" + Newtonsoft.Json.JsonConvert.SerializeObject(prefix.Substring(prefix.LastIndexOf("/") + 1)) + ",\"expanded\":true,");
                    resultado.Append("\"children\":[");
                    resultado.Append(CarpetasAnidadas(elemento.Prefix, blobContainerClient));
                    resultado.Append("]");
                    resultado.Append("},");
                }

                if (lista.Count > 0)
                {
                    resultado = new System.Text.StringBuilder(resultado.ToString(0, resultado.Length - 1));
                }
            }

            resultado.Append("]");
            return Content(resultado.ToString(), "application/json");
        }
#endregion
    }
}