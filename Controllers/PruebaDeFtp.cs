using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Threading.Tasks;
namespace Codium.Web.Controllers
{
    /// <summary>
    /// Prueba de FTP
    /// </summary>
    [ApiExplorerSettings(IgnoreApi = true)]
    public class PruebaDeFtp : Controller
    {
#region Variables Privadas
        /// <summary>
        /// Web Host Environment
        /// </summary>        
        private readonly IWebHostEnvironment _env;
        /// <summary>
        /// Razor View To StringRenderer
        /// </summary>
        private readonly IRazorViewToStringRenderer _renderer;
        /// <summary>
        /// Objeto para encriptar y desencriptar valores
        /// </summary>
        private readonly IDataProtector _protector;
        /// <summary>
        /// Acceso a la configuración de la aplicación
        /// </summary>
        private readonly Microsoft.Extensions.Configuration.IConfiguration _configuracion;
        /// <summary>
        /// Variable con el idioma de la aplicación
        /// </summary>
        private readonly Microsoft.Extensions.Localization.IStringLocalizer<PruebaDeExcel.RecursosCompartidos> _idioma;
        /// <summary>
        /// Opciones de Idioma
        /// </summary>
        private readonly Microsoft.Extensions.Options.IOptions<Microsoft.AspNetCore.Builder.RequestLocalizationOptions> _opcionesDeIdioma;
#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name = "env">Web Host Environment</param>
        /// <param name = "renderer">Razor View To String Renderer</param>
        /// <param name = "protector">Objeto para encriptar y desencriptar valores</param>
        /// <param name = "configuracion">Acceso a la configuración de la aplicación</param>
        /// <param name = "idioma">Variable con el idioma de la aplicación</param>
        /// <param name = "opcionesDeIdioma">Opciones de Idioma</param>
        public PruebaDeFtp(IWebHostEnvironment env, IRazorViewToStringRenderer renderer, IDataProtectionProvider protector, Microsoft.Extensions.Configuration.IConfiguration configuracion, Microsoft.Extensions.Localization.IStringLocalizer<PruebaDeExcel.RecursosCompartidos> idioma, Microsoft.Extensions.Options.IOptions<Microsoft.AspNetCore.Builder.RequestLocalizationOptions> opcionesDeIdioma)
        {
            this._env = env;
            this._renderer = renderer;
            this._protector = protector.CreateProtector("URLProtection");
            this._configuracion = configuracion;
            this._idioma = idioma;
            this._opcionesDeIdioma = opcionesDeIdioma;
        }

#endregion
#region Métodos
        /// <summary>
        /// Función que recupera las subcarpetas de un folder.
        /// </summary>
        /// <param name = "carpeta">Variable con el folder a buscar</param>
        /// <param name = "pruebaDeFtp">Conexión</param>
        /// <returns>string</returns>
        private string CarpetasAnidadas(string carpeta, FluentFTP.FtpClient pruebaDeFtp)
        {
            pruebaDeFtp.SetWorkingDirectory(carpeta);
            System.Text.StringBuilder resultado = new System.Text.StringBuilder();
            var lista = pruebaDeFtp.GetListing();
            foreach (var elemento in lista)
            {
                if (elemento.Type == FluentFTP.FtpObjectType.Directory)
                {
                    resultado.Append("{");
                    resultado.Append("\"name\":" + Newtonsoft.Json.JsonConvert.SerializeObject(elemento.Name) + ",\"expanded\":true,");
                    resultado.Append("\"children\":[");
                    resultado.Append(CarpetasAnidadas(carpeta + "/" + elemento.Name, pruebaDeFtp));
                    resultado.Append("]");
                    resultado.Append("},");
                }
            }

            if (lista.Length > 0 && resultado.ToString() != "")
            {
                resultado = new System.Text.StringBuilder(resultado.ToString(0, resultado.Length - 1));
            }

            return resultado.ToString();
        }

        /// <summary>
        /// Función para descargar un archivo.
        /// </summary>
        /// <param name = "archivo">Archivo a descargar</param>
        /// <returns>File</returns>
        [Route("PruebaDeFtp/Descargar")]
        [HttpGet]
        public IActionResult Descargar(string archivo)
        {
            FluentFTP.FtpClient pruebaDeFtp = new FluentFTP.FtpClient(this._configuracion["AppSettings:PruebaDeFtp"], int.Parse(this._configuracion["AppSettings:PruebaDeFtpPuerto"]), this._configuracion["AppSettings:PruebaDeFtpUsuario"], this._configuracion["AppSettings:PruebaDeFtpContrasena"]);
            pruebaDeFtp.Connect();
            System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
            pruebaDeFtp.DownloadStream(memoryStream, "/" + (this._configuracion["AppSettings:PruebaDeFtpCarpeta"] == "" ? "" : this._configuracion["AppSettings:PruebaDeFtpCarpeta"] + "/") + archivo);
            pruebaDeFtp.Disconnect();
            byte[] byteArray = memoryStream.ToArray();
            memoryStream.Close();
            return File(byteArray, MimeMapping.MimeUtility.GetMimeMapping(archivo.Split('.').Last()), archivo.Split('.').Last());
        }

        /// <summary>
        /// Función que borrar carpetas o archivos
        /// </summary>
        /// <param name = "toDelete">Listado de archivos o carpetas a borrar</param>
        /// <returns>Json</returns>
        [Route("PruebaDeFtp/Borrar")]
        [HttpPost]
        public IActionResult Borrar(string toDelete)
        {
            FluentFTP.FtpClient pruebaDeFtp = new FluentFTP.FtpClient(this._configuracion["AppSettings:PruebaDeFtp"], int.Parse(this._configuracion["AppSettings:PruebaDeFtpPuerto"]), this._configuracion["AppSettings:PruebaDeFtpUsuario"], this._configuracion["AppSettings:PruebaDeFtpContrasena"]);
            pruebaDeFtp.Connect();
            string[] lista = toDelete.Split('|');
            foreach (string elemento in lista)
            {
                if (elemento != "")
                {
                    var objectInfo = pruebaDeFtp.GetObjectInfo("/" + (this._configuracion["AppSettings:PruebaDeFtpCarpeta"] == "" ? "" : this._configuracion["AppSettings:PruebaDeFtpCarpeta"] + "/") + elemento);
                    if (objectInfo.Type == FluentFTP.FtpObjectType.Directory)
                    {
                        pruebaDeFtp.DeleteDirectory("/" + (this._configuracion["AppSettings:PruebaDeFtpCarpeta"] == "" ? "" : this._configuracion["AppSettings:PruebaDeFtpCarpeta"] + "/") + elemento);
                    }
                    else
                    {
                        pruebaDeFtp.DeleteFile("/" + (this._configuracion["AppSettings:PruebaDeFtpCarpeta"] == "" ? "" : this._configuracion["AppSettings:PruebaDeFtpCarpeta"] + "/") + elemento);
                    }
                }
            }

            pruebaDeFtp.Disconnect();
            return Json(true);
        }

        /// <summary>
        /// Función para subir archivos
        /// </summary>
        /// <param name = "carpeta">Variable con el folder a buscar</param>
        /// <param name = "file">Archivo a subir</param>
        /// <returns>Json</returns>
        [Route("PruebaDeFtp/SubirArchivo")]
        [HttpPost]
        public IActionResult SubirArchivo(string carpeta, List<IFormFile> file)
        {
            FluentFTP.FtpClient pruebaDeFtp = new FluentFTP.FtpClient(this._configuracion["AppSettings:PruebaDeFtp"], int.Parse(this._configuracion["AppSettings:PruebaDeFtpPuerto"]), this._configuracion["AppSettings:PruebaDeFtpUsuario"], this._configuracion["AppSettings:PruebaDeFtpContrasena"]);
            pruebaDeFtp.Connect();
            foreach (var inputFile in file)
            {
                if (inputFile.Length > 0)
                {
                    System.IO.Stream stream = inputFile.OpenReadStream();
                    byte[] binary = new byte[stream.Length];
                    stream.Read(binary, 0, (int)stream.Length);
                    pruebaDeFtp.UploadBytes(binary, "/" + (this._configuracion["AppSettings:PruebaDeFtpCarpeta"] == "" ? "" : this._configuracion["AppSettings:PruebaDeFtpCarpeta"] + "/") + carpeta + "/" + inputFile.FileName, FluentFTP.FtpRemoteExists.Overwrite);
                }
            }

            pruebaDeFtp.Disconnect();
            return Json(true);
        }

        /// <summary>
        /// Función que agregar una carpeta
        /// </summary>
        /// <param name = "carpeta">Variable con el folder a buscar</param>
        /// <param name = "agregarFolder">Nombre de Folder</param>
        /// <returns>Json</returns>
        [Route("PruebaDeFtp/AgregarFolder")]
        [HttpPost]
        public IActionResult AgregarFolder(string carpeta, string agregarFolder)
        {
            FluentFTP.FtpClient pruebaDeFtp = new FluentFTP.FtpClient(this._configuracion["AppSettings:PruebaDeFtp"], int.Parse(this._configuracion["AppSettings:PruebaDeFtpPuerto"]), this._configuracion["AppSettings:PruebaDeFtpUsuario"], this._configuracion["AppSettings:PruebaDeFtpContrasena"]);
            pruebaDeFtp.Connect();
            if (!pruebaDeFtp.DirectoryExists("/" + (this._configuracion["AppSettings:PruebaDeFtpCarpeta"] == "" ? "" : this._configuracion["AppSettings:PruebaDeFtpCarpeta"] + "/") + carpeta + "/" + agregarFolder))
            {
                pruebaDeFtp.CreateDirectory("/" + (this._configuracion["AppSettings:PruebaDeFtpCarpeta"] == "" ? "" : this._configuracion["AppSettings:PruebaDeFtpCarpeta"] + "/") + carpeta + "/" + agregarFolder);
            }

            pruebaDeFtp.Disconnect();
            return Json(true);
        }

        /// <summary>
        /// Función que recupera los archivos del disco.
        /// </summary>
        /// <param name = "carpeta">Variable con el folder a buscar</param>
        /// <param name = "tema">Diseño que se aplicará a la vista.</param>
        /// <returns>Json</returns>
        [Route("PruebaDeFtp/ListaDeArchivos")]
        [HttpPost]
        public IActionResult ListaDeArchivos(string carpeta, long tema)
        {
            FluentFTP.FtpClient pruebaDeFtp = new FluentFTP.FtpClient(this._configuracion["AppSettings:PruebaDeFtp"], int.Parse(this._configuracion["AppSettings:PruebaDeFtpPuerto"]), this._configuracion["AppSettings:PruebaDeFtpUsuario"], this._configuracion["AppSettings:PruebaDeFtpContrasena"]);
            pruebaDeFtp.Connect();
            System.Text.StringBuilder resultado = new System.Text.StringBuilder();
            // Iconos
            string imageFolder = "";
            string imagePredefinedFile = "";
            string image3Ds = "";
            string imageAi = "";
            string imageAsp = "";
            string imageAvi = "";
            string imageBin = "";
            string imageCom = "";
            string imageCss = "";
            string imageCsv = "";
            string imageDbf = "";
            string imageDll = "";
            string imageDocDocx = "";
            string imageDwg = "";
            string imageEml = "";
            string imageEps = "";
            string imageExe = "";
            string imageFla = "";
            string imageGif = "";
            string imageHtmHtml = "";
            string imageIco = "";
            string imageIni = "";
            string imageIso = "";
            string imageJar = "";
            string imageJpgJpeg = "";
            string imageJs = "";
            string imageMkv = "";
            string imageMov = "";
            string imageMp3 = "";
            string imageMp4 = "";
            string imageNfo = "";
            string imageObj = "";
            string imageOtf = "";
            string imagePdf = "";
            string imagePkg = "";
            string imagePng = "";
            string imagePptPptx = "";
            string imagePsd = "";
            string imageRtf = "";
            string imageSvg = "";
            string imageTtf = "";
            string imageTxt = "";
            string imageVcf = "";
            string imageWav = "";
            string imageWmv = "";
            string imageXlsXlsx = "";
            string imageXml = "";
            string imageZip = "";
            // Prueba
            if (tema == 1)
            {
                imageFolder = "/assets/Prueba_Closefolder.png";
                imagePredefinedFile = "/assets/Prueba_Defaulticon.png";
                image3Ds = "/assets/Prueba_3Ds.png";
                imageAi = "/assets/Prueba_Ai.png";
                imageAsp = "/assets/Prueba_Asp.png";
                imageAvi = "/assets/Prueba_Avi.png";
                imageBin = "/assets/Prueba_Bin.png";
                imageCom = "/assets/Prueba_Com.png";
                imageCss = "/assets/Prueba_Css.png";
                imageCsv = "/assets/Prueba_Csv.png";
                imageDbf = "/assets/Prueba_Dbf.png";
                imageDll = "/assets/Prueba_Dll.png";
                imageDocDocx = "/assets/Prueba_DocDocx.png";
                imageDwg = "/assets/Prueba_Dwg.png";
                imageEml = "/assets/Prueba_Eml.png";
                imageEps = "/assets/Prueba_Eps.png";
                imageExe = "/assets/Prueba_Exe.png";
                imageFla = "/assets/Prueba_Fla.png";
                imageGif = "/assets/Prueba_Gif.png";
                imageHtmHtml = "/assets/Prueba_HtmHtml.png";
                imageIco = "/assets/Prueba_Ico.png";
                imageIni = "/assets/Prueba_Ini.png";
                imageIso = "/assets/Prueba_Iso.png";
                imageJar = "/assets/Prueba_Jar.png";
                imageJpgJpeg = "/assets/Prueba_JpgJpeg.png";
                imageJs = "/assets/Prueba_Js.png";
                imageMkv = "/assets/Prueba_Mkv.png";
                imageMov = "/assets/Prueba_Mov.png";
                imageMp3 = "/assets/Prueba_Mp3.png";
                imageMp4 = "/assets/Prueba_Mp4.png";
                imageNfo = "/assets/Prueba_Nfo.png";
                imageObj = "/assets/Prueba_Obj.png";
                imageOtf = "/assets/Prueba_Otf.png";
                imagePdf = "/assets/Prueba_Pdf.png";
                imagePkg = "/assets/Prueba_Pkg.png";
                imagePng = "/assets/Prueba_Png.png";
                imagePptPptx = "/assets/Prueba_PptPptx.png";
                imagePsd = "/assets/Prueba_Psd.png";
                imageRtf = "/assets/Prueba_Rtf.png";
                imageSvg = "/assets/Prueba_Svg.png";
                imageTtf = "/assets/Prueba_Ttf.png";
                imageTxt = "/assets/Prueba_Txt.png";
                imageVcf = "/assets/Prueba_Vcf.png";
                imageWav = "/assets/Prueba_Wav.png";
                imageWmv = "/assets/Prueba_Wmv.png";
                imageXlsXlsx = "/assets/Prueba_XlsXlsx.png";
                imageXml = "/assets/Prueba_Xml.png";
                imageZip = "/assets/Prueba_Zip.png";
            }

            resultado.Append("[");
            pruebaDeFtp.SetWorkingDirectory("/" + (this._configuracion["AppSettings:PruebaDeFtpCarpeta"] == "" ? "" : this._configuracion["AppSettings:PruebaDeFtpCarpeta"] + "/") + carpeta);
            var lista = pruebaDeFtp.GetListing();
            foreach (var elemento in lista)
            {
                if (elemento.Type == FluentFTP.FtpObjectType.Directory)
                {
                    resultado.Append("{");
                    resultado.Append("\"name\":" + Newtonsoft.Json.JsonConvert.SerializeObject(elemento.Name) + ",");
                    resultado.Append("\"shortName\":" + Newtonsoft.Json.JsonConvert.SerializeObject(elemento.Name.Length > 10 ? elemento.Name.Substring(0, 10) + "..." : elemento.Name) + ",");
                    resultado.Append("\"fullPath\":" + Newtonsoft.Json.JsonConvert.SerializeObject(this._configuracion["AppSettings:PruebaDeFtpCarpeta"] == "" ? elemento.FullName : elemento.FullName.Replace("/" + this._configuracion["AppSettings:PruebaDeFtpCarpeta"] + "/", "")) + ",");
                    resultado.Append("\"size\":0,");
                    resultado.Append("\"image\":\"" + imageFolder + "\",");
                    resultado.Append("\"isFolder\":true");
                    resultado.Append("},");
                }
            }

            foreach (var elemento in lista)
            {
                if (elemento.Type != FluentFTP.FtpObjectType.Directory)
                {
                    string imageFile = imagePredefinedFile;
                    if (image3Ds != "" && (elemento.Name.Split('.').Last() == "3ds"))
                    {
                        imageFile = image3Ds;
                    }

                    if (imageAi != "" && (elemento.Name.Split('.').Last() == "ai"))
                    {
                        imageFile = imageAi;
                    }

                    if (imageAsp != "" && (elemento.Name.Split('.').Last() == "asp"))
                    {
                        imageFile = imageAsp;
                    }

                    if (imageAvi != "" && (elemento.Name.Split('.').Last() == "avi"))
                    {
                        imageFile = imageAvi;
                    }

                    if (imageBin != "" && (elemento.Name.Split('.').Last() == "bin"))
                    {
                        imageFile = imageBin;
                    }

                    if (imageCom != "" && (elemento.Name.Split('.').Last() == "com"))
                    {
                        imageFile = imageCom;
                    }

                    if (imageCss != "" && (elemento.Name.Split('.').Last() == "css"))
                    {
                        imageFile = imageCss;
                    }

                    if (imageCsv != "" && (elemento.Name.Split('.').Last() == "csv"))
                    {
                        imageFile = imageCsv;
                    }

                    if (imageDbf != "" && (elemento.Name.Split('.').Last() == "dbf"))
                    {
                        imageFile = imageDbf;
                    }

                    if (imageDll != "" && (elemento.Name.Split('.').Last() == "dll"))
                    {
                        imageFile = imageDll;
                    }

                    if (imageDocDocx != "" && (elemento.Name.Split('.').Last() == "doc" || elemento.Name.Split('.').Last() == "docx"))
                    {
                        imageFile = imageDocDocx;
                    }

                    if (imageDwg != "" && (elemento.Name.Split('.').Last() == "dwg"))
                    {
                        imageFile = imageDwg;
                    }

                    if (imageEml != "" && (elemento.Name.Split('.').Last() == "eml"))
                    {
                        imageFile = imageEml;
                    }

                    if (imageEps != "" && (elemento.Name.Split('.').Last() == "eps"))
                    {
                        imageFile = imageEps;
                    }

                    if (imageExe != "" && (elemento.Name.Split('.').Last() == "exe"))
                    {
                        imageFile = imageExe;
                    }

                    if (imageFla != "" && (elemento.Name.Split('.').Last() == "fla"))
                    {
                        imageFile = imageFla;
                    }

                    if (imageGif != "" && (elemento.Name.Split('.').Last() == "gif"))
                    {
                        imageFile = imageGif;
                    }

                    if (imageHtmHtml != "" && (elemento.Name.Split('.').Last() == "htm" || elemento.Name.Split('.').Last() == "html"))
                    {
                        imageFile = imageHtmHtml;
                    }

                    if (imageIco != "" && (elemento.Name.Split('.').Last() == "ico"))
                    {
                        imageFile = imageIco;
                    }

                    if (imageIni != "" && (elemento.Name.Split('.').Last() == "ini"))
                    {
                        imageFile = imageIni;
                    }

                    if (imageIso != "" && (elemento.Name.Split('.').Last() == "iso"))
                    {
                        imageFile = imageIso;
                    }

                    if (imageJar != "" && (elemento.Name.Split('.').Last() == "jar"))
                    {
                        imageFile = imageJar;
                    }

                    if (imageJpgJpeg != "" && (elemento.Name.Split('.').Last() == "jpg" || elemento.Name.Split('.').Last() == "jpeg"))
                    {
                        imageFile = imageJpgJpeg;
                    }

                    if (imageJs != "" && (elemento.Name.Split('.').Last() == "js"))
                    {
                        imageFile = imageJs;
                    }

                    if (imageMkv != "" && (elemento.Name.Split('.').Last() == "mkv"))
                    {
                        imageFile = imageMkv;
                    }

                    if (imageMov != "" && (elemento.Name.Split('.').Last() == "mov"))
                    {
                        imageFile = imageMov;
                    }

                    if (imageMp3 != "" && (elemento.Name.Split('.').Last() == "mp3"))
                    {
                        imageFile = imageMp3;
                    }

                    if (imageMp4 != "" && (elemento.Name.Split('.').Last() == "mp4"))
                    {
                        imageFile = imageMp4;
                    }

                    if (imageNfo != "" && (elemento.Name.Split('.').Last() == "nfo"))
                    {
                        imageFile = imageNfo;
                    }

                    if (imageObj != "" && (elemento.Name.Split('.').Last() == "obj"))
                    {
                        imageFile = imageObj;
                    }

                    if (imageOtf != "" && (elemento.Name.Split('.').Last() == "otf"))
                    {
                        imageFile = imageOtf;
                    }

                    if (imagePdf != "" && (elemento.Name.Split('.').Last() == "pdf"))
                    {
                        imageFile = imagePdf;
                    }

                    if (imagePkg != "" && (elemento.Name.Split('.').Last() == "pkg"))
                    {
                        imageFile = imagePkg;
                    }

                    if (imagePng != "" && (elemento.Name.Split('.').Last() == "png"))
                    {
                        imageFile = imagePng;
                    }

                    if (imagePptPptx != "" && (elemento.Name.Split('.').Last() == "ppt" || elemento.Name.Split('.').Last() == "pptx"))
                    {
                        imageFile = imagePptPptx;
                    }

                    if (imagePsd != "" && (elemento.Name.Split('.').Last() == "psd"))
                    {
                        imageFile = imagePsd;
                    }

                    if (imageRtf != "" && (elemento.Name.Split('.').Last() == "rtf"))
                    {
                        imageFile = imageRtf;
                    }

                    if (imageSvg != "" && (elemento.Name.Split('.').Last() == "svg"))
                    {
                        imageFile = imageSvg;
                    }

                    if (imageTtf != "" && (elemento.Name.Split('.').Last() == "ttf"))
                    {
                        imageFile = imageTtf;
                    }

                    if (imageTxt != "" && (elemento.Name.Split('.').Last() == "txt"))
                    {
                        imageFile = imageTxt;
                    }

                    if (imageVcf != "" && (elemento.Name.Split('.').Last() == "vcf"))
                    {
                        imageFile = imageVcf;
                    }

                    if (imageWav != "" && (elemento.Name.Split('.').Last() == "wav"))
                    {
                        imageFile = imageWav;
                    }

                    if (imageWmv != "" && (elemento.Name.Split('.').Last() == "wmv"))
                    {
                        imageFile = imageWmv;
                    }

                    if (imageXlsXlsx != "" && (elemento.Name.Split('.').Last() == "xls" || elemento.Name.Split('.').Last() == "xlsx"))
                    {
                        imageFile = imageXlsXlsx;
                    }

                    if (imageXml != "" && (elemento.Name.Split('.').Last() == "xml"))
                    {
                        imageFile = imageXml;
                    }

                    if (imageZip != "" && (elemento.Name.Split('.').Last() == "zip"))
                    {
                        imageFile = imageZip;
                    }

                    resultado.Append("{");
                    resultado.Append("\"name\":" + Newtonsoft.Json.JsonConvert.SerializeObject(elemento.Name) + ",");
                    resultado.Append("\"shortName\":" + Newtonsoft.Json.JsonConvert.SerializeObject(elemento.Name.Length > 10 ? elemento.Name.Substring(0, 10) + "..." : elemento.Name) + ",");
                    resultado.Append("\"fullPath\":" + Newtonsoft.Json.JsonConvert.SerializeObject(this._configuracion["AppSettings:PruebaDeFtpCarpeta"] == "" ? elemento.FullName : elemento.FullName.Replace("/" + this._configuracion["AppSettings:PruebaDeFtpCarpeta"] + "/", "")) + ",");
                    resultado.Append("\"size\":" + elemento.Size + ",");
                    resultado.Append("\"image\":\"" + imageFile + "\",");
                    resultado.Append("\"isFolder\":false");
                    resultado.Append("},");
                }
            }

            if (lista.Length > 0)
            {
                resultado = new System.Text.StringBuilder(resultado.ToString(0, resultado.Length - 1));
            }

            resultado.Append("]");
            pruebaDeFtp.Disconnect();
            return Content(resultado.ToString(), "application/json");
        }

        /// <summary>
        /// Función que recupera los folders del disco.
        /// </summary>
        /// <param name = "carpeta">Variable con el folder a buscar</param>
        /// <returns>Json</returns>
        [Route("PruebaDeFtp/ListaDeCarpetas")]
        [HttpPost]
        public IActionResult ListaDeCarpetas(string carpeta)
        {
            FluentFTP.FtpClient pruebaDeFtp = new FluentFTP.FtpClient(this._configuracion["AppSettings:PruebaDeFtp"], int.Parse(this._configuracion["AppSettings:PruebaDeFtpPuerto"]), this._configuracion["AppSettings:PruebaDeFtpUsuario"], this._configuracion["AppSettings:PruebaDeFtpContrasena"]);
            pruebaDeFtp.Connect();
            System.Text.StringBuilder resultado = new System.Text.StringBuilder();
            resultado.Append("[");
            if (!pruebaDeFtp.DirectoryExists("/" + (this._configuracion["AppSettings:PruebaDeFtpCarpeta"] == "" ? "" : this._configuracion["AppSettings:PruebaDeFtpCarpeta"] + "/") + carpeta))
            {
                pruebaDeFtp.CreateDirectory("/" + (this._configuracion["AppSettings:PruebaDeFtpCarpeta"] == "" ? "" : this._configuracion["AppSettings:PruebaDeFtpCarpeta"] + "/") + carpeta);
            }

            pruebaDeFtp.SetWorkingDirectory("/" + (this._configuracion["AppSettings:PruebaDeFtpCarpeta"] == "" ? "" : this._configuracion["AppSettings:PruebaDeFtpCarpeta"] + "/") + carpeta);
            var lista = pruebaDeFtp.GetListing();
            foreach (var elemento in lista)
            {
                if (elemento.Type == FluentFTP.FtpObjectType.Directory)
                {
                    resultado.Append("{");
                    resultado.Append("\"name\":" + Newtonsoft.Json.JsonConvert.SerializeObject(elemento.Name) + ",\"expanded\":true,");
                    resultado.Append("\"children\":[");
                    resultado.Append(CarpetasAnidadas("/" + (this._configuracion["AppSettings:PruebaDeFtpCarpeta"] == "" ? "" : this._configuracion["AppSettings:PruebaDeFtpCarpeta"] + "/") + carpeta + "/" + elemento.Name, pruebaDeFtp));
                    resultado.Append("]");
                    resultado.Append("},");
                }
            }

            if (lista.Length > 0)
            {
                resultado = new System.Text.StringBuilder(resultado.ToString(0, resultado.Length - 1));
            }

            resultado.Append("]");
            pruebaDeFtp.Disconnect();
            return Content(resultado.ToString(), "application/json");
        }
#endregion
    }
}