using Microsoft.AspNetCore.Authentication;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Codium.Web.Controllers
{
    /// <summary>
    /// Nuevo / Editar Cedas Original
    /// </summary>
    [ApiExplorerSettings(IgnoreApi = true)]
    public class NuevoEditarCedasOriginal : Controller
    {
#region Variables Privadas
        /// <summary>
        /// Objeto para encriptar y desencriptar valores
        /// </summary>
        private readonly Microsoft.AspNetCore.DataProtection.IDataProtector _protector;
        /// <summary>
        /// Acceso a la configuración de la aplicación
        /// </summary>
        private readonly Microsoft.Extensions.Configuration.IConfiguration _configuracion;
        /// <summary>
        /// Variable con el idioma de la aplicación
        /// </summary>
        private readonly Microsoft.Extensions.Localization.IStringLocalizer<PruebaDeExcel.RecursosCompartidos> _idioma;
        /// <summary>
        /// Opciones de Idioma
        /// </summary>
        private readonly Microsoft.Extensions.Options.IOptions<Microsoft.AspNetCore.Builder.RequestLocalizationOptions> _opcionesDeIdioma;
        /// <summary>
        /// Web Host Environment
        /// </summary>        
        private readonly IWebHostEnvironment _env;
        /// <summary>
        /// MySQL®
        /// </summary>
        private readonly Codium.Web.Models.IMysqlrUnitOfWork _mysqlr;
        /// <summary>
        /// Prueba de Oracle
        /// </summary>
        private readonly Codium.Web.Models.IPruebaDeOracleUnitOfWork _pruebaDeOracle;
        /// <summary>
        /// SQLite
        /// </summary>
        private readonly Codium.Web.Models.ISqliteUnitOfWork _sqlite;
#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name = "env">Web Host Environment</param>
        /// <param name = "protector">Objeto para encriptar y desencriptar valores</param>
        /// <param name = "configuracion">Acceso a la configuración de la aplicación</param>
        /// <param name = "idioma">Variable con el idioma de la aplicación</param>
        /// <param name = "opcionesDeIdioma">Opciones de Idioma</param>
        /// <param name = "mysqlr">MySQL®</param>
        /// <param name = "pruebaDeOracle">Prueba de Oracle</param>
        /// <param name = "sqlite">SQLite</param>
        public NuevoEditarCedasOriginal(IWebHostEnvironment env, Microsoft.AspNetCore.DataProtection.IDataProtectionProvider protector, Microsoft.Extensions.Configuration.IConfiguration configuracion, Microsoft.Extensions.Localization.IStringLocalizer<PruebaDeExcel.RecursosCompartidos> idioma, Microsoft.Extensions.Options.IOptions<Microsoft.AspNetCore.Builder.RequestLocalizationOptions> opcionesDeIdioma, Codium.Web.Models.IMysqlrUnitOfWork mysqlr, Codium.Web.Models.IPruebaDeOracleUnitOfWork pruebaDeOracle, Codium.Web.Models.ISqliteUnitOfWork sqlite)
        {
            this._env = env;
            this._protector = protector.CreateProtector("URLProtection");
            this._configuracion = configuracion;
            this._idioma = idioma;
            this._opcionesDeIdioma = opcionesDeIdioma;
            this._mysqlr = mysqlr;
            this._pruebaDeOracle = pruebaDeOracle;
            this._sqlite = sqlite;
        }

#endregion
#region Métodos
        /// <summary>
        /// Fuente de Datos Recuperar Cedas Original
        /// </summary>
        /// <param name = "llavePrimariaCedasOriginal">Llave Primaria (Cedas Original) Igual a (=?)</param>
        /// <param name = "pagina">Página</param>
        /// <param name = "tamanoDePagina">Tamaño de Página</param>
        /// <param name = "ordenarPor">Ordenar por</param>
        /// <param name = "tipo">Tipo</param>
        /// <returns>List</returns>
        [Authorize(AuthenticationSchemes = Microsoft.AspNetCore.Authentication.Facebook.FacebookDefaults.AuthenticationScheme + "," + Microsoft.AspNetCore.Authentication.OpenIdConnect.OpenIdConnectDefaults.AuthenticationScheme)]
        [IgnoreAntiforgeryToken]
        [HttpPost]
        public IActionResult FuenteDeDatosRecuperarCedasOriginal(string llavePrimariaCedasOriginal, int pagina, int tamanoDePagina, string ordenarPor, string tipo)
        {
            return Json(this._mysqlr.CedasOriginal.DetalleCedasOriginal(PruebaDeExcel.Funciones.TryParse<long?>((llavePrimariaCedasOriginal != null && llavePrimariaCedasOriginal != "" && llavePrimariaCedasOriginal != "_" ? _protector.Unprotect(llavePrimariaCedasOriginal) : ""), 0L), pagina, tamanoDePagina, ordenarPor, tipo));
        }

        /// <summary>
        /// Fuente de Datos Recuperar Cedas Original
        /// </summary>
        /// <param name = "llavePrimariaCedasOriginal">Llave Primaria (Cedas Original) Igual a (=?)</param>
        /// <param name = "pagina">Página</param>
        /// <param name = "tamanoDePagina">Tamaño de Página</param>
        /// <returns>List</returns>
        [Authorize(AuthenticationSchemes = Microsoft.AspNetCore.Authentication.Facebook.FacebookDefaults.AuthenticationScheme + "," + Microsoft.AspNetCore.Authentication.OpenIdConnect.OpenIdConnectDefaults.AuthenticationScheme)]
        [IgnoreAntiforgeryToken]
        [HttpPost]
        public IActionResult FuenteDeDatosRecuperarCedasOriginalCompleto(string llavePrimariaCedasOriginal, int pagina, int tamanoDePagina)
        {
            string ordenarPor = Request.Form["sort[0][field]"].ToString();
            string tipo = Request.Form["sort[0][dir]"].ToString().ToUpper();
            return Json(new
            {
            total = System.Math.Ceiling((double)this._mysqlr.CedasOriginal.DetalleCedasOriginalTotal(PruebaDeExcel.Funciones.TryParse<long?>((llavePrimariaCedasOriginal != null && llavePrimariaCedasOriginal != "" && llavePrimariaCedasOriginal != "_" ? _protector.Unprotect(llavePrimariaCedasOriginal) : ""), 0L)) / (double)tamanoDePagina), datos = this._mysqlr.CedasOriginal.DetalleCedasOriginal(PruebaDeExcel.Funciones.TryParse<long?>((llavePrimariaCedasOriginal != null && llavePrimariaCedasOriginal != "" && llavePrimariaCedasOriginal != "_" ? _protector.Unprotect(llavePrimariaCedasOriginal) : ""), 0L), pagina, tamanoDePagina, ordenarPor, tipo)}

            );
        }

        /// <summary>
        /// Menú por base de datos
        /// </summary>
        /// <param name = "datos">Lista de datos para llenar el menú</param>
        /// <param name = "valor">Valor con el que se filtra la lista de elementos</param>
        /// <returns>String</returns>
        private string MenuPorBaseDeDatos(List<Codium.Web.Models.MysqlrRepositorios.DTO.ListaDeMenus> datos, Int16? valor)
        {
            System.Text.StringBuilder elementos = new System.Text.StringBuilder();
            foreach (Codium.Web.Models.MysqlrRepositorios.DTO.ListaDeMenus listaDeMenus in datos.Where(x => x.PadreMenus == valor).ToList())
            {
                elementos.Append("<li");
                elementos.Append(" class=\"" + listaDeMenus.EstiloMenus + "\"");
                elementos.Append(">");
                elementos.Append("<a href=\"");
                elementos.Append(listaDeMenus.LigaMenus);
                elementos.Append("\">");
                if (listaDeMenus.IconoMenus != null && listaDeMenus.IconoMenus != "")
                {
                    elementos.Append("<img src=\"" + listaDeMenus.IconoMenus + "\" style=\"height:28px;margin-right:5px;\" />");
                }

                elementos.Append(listaDeMenus.NombreMenus);
                elementos.Append("</a>");
                if (datos.Count(x => x.PadreMenus == listaDeMenus.IdentificadorMenus) > 0)
                {
                    elementos.Append("<ul>");
                    elementos.Append(this.MenuPorBaseDeDatos(datos, listaDeMenus.IdentificadorMenus));
                    elementos.Append("</ul>");
                }

                elementos.Append("</li>");
            }

            return elementos.ToString();
        }

        /// <summary>
        /// Acción principal de la clase, solo funciona con GET.
        /// </summary>
        /// <param name = "identificador">identificador</param>
        [HttpGet]
        [Authorize(AuthenticationSchemes = Microsoft.AspNetCore.Authentication.Facebook.FacebookDefaults.AuthenticationScheme + "," + Microsoft.AspNetCore.Authentication.OpenIdConnect.OpenIdConnectDefaults.AuthenticationScheme + "," + Microsoft.AspNetCore.Authentication.Cookies.CookieAuthenticationDefaults.AuthenticationScheme)]
        [Route("cedas_originales/nuevo")]
        [Route("cedas_originales/editar/{identificador}")]
        public IActionResult Index(string identificador)
        {
            // Menús
            ViewData["ListaMenus"] = this._mysqlr.Menus.ListaDeMenus(0, 0, "", "");
            // Menú por base de datos
            ViewData["menuPorBaseDeDatos"] = this.MenuPorBaseDeDatos((List<Codium.Web.Models.MysqlrRepositorios.DTO.ListaDeMenus>)ViewData["ListaMenus"], null);
            ViewData["LanguageIdentifier"] = this._idioma["LanguageIdentifier"].Value;
            ViewData["Title"] = this._idioma["NuevoEditarCedasOriginal"].Value;
            return View();
        }
#endregion
    }
}