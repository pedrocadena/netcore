using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Threading.Tasks;
namespace Codium.Web.Controllers
{
    /// <summary>
    /// Login
    /// </summary>
    [ApiExplorerSettings(IgnoreApi = true)]
    public class Login : Controller
    {
#region Variables Privadas
        /// <summary>
        /// Web Host Environment
        /// </summary>        
        private readonly IWebHostEnvironment _env;
        /// <summary>
        /// Razor View To StringRenderer
        /// </summary>
        private readonly IRazorViewToStringRenderer _renderer;
        /// <summary>
        /// Objeto para encriptar y desencriptar valores
        /// </summary>
        private readonly IDataProtector _protector;
        /// <summary>
        /// Acceso a la configuración de la aplicación
        /// </summary>
        private readonly Microsoft.Extensions.Configuration.IConfiguration _configuracion;
        /// <summary>
        /// Variable con el idioma de la aplicación
        /// </summary>
        private readonly Microsoft.Extensions.Localization.IStringLocalizer<PruebaDeExcel.RecursosCompartidos> _idioma;
        /// <summary>
        /// Opciones de Idioma
        /// </summary>
        private readonly Microsoft.Extensions.Options.IOptions<Microsoft.AspNetCore.Builder.RequestLocalizationOptions> _opcionesDeIdioma;
        /// <summary>
        /// MySQL®
        /// </summary>
        private readonly Codium.Web.Models.IMysqlrUnitOfWork _mysqlr;
        /// <summary>
        /// Prueba de Oracle
        /// </summary>
        private readonly Codium.Web.Models.IPruebaDeOracleUnitOfWork _pruebaDeOracle;
        /// <summary>
        /// SQLite
        /// </summary>
        private readonly Codium.Web.Models.ISqliteUnitOfWork _sqlite;
#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name = "env">Web Host Environment</param>
        /// <param name = "renderer">Razor View To String Renderer</param>
        /// <param name = "protector">Objeto para encriptar y desencriptar valores</param>
        /// <param name = "configuracion">Acceso a la configuración de la aplicación</param>
        /// <param name = "idioma">Variable con el idioma de la aplicación</param>
        /// <param name = "opcionesDeIdioma">Opciones de Idioma</param>
        /// <param name = "mysqlr">MySQL®</param>
        /// <param name = "pruebaDeOracle">Prueba de Oracle</param>
        /// <param name = "sqlite">SQLite</param>
        public Login(IWebHostEnvironment env, IRazorViewToStringRenderer renderer, IDataProtectionProvider protector, Microsoft.Extensions.Configuration.IConfiguration configuracion, Microsoft.Extensions.Localization.IStringLocalizer<PruebaDeExcel.RecursosCompartidos> idioma, Microsoft.Extensions.Options.IOptions<Microsoft.AspNetCore.Builder.RequestLocalizationOptions> opcionesDeIdioma, Codium.Web.Models.IMysqlrUnitOfWork mysqlr, Codium.Web.Models.IPruebaDeOracleUnitOfWork pruebaDeOracle, Codium.Web.Models.ISqliteUnitOfWork sqlite)
        {
            this._env = env;
            this._renderer = renderer;
            this._protector = protector.CreateProtector("URLProtection");
            this._configuracion = configuracion;
            this._idioma = idioma;
            this._opcionesDeIdioma = opcionesDeIdioma;
            this._mysqlr = mysqlr;
            this._pruebaDeOracle = pruebaDeOracle;
            this._sqlite = sqlite;
        }

#endregion
#region Métodos
        /// <summary>
        /// Función que valida las credenciales del usuario.
        /// </summary>
        /// <returns>Json</returns>
        [Route("Login/Validar")]
        [HttpPost]
        public IActionResult Validar()
        {
            try
            {
                List<Codium.Web.Models.MysqlrRepositorios.DTO.Login> lista = this._mysqlr.Usuarios.Login(PruebaDeExcel.Funciones.TryParse<string>(Request.Form["usuarioFormularioDeLogin"], ""), PruebaDeExcel.Funciones.TryParse<string>(Request.Form["contrasenaFormularioDeLogin"], ""), 0, 0, "", "");
                Codium.Web.Models.MysqlrRepositorios.DTO.Login elemento = lista.First();
                if (elemento.IdentificadorUsuarios != null)
                {
                    HttpContext.Session.SetString("Usuario", elemento.IdentificadorUsuarios.ToString()); // Usuario
                }

                if (elemento.NombreRoles != null)
                {
                    HttpContext.Session.SetString("Rol", elemento.NombreRoles.ToString()); // Rol
                }

                HttpContext.Session.SetString("Login", "1");
                string url = "/cedas";
                var authClaims = new List<System.Security.Claims.Claim>{new System.Security.Claims.Claim(System.IdentityModel.Tokens.Jwt.JwtRegisteredClaimNames.Sub, elemento.IdentificadorUsuarios.ToString()), new System.Security.Claims.Claim(System.Security.Claims.ClaimTypes.Role as string, elemento.NombreRoles), new System.Security.Claims.Claim(System.IdentityModel.Tokens.Jwt.JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())};
                var authSigningKey = new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(this._configuracion["PruebaDeJwt:ClaveSecreta"]));
                var jwtSecurityToken = new System.IdentityModel.Tokens.Jwt.JwtSecurityToken(issuer: this._configuracion["PruebaDeJwt:EmisorValido"], audience: this._configuracion["PruebaDeJwt:AudienciaValida"], expires: System.DateTime.Now.AddDays(1), claims: authClaims, signingCredentials: new Microsoft.IdentityModel.Tokens.SigningCredentials(authSigningKey, Microsoft.IdentityModel.Tokens.SecurityAlgorithms.HmacSha256));
                var token = new System.IdentityModel.Tokens.Jwt.JwtSecurityTokenHandler().WriteToken(jwtSecurityToken);
                HttpContext.Response.Cookies.Append("jwt.token", token, new CookieOptions{IsEssential = true, Expires = jwtSecurityToken.ValidTo});
                return Json(new
                {
                data = elemento, url = url, token = token
                }

                );
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
#endregion
    }
}