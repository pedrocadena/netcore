using Codium.Web.Models.DirectorioActivoEntidades;

using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Codium.Web.Controllers
{
    /// <summary>
    /// Login por directorio activo
    /// </summary>
    [ApiExplorerSettings(IgnoreApi = true)]
    public class LoginPorDirectorioActivo : Controller
    {
#region Variables Privadas
        /// <summary>
        /// Web Host Environment
        /// </summary>        
        private readonly IWebHostEnvironment _env;
        /// <summary>
        /// Razor View To StringRenderer
        /// </summary>
        private readonly IRazorViewToStringRenderer _renderer;
        /// <summary>
        /// Objeto para encriptar y desencriptar valores
        /// </summary>
        private readonly IDataProtector _protector;
        /// <summary>
        /// Acceso a la configuración de la aplicación
        /// </summary>
        private readonly Microsoft.Extensions.Configuration.IConfiguration _configuracion;
        /// <summary>
        /// Variable con el idioma de la aplicación
        /// </summary>
        private readonly Microsoft.Extensions.Localization.IStringLocalizer<PruebaDeExcel.RecursosCompartidos> _idioma;
        /// <summary>
        /// Opciones de Idioma
        /// </summary>
        private readonly Microsoft.Extensions.Options.IOptions<Microsoft.AspNetCore.Builder.RequestLocalizationOptions> _opcionesDeIdioma;
#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name = "env">Web Host Environment</param>
        /// <param name = "renderer">Razor View To String Renderer</param>
        /// <param name = "protector">Objeto para encriptar y desencriptar valores</param>
        /// <param name = "configuracion">Acceso a la configuración de la aplicación</param>
        /// <param name = "idioma">Variable con el idioma de la aplicación</param>
        /// <param name = "opcionesDeIdioma">Opciones de Idioma</param>
        public LoginPorDirectorioActivo(IWebHostEnvironment env, IRazorViewToStringRenderer renderer, IDataProtectionProvider protector, Microsoft.Extensions.Configuration.IConfiguration configuracion, Microsoft.Extensions.Localization.IStringLocalizer<PruebaDeExcel.RecursosCompartidos> idioma, Microsoft.Extensions.Options.IOptions<Microsoft.AspNetCore.Builder.RequestLocalizationOptions> opcionesDeIdioma)
        {
            this._env = env;
            this._renderer = renderer;
            this._protector = protector.CreateProtector("URLProtection");
            this._configuracion = configuracion;
            this._idioma = idioma;
            this._opcionesDeIdioma = opcionesDeIdioma;
        }

#endregion
#region Métodos
        /// <summary>
        /// Función que valida las credenciales del usuario.
        /// </summary>
        /// <returns>Json</returns>
        [Route("LoginPorDirectorioActivo/Validar")]
        [HttpPost]
        public IActionResult Validar()
        {
            try
            {
                string nuevoDN = "";
                string[] dnArray = this._configuracion["AppSettings:DirectorioActivoNombresDistinguidosDn"].Split(',');
                foreach (string dn in dnArray)
                {
                    if (dn.Split('=').GetValue(0).ToString().ToUpper() == "DC")
                    {
                        nuevoDN += dn + ",";
                    }
                }

                if (nuevoDN != "")
                {
                    nuevoDN = nuevoDN.Substring(0, nuevoDN.Length - 1);
                }

                System.DirectoryServices.DirectoryEntry directoryEntry = new System.DirectoryServices.DirectoryEntry("LDAP://" + this._configuracion["AppSettings:DirectorioActivo"] + ":" + this._configuracion["AppSettings:DirectorioActivoPuerto"] + "/CN=" + Request.Form["usuarioFormularioDeLogin"] + "," + nuevoDN, "CN=" + Request.Form["usuarioFormularioDeLogin"] + "," + nuevoDN, Request.Form["contrasenaFormularioDeLogin"], System.DirectoryServices.AuthenticationTypes.None);
                DirectorioActivoColeccion elemento = new DirectorioActivoColeccion();
                if (directoryEntry.Properties.Contains("cn"))
                {
                    elemento.Cn = (string)directoryEntry.Properties["cn"].Value;
                }

                if (directoryEntry.Properties.Contains("description"))
                {
                    elemento.Description = (string)directoryEntry.Properties["description"].Value;
                }

                if (directoryEntry.Properties.Contains("displayName"))
                {
                    elemento.Displayname = (string)directoryEntry.Properties["displayName"].Value;
                }

                if (directoryEntry.Properties.Contains("gidNumber"))
                {
                    elemento.Gidnumber = (int)directoryEntry.Properties["gidNumber"].Value;
                }

                if (directoryEntry.Properties.Contains("givenName"))
                {
                    elemento.Givenname = (string)directoryEntry.Properties["givenName"].Value;
                }

                if (directoryEntry.Properties.Contains("homeDirectory"))
                {
                    elemento.Homedirectory = (string)directoryEntry.Properties["homeDirectory"].Value;
                }

                if (directoryEntry.Properties.Contains("initials"))
                {
                    elemento.Initials = (string)directoryEntry.Properties["initials"].Value;
                }

                if (directoryEntry.Properties.Contains("mail"))
                {
                    elemento.Mail = (string)directoryEntry.Properties["mail"].Value;
                }

                if (directoryEntry.Properties.Contains("o"))
                {
                    elemento.O = (string)directoryEntry.Properties["o"].Value;
                }

                if (directoryEntry.Properties.Contains("sn"))
                {
                    elemento.Sn = (string)directoryEntry.Properties["sn"].Value;
                }

                if (directoryEntry.Properties.Contains("telephoneNumber"))
                {
                    elemento.Telephonenumber = (string)directoryEntry.Properties["telephoneNumber"].Value;
                }

                if (directoryEntry.Properties.Contains("uid"))
                {
                    elemento.Uid = (string)directoryEntry.Properties["uid"].Value;
                }

                if (directoryEntry.Properties.Contains("uidNumber"))
                {
                    elemento.Uidnumber = (int)directoryEntry.Properties["uidNumber"].Value;
                }

                if (directoryEntry.Properties.Contains("userPassword"))
                {
                    elemento.Userpassword = (byte[])directoryEntry.Properties["userPassword"].Value;
                }

                if (elemento.Cn != null)
                {
                    HttpContext.Session.SetString("Rol", elemento.Cn.ToString()); // Rol
                }

                HttpContext.Session.SetString("LoginPorDirectorioActivo", "1");
                string url = "/cedas";
                var authClaims = new List<System.Security.Claims.Claim>{new System.Security.Claims.Claim(System.IdentityModel.Tokens.Jwt.JwtRegisteredClaimNames.Sub, elemento.Cn), new System.Security.Claims.Claim(System.IdentityModel.Tokens.Jwt.JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())};
                var authSigningKey = new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(this._configuracion["PruebaDeJwt:ClaveSecreta"]));
                var jwtSecurityToken = new System.IdentityModel.Tokens.Jwt.JwtSecurityToken(issuer: this._configuracion["PruebaDeJwt:EmisorValido"], audience: this._configuracion["PruebaDeJwt:AudienciaValida"], expires: System.DateTime.Now.AddDays(1), claims: authClaims, signingCredentials: new Microsoft.IdentityModel.Tokens.SigningCredentials(authSigningKey, Microsoft.IdentityModel.Tokens.SecurityAlgorithms.HmacSha256));
                var token = new System.IdentityModel.Tokens.Jwt.JwtSecurityTokenHandler().WriteToken(jwtSecurityToken);
                HttpContext.Response.Cookies.Append("jwt.token", token, new CookieOptions{IsEssential = true, Expires = jwtSecurityToken.ValidTo});
                return Json(new
                {
                data = elemento, url = url, token = token
                }

                );
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
#endregion
    }
}