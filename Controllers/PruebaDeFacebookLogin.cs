using Microsoft.AspNetCore.Authentication;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
namespace Codium.Web.Controllers
{
    /// <summary>
    /// Prueba de Facebook Login
    /// </summary>
    [ApiExplorerSettings(IgnoreApi = true)]
    public class PruebaDeFacebookLoginController : Controller
    {
#region Métodos
        /// <summary>
        /// Login
        /// </summary>
        /// <param name = "returnURL">Return URL</param>
        /// <returns>Challenge</returns>
        [HttpPost]
        public IActionResult Login(string returnURL)
        {
            return new ChallengeResult(Microsoft.AspNetCore.Authentication.Facebook.FacebookDefaults.AuthenticationScheme, new AuthenticationProperties{RedirectUri = returnURL == null || returnURL == "" ? "/" : returnURL});
        }
#endregion
    }
}