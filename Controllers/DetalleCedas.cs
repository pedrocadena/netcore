using Codium.Web.Models;

using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Codium.Web.Controllers
{
    /// <summary>
    /// Detalle Cedas
    /// </summary>
    [ApiExplorerSettings(IgnoreApi = true)]
    public class DetalleCedas : Controller
    {
#region Variables Privadas
        /// <summary>
        /// Web Host Environment
        /// </summary>        
        private readonly IWebHostEnvironment _env;
        /// <summary>
        /// Razor View To StringRenderer
        /// </summary>
        private readonly IRazorViewToStringRenderer _renderer;
        /// <summary>
        /// Objeto para encriptar y desencriptar valores
        /// </summary>
        private readonly IDataProtector _protector;
        /// <summary>
        /// Acceso a la configuración de la aplicación
        /// </summary>
        private readonly Microsoft.Extensions.Configuration.IConfiguration _configuracion;
        /// <summary>
        /// Variable con el idioma de la aplicación
        /// </summary>
        private readonly Microsoft.Extensions.Localization.IStringLocalizer<PruebaDeExcel.RecursosCompartidos> _idioma;
        /// <summary>
        /// Opciones de Idioma
        /// </summary>
        private readonly Microsoft.Extensions.Options.IOptions<Microsoft.AspNetCore.Builder.RequestLocalizationOptions> _opcionesDeIdioma;
        /// <summary>
        /// MySQL®
        /// </summary>
        private readonly Codium.Web.Models.Mysqlr _mysqlrConexion;
        /// <summary>
        /// MySQL®
        /// </summary>
        private readonly Codium.Web.Models.IMysqlrUnitOfWork _mysqlr;
        /// <summary>
        /// SQLite
        /// </summary>
        private readonly Codium.Web.Models.Sqlite _sqliteConexion;
        /// <summary>
        /// SQLite
        /// </summary>
        private readonly Codium.Web.Models.ISqliteUnitOfWork _sqlite;
#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name = "env">Web Host Environment</param>
        /// <param name = "renderer">Razor View To String Renderer</param>
        /// <param name = "protector">Objeto para encriptar y desencriptar valores</param>
        /// <param name = "configuracion">Acceso a la configuración de la aplicación</param>
        /// <param name = "idioma">Variable con el idioma de la aplicación</param>
        /// <param name = "opcionesDeIdioma">Opciones de Idioma</param>
        /// <param name = "mysqlrConexion">MySQL®</param>
        /// <param name = "mysqlr">MySQL®</param>
        /// <param name = "sqliteConexion">SQLite</param>
        /// <param name = "sqlite">SQLite</param>
        public DetalleCedas(IWebHostEnvironment env, IRazorViewToStringRenderer renderer, IDataProtectionProvider protector, Microsoft.Extensions.Configuration.IConfiguration configuracion, Microsoft.Extensions.Localization.IStringLocalizer<PruebaDeExcel.RecursosCompartidos> idioma, Microsoft.Extensions.Options.IOptions<Microsoft.AspNetCore.Builder.RequestLocalizationOptions> opcionesDeIdioma, Codium.Web.Models.Mysqlr mysqlrConexion, Codium.Web.Models.IMysqlrUnitOfWork mysqlr, Codium.Web.Models.Sqlite sqliteConexion, Codium.Web.Models.ISqliteUnitOfWork sqlite)
        {
            this._env = env;
            this._renderer = renderer;
            this._protector = protector.CreateProtector("URLProtection");
            this._configuracion = configuracion;
            this._idioma = idioma;
            this._opcionesDeIdioma = opcionesDeIdioma;
            this._mysqlrConexion = mysqlrConexion;
            this._mysqlr = mysqlr;
            this._sqliteConexion = sqliteConexion;
            this._sqlite = sqlite;
        }

#endregion
#region Métodos
        /// <summary>
        /// Nuevo Notificaciones Web
        /// </summary>
        /// <param name = "identificador">Identificador</param>
        /// <returns>Json</returns>
        [Route("DetalleCedas/NuevoNotificacionesWeb/{identificador}")]
        [HttpPost]
        public IActionResult NuevoNotificacionesWeb(string identificador)
        {
            try
            {
                System.Text.StringBuilder resultado = new System.Text.StringBuilder();
                resultado.Append("{");
                resultado.Append("\"nuevoNotificacionesWeb\":{");
                System.Net.WebClient nuevoNotificacionesWeb = new System.Net.WebClient();
                nuevoNotificacionesWeb.Headers.Add(System.Net.HttpRequestHeader.Authorization, "api_key=" + this._configuracion["AppSettings:PruebaDeNotificacion"]);
                List<object> nuevoNotificacionesWebLista = new List<object>();
                List<Codium.Web.Models.MysqlrRepositorios.DTO.DetalleCedas> nuevoNotificacionesWebDetalleCedas = this._mysqlr.Cedas.DetalleCedas(1L, 0, 0, "", "");
                foreach (Codium.Web.Models.MysqlrRepositorios.DTO.DetalleCedas nuevoNotificacionesWebElemento in nuevoNotificacionesWebDetalleCedas)
                {
                    nuevoNotificacionesWebLista.Add(Newtonsoft.Json.JsonConvert.DeserializeObject(nuevoNotificacionesWeb.UploadString("https://api.pushalert.co/rest/v1/segment/" + this._idioma["Segmento"].Value + "/send", "title=" + nuevoNotificacionesWebElemento.ClaveCedasSaderCedas + "&message=" + nuevoNotificacionesWebElemento.EstadoCedas + "&url=" + nuevoNotificacionesWebElemento.IdCedaSegalmexCedas)));
                }

                resultado.Append("\"resultado\":" + Newtonsoft.Json.JsonConvert.SerializeObject(nuevoNotificacionesWebLista));
                resultado.Append("}");
                resultado.Append("}");
                resultado.Append(",");
                resultado.Append("\"guardarCedas\":{");
                Codium.Web.Models.MysqlrEntidades.Cedas tablaGuardarCedas1 = null;
                using (var transaccion = new System.Transactions.TransactionScope())
                {
                    bool esNuevoTablaGuardarCedas1 = true;
                    long llavePrimaria = PruebaDeExcel.Funciones.TryParse<long>((identificador != null && identificador != "" && identificador != "_" ? _protector.Unprotect(identificador) : ""));
                    tablaGuardarCedas1 = this._mysqlrConexion.Cedas.FirstOrDefault(x => x.LlavePrimaria == llavePrimaria);
                    if (tablaGuardarCedas1 == null)
                    {
                        tablaGuardarCedas1 = new Codium.Web.Models.MysqlrEntidades.Cedas();
                        this._mysqlrConexion.Cedas.Add(tablaGuardarCedas1);
                    }
                    else
                    {
                        esNuevoTablaGuardarCedas1 = false;
                    }

                    if (esNuevoTablaGuardarCedas1)
                    {
                        string idCedaSegalmexUnico = PruebaDeExcel.Funciones.TryParse<string>(Request.Form["editarIdCedaSegalmexCedasDetalleCedas"]);
                        if (this._mysqlrConexion.Cedas.Count(x => x.IdCedaSegalmex == idCedaSegalmexUnico) > 0)
                        {
                            throw new Exception("NotUnique");
                        }
                    }

                    tablaGuardarCedas1.ClaveCedasSader = PruebaDeExcel.Funciones.TryParse<string>(Request.Form["editarClaveCedasSaderCedasDetalleCedas"]);
                    tablaGuardarCedas1.Decimales = PruebaDeExcel.Funciones.TryParse<decimal?>(Request.Form["editarDecimalesCedasDetalleCedas"]);
                    tablaGuardarCedas1.Estado = PruebaDeExcel.Funciones.TryParse<string>(Request.Form["editarEstadoCedasDetalleCedas"]);
                    tablaGuardarCedas1.IdCedaSegalmex = PruebaDeExcel.Funciones.TryParse<string>(Request.Form["editarIdCedaSegalmexCedasDetalleCedas"]);
                    if (Request.Form.Files["editarLogotipoCedasDetalleCedas"] != null && Request.Form.Files["editarLogotipoCedasDetalleCedas"].FileName != "" && Request.Form.Files["editarLogotipoCedasDetalleCedas"].Length > 0)
                    {
                        System.IO.Stream stream = Request.Form.Files["editarLogotipoCedasDetalleCedas"].OpenReadStream();
                        byte[] binary = new byte[stream.Length];
                        stream.Read(binary, 0, (int)stream.Length);
                        tablaGuardarCedas1.Logotipo = binary;
                        tablaGuardarCedas1.Columna4FileName = Request.Form.Files["editarLogotipoCedasDetalleCedas"].FileName;
                        tablaGuardarCedas1.Columna4Size = (int)Request.Form.Files["editarLogotipoCedasDetalleCedas"].Length;
                        tablaGuardarCedas1.Columna4MimeType = Request.Form.Files["editarLogotipoCedasDetalleCedas"].ContentType;
                    }

                    if (esNuevoTablaGuardarCedas1)
                    {
                        tablaGuardarCedas1.Estatus = 1;
                    }

                    if (esNuevoTablaGuardarCedas1)
                    {
                        tablaGuardarCedas1.FechaDeCreacion = PruebaDeExcel.Funciones.TryParse<System.DateTime>(System.DateTime.Now);
                    }

                    if (!esNuevoTablaGuardarCedas1)
                    {
                        tablaGuardarCedas1.FechaDeModificacion = PruebaDeExcel.Funciones.TryParse<System.DateTime?>(System.DateTime.Now);
                    }

                    this._mysqlrConexion.SaveChanges();
                    resultado.Append("\"tablaGuardarCedas1\":");
                    resultado.Append(Newtonsoft.Json.JsonConvert.SerializeObject(tablaGuardarCedas1));
                    transaccion.Complete();
                }

                resultado.Append("}");
                return Content(resultado.ToString(), "application/json");
            }
            catch (Exception ex)
            {
                return StatusCode(505, ex.Message);
            }
        }
#endregion
    }
}