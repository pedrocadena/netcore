using Codium.Web.Models;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Codium.Web.Controllers
{
    /// <summary>
    /// Tabla de Datos Cedas Original
    /// </summary>
    [ApiExplorerSettings(IgnoreApi = true)]
    public class TablaDeDatosCedasOriginal : Controller
    {
#region Variables Privadas
        /// <summary>
        /// Web Host Environment
        /// </summary>        
        private readonly IWebHostEnvironment _env;
        /// <summary>
        /// Razor View To StringRenderer
        /// </summary>
        private readonly IRazorViewToStringRenderer _renderer;
        /// <summary>
        /// Objeto para encriptar y desencriptar valores
        /// </summary>
        private readonly IDataProtector _protector;
        /// <summary>
        /// Acceso a la configuración de la aplicación
        /// </summary>
        private readonly Microsoft.Extensions.Configuration.IConfiguration _configuracion;
        /// <summary>
        /// Variable con el idioma de la aplicación
        /// </summary>
        private readonly Microsoft.Extensions.Localization.IStringLocalizer<PruebaDeExcel.RecursosCompartidos> _idioma;
        /// <summary>
        /// Opciones de Idioma
        /// </summary>
        private readonly Microsoft.Extensions.Options.IOptions<Microsoft.AspNetCore.Builder.RequestLocalizationOptions> _opcionesDeIdioma;
        /// <summary>
        /// MySQL®
        /// </summary>
        private readonly Codium.Web.Models.Mysqlr _mysqlrConexion;
        /// <summary>
        /// MySQL®
        /// </summary>
        private readonly Codium.Web.Models.IMysqlrUnitOfWork _mysqlr;
        /// <summary>
        /// SQLite
        /// </summary>
        private readonly Codium.Web.Models.Sqlite _sqliteConexion;
        /// <summary>
        /// SQLite
        /// </summary>
        private readonly Codium.Web.Models.ISqliteUnitOfWork _sqlite;
#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name = "env">Web Host Environment</param>
        /// <param name = "renderer">Razor View To String Renderer</param>
        /// <param name = "protector">Objeto para encriptar y desencriptar valores</param>
        /// <param name = "configuracion">Acceso a la configuración de la aplicación</param>
        /// <param name = "idioma">Variable con el idioma de la aplicación</param>
        /// <param name = "opcionesDeIdioma">Opciones de Idioma</param>
        /// <param name = "mysqlrConexion">MySQL®</param>
        /// <param name = "mysqlr">MySQL®</param>
        /// <param name = "sqliteConexion">SQLite</param>
        /// <param name = "sqlite">SQLite</param>
        public TablaDeDatosCedasOriginal(IWebHostEnvironment env, IRazorViewToStringRenderer renderer, IDataProtectionProvider protector, Microsoft.Extensions.Configuration.IConfiguration configuracion, Microsoft.Extensions.Localization.IStringLocalizer<PruebaDeExcel.RecursosCompartidos> idioma, Microsoft.Extensions.Options.IOptions<Microsoft.AspNetCore.Builder.RequestLocalizationOptions> opcionesDeIdioma, Codium.Web.Models.Mysqlr mysqlrConexion, Codium.Web.Models.IMysqlrUnitOfWork mysqlr, Codium.Web.Models.Sqlite sqliteConexion, Codium.Web.Models.ISqliteUnitOfWork sqlite)
        {
            this._env = env;
            this._renderer = renderer;
            this._protector = protector.CreateProtector("URLProtection");
            this._configuracion = configuracion;
            this._idioma = idioma;
            this._opcionesDeIdioma = opcionesDeIdioma;
            this._mysqlrConexion = mysqlrConexion;
            this._mysqlr = mysqlr;
            this._sqliteConexion = sqliteConexion;
            this._sqlite = sqlite;
        }

#endregion
#region Métodos
        /// <summary>
        /// Borrar Cedas Original
        /// </summary>
        /// <returns>Json</returns>
        [Route("TablaDeDatosCedasOriginal/BorrarCedasOriginal")]
        [HttpPost]
        [Authorize(AuthenticationSchemes = Microsoft.AspNetCore.Authentication.Facebook.FacebookDefaults.AuthenticationScheme + "," + Microsoft.AspNetCore.Authentication.OpenIdConnect.OpenIdConnectDefaults.AuthenticationScheme)]
        [IgnoreAntiforgeryToken]
        public IActionResult BorrarCedasOriginal()
        {
            try
            {
                System.Text.StringBuilder resultado = new System.Text.StringBuilder();
                resultado.Append("{");
                this._mysqlr.CedasOriginal.ActualizarCedasOriginal(3, PruebaDeExcel.Funciones.TryParse<long?>(Request.Form["llavePrimaria"], null));
                resultado.Append("\"borrarCedasOriginal\":{");
                resultado.Append("\"resultado\":" + Newtonsoft.Json.JsonConvert.SerializeObject(true));
                resultado.Append("}");
                resultado.Append("}");
                return Content(resultado.ToString(), "application/json");
            }
            catch (Exception ex)
            {
                return StatusCode(505, ex.Message);
            }
        }

        /// <summary>
        /// Deshabilitado Cedas Original
        /// </summary>
        /// <returns>Json</returns>
        [Route("TablaDeDatosCedasOriginal/DeshabilitadoCedasOriginal")]
        [HttpPost]
        [Authorize(AuthenticationSchemes = Microsoft.AspNetCore.Authentication.Facebook.FacebookDefaults.AuthenticationScheme + "," + Microsoft.AspNetCore.Authentication.OpenIdConnect.OpenIdConnectDefaults.AuthenticationScheme)]
        [IgnoreAntiforgeryToken]
        public IActionResult DeshabilitadoCedasOriginal()
        {
            try
            {
                System.Text.StringBuilder resultado = new System.Text.StringBuilder();
                resultado.Append("{");
                this._mysqlr.CedasOriginal.ActualizarCedasOriginal(2, PruebaDeExcel.Funciones.TryParse<long?>(Request.Form["llavePrimaria"], null));
                resultado.Append("\"deshabilitadoCedasOriginal\":{");
                resultado.Append("\"resultado\":" + Newtonsoft.Json.JsonConvert.SerializeObject(true));
                resultado.Append("}");
                resultado.Append("}");
                return Content(resultado.ToString(), "application/json");
            }
            catch (Exception ex)
            {
                return StatusCode(505, ex.Message);
            }
        }

        /// <summary>
        /// Habilitado Cedas Original
        /// </summary>
        /// <returns>Json</returns>
        [Route("TablaDeDatosCedasOriginal/HabilitadoCedasOriginal")]
        [HttpPost]
        [Authorize(AuthenticationSchemes = Microsoft.AspNetCore.Authentication.Facebook.FacebookDefaults.AuthenticationScheme + "," + Microsoft.AspNetCore.Authentication.OpenIdConnect.OpenIdConnectDefaults.AuthenticationScheme)]
        [IgnoreAntiforgeryToken]
        public IActionResult HabilitadoCedasOriginal()
        {
            try
            {
                System.Text.StringBuilder resultado = new System.Text.StringBuilder();
                resultado.Append("{");
                this._mysqlr.CedasOriginal.ActualizarCedasOriginal(1, PruebaDeExcel.Funciones.TryParse<long?>(Request.Form["llavePrimaria"], null));
                resultado.Append("\"habilitadoCedasOriginal\":{");
                resultado.Append("\"resultado\":" + Newtonsoft.Json.JsonConvert.SerializeObject(true));
                resultado.Append("}");
                resultado.Append("}");
                return Content(resultado.ToString(), "application/json");
            }
            catch (Exception ex)
            {
                return StatusCode(505, ex.Message);
            }
        }
#endregion
    }
}