using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Threading.Tasks;
namespace Codium.Web.Controllers
{
    /// <summary>
    /// Disco
    /// </summary>
    [ApiExplorerSettings(IgnoreApi = true)]
    public class Disco : Controller
    {
#region Variables Privadas
        /// <summary>
        /// Web Host Environment
        /// </summary>        
        private readonly IWebHostEnvironment _env;
        /// <summary>
        /// Razor View To StringRenderer
        /// </summary>
        private readonly IRazorViewToStringRenderer _renderer;
        /// <summary>
        /// Objeto para encriptar y desencriptar valores
        /// </summary>
        private readonly IDataProtector _protector;
        /// <summary>
        /// Acceso a la configuración de la aplicación
        /// </summary>
        private readonly Microsoft.Extensions.Configuration.IConfiguration _configuracion;
        /// <summary>
        /// Variable con el idioma de la aplicación
        /// </summary>
        private readonly Microsoft.Extensions.Localization.IStringLocalizer<PruebaDeExcel.RecursosCompartidos> _idioma;
        /// <summary>
        /// Opciones de Idioma
        /// </summary>
        private readonly Microsoft.Extensions.Options.IOptions<Microsoft.AspNetCore.Builder.RequestLocalizationOptions> _opcionesDeIdioma;
#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name = "env">Web Host Environment</param>
        /// <param name = "renderer">Razor View To String Renderer</param>
        /// <param name = "protector">Objeto para encriptar y desencriptar valores</param>
        /// <param name = "configuracion">Acceso a la configuración de la aplicación</param>
        /// <param name = "idioma">Variable con el idioma de la aplicación</param>
        /// <param name = "opcionesDeIdioma">Opciones de Idioma</param>
        public Disco(IWebHostEnvironment env, IRazorViewToStringRenderer renderer, IDataProtectionProvider protector, Microsoft.Extensions.Configuration.IConfiguration configuracion, Microsoft.Extensions.Localization.IStringLocalizer<PruebaDeExcel.RecursosCompartidos> idioma, Microsoft.Extensions.Options.IOptions<Microsoft.AspNetCore.Builder.RequestLocalizationOptions> opcionesDeIdioma)
        {
            this._env = env;
            this._renderer = renderer;
            this._protector = protector.CreateProtector("URLProtection");
            this._configuracion = configuracion;
            this._idioma = idioma;
            this._opcionesDeIdioma = opcionesDeIdioma;
        }

#endregion
#region Métodos
        /// <summary>
        /// Función que recupera las subcarpetas de un folder.
        /// </summary>
        /// <param name = "carpeta">Variable con el folder a buscar</param>
        /// <returns>string</returns>
        private string CarpetasAnidadas(string carpeta)
        {
            System.Text.StringBuilder resultado = new System.Text.StringBuilder();
            string[] lista = System.IO.Directory.GetDirectories(carpeta);
            foreach (string elemento in lista)
            {
                resultado.Append("{");
                resultado.Append("\"name\":" + Newtonsoft.Json.JsonConvert.SerializeObject(new System.IO.DirectoryInfo(elemento).Name) + ",\"expanded\":true,");
                resultado.Append("\"children\":[");
                resultado.Append(CarpetasAnidadas(elemento));
                resultado.Append("]");
                resultado.Append("},");
            }

            if (lista.Length > 0)
            {
                resultado = new System.Text.StringBuilder(resultado.ToString(0, resultado.Length - 1));
            }

            return resultado.ToString();
        }

        /// <summary>
        /// Función para descargar un archivo.
        /// </summary>
        /// <param name = "archivo">Archivo a descargar</param>
        /// <returns>File</returns>
        [Route("Disco/Descargar")]
        [HttpGet]
        public IActionResult Descargar(string archivo)
        {
            return File(System.IO.File.ReadAllBytes(System.IO.Path.Combine(this._env.WebRootPath, archivo)), MimeMapping.MimeUtility.GetMimeMapping(System.IO.Path.Combine(this._env.WebRootPath, archivo)), new System.IO.FileInfo(System.IO.Path.Combine(this._env.WebRootPath, archivo)).Name);
        }

        /// <summary>
        /// Función que borrar carpetas o archivos
        /// </summary>
        /// <param name = "toDelete">Listado de archivos o carpetas a borrar</param>
        /// <returns>Json</returns>
        [Route("Disco/Borrar")]
        [HttpPost]
        public IActionResult Borrar(string toDelete)
        {
            string[] lista = toDelete.Split('|');
            foreach (string elemento in lista)
            {
                if (elemento != "")
                {
                    System.IO.FileAttributes fileAttributes = System.IO.File.GetAttributes(System.IO.Path.Combine(this._env.WebRootPath, elemento));
                    if ((fileAttributes & System.IO.FileAttributes.Directory) == System.IO.FileAttributes.Directory)
                    {
                        System.IO.Directory.Delete(System.IO.Path.Combine(this._env.WebRootPath, elemento), true);
                    }
                    else
                    {
                        System.IO.File.Delete(System.IO.Path.Combine(this._env.WebRootPath, elemento));
                    }
                }
            }

            return Json(true);
        }

        /// <summary>
        /// Función para subir archivos
        /// </summary>
        /// <param name = "carpeta">Variable con el folder a buscar</param>
        /// <param name = "file">Archivo a subir</param>
        /// <returns>Json</returns>
        [Route("Disco/SubirArchivo")]
        [HttpPost]
        public IActionResult SubirArchivo(string carpeta, List<IFormFile> file)
        {
            foreach (var inputFile in file)
            {
                if (inputFile.Length > 0)
                {
                    System.IO.Stream stream = inputFile.OpenReadStream();
                    byte[] binary = new byte[stream.Length];
                    stream.Read(binary, 0, (int)stream.Length);
                    System.IO.File.WriteAllBytes(System.IO.Path.Combine(this._env.WebRootPath, carpeta, inputFile.FileName), binary);
                }
            }

            return Json(true);
        }

        /// <summary>
        /// Función que agregar una carpeta
        /// </summary>
        /// <param name = "carpeta">Variable con el folder a buscar</param>
        /// <param name = "agregarFolder">Nombre de Folder</param>
        /// <returns>Json</returns>
        [Route("Disco/AgregarFolder")]
        [HttpPost]
        public IActionResult AgregarFolder(string carpeta, string agregarFolder)
        {
            if (!System.IO.Directory.Exists(System.IO.Path.Combine(this._env.WebRootPath, carpeta, agregarFolder)))
            {
                System.IO.Directory.CreateDirectory(System.IO.Path.Combine(this._env.WebRootPath, carpeta, agregarFolder));
            }

            return Json(true);
        }

        /// <summary>
        /// Función que recupera los archivos del disco.
        /// </summary>
        /// <param name = "carpeta">Variable con el folder a buscar</param>
        /// <param name = "tema">Diseño que se aplicará a la vista.</param>
        /// <returns>Json</returns>
        [Route("Disco/ListaDeArchivos")]
        [HttpPost]
        public IActionResult ListaDeArchivos(string carpeta, long tema)
        {
            System.Text.StringBuilder resultado = new System.Text.StringBuilder();
            // Iconos
            string imageFolder = "";
            string imagePredefinedFile = "";
            string image3Ds = "";
            string imageAi = "";
            string imageAsp = "";
            string imageAvi = "";
            string imageBin = "";
            string imageCom = "";
            string imageCss = "";
            string imageCsv = "";
            string imageDbf = "";
            string imageDll = "";
            string imageDocDocx = "";
            string imageDwg = "";
            string imageEml = "";
            string imageEps = "";
            string imageExe = "";
            string imageFla = "";
            string imageGif = "";
            string imageHtmHtml = "";
            string imageIco = "";
            string imageIni = "";
            string imageIso = "";
            string imageJar = "";
            string imageJpgJpeg = "";
            string imageJs = "";
            string imageMkv = "";
            string imageMov = "";
            string imageMp3 = "";
            string imageMp4 = "";
            string imageNfo = "";
            string imageObj = "";
            string imageOtf = "";
            string imagePdf = "";
            string imagePkg = "";
            string imagePng = "";
            string imagePptPptx = "";
            string imagePsd = "";
            string imageRtf = "";
            string imageSvg = "";
            string imageTtf = "";
            string imageTxt = "";
            string imageVcf = "";
            string imageWav = "";
            string imageWmv = "";
            string imageXlsXlsx = "";
            string imageXml = "";
            string imageZip = "";
            // Prueba
            if (tema == 1)
            {
                imageFolder = "/assets/Prueba_Closefolder.png";
                imagePredefinedFile = "/assets/Prueba_Defaulticon.png";
                image3Ds = "/assets/Prueba_3Ds.png";
                imageAi = "/assets/Prueba_Ai.png";
                imageAsp = "/assets/Prueba_Asp.png";
                imageAvi = "/assets/Prueba_Avi.png";
                imageBin = "/assets/Prueba_Bin.png";
                imageCom = "/assets/Prueba_Com.png";
                imageCss = "/assets/Prueba_Css.png";
                imageCsv = "/assets/Prueba_Csv.png";
                imageDbf = "/assets/Prueba_Dbf.png";
                imageDll = "/assets/Prueba_Dll.png";
                imageDocDocx = "/assets/Prueba_DocDocx.png";
                imageDwg = "/assets/Prueba_Dwg.png";
                imageEml = "/assets/Prueba_Eml.png";
                imageEps = "/assets/Prueba_Eps.png";
                imageExe = "/assets/Prueba_Exe.png";
                imageFla = "/assets/Prueba_Fla.png";
                imageGif = "/assets/Prueba_Gif.png";
                imageHtmHtml = "/assets/Prueba_HtmHtml.png";
                imageIco = "/assets/Prueba_Ico.png";
                imageIni = "/assets/Prueba_Ini.png";
                imageIso = "/assets/Prueba_Iso.png";
                imageJar = "/assets/Prueba_Jar.png";
                imageJpgJpeg = "/assets/Prueba_JpgJpeg.png";
                imageJs = "/assets/Prueba_Js.png";
                imageMkv = "/assets/Prueba_Mkv.png";
                imageMov = "/assets/Prueba_Mov.png";
                imageMp3 = "/assets/Prueba_Mp3.png";
                imageMp4 = "/assets/Prueba_Mp4.png";
                imageNfo = "/assets/Prueba_Nfo.png";
                imageObj = "/assets/Prueba_Obj.png";
                imageOtf = "/assets/Prueba_Otf.png";
                imagePdf = "/assets/Prueba_Pdf.png";
                imagePkg = "/assets/Prueba_Pkg.png";
                imagePng = "/assets/Prueba_Png.png";
                imagePptPptx = "/assets/Prueba_PptPptx.png";
                imagePsd = "/assets/Prueba_Psd.png";
                imageRtf = "/assets/Prueba_Rtf.png";
                imageSvg = "/assets/Prueba_Svg.png";
                imageTtf = "/assets/Prueba_Ttf.png";
                imageTxt = "/assets/Prueba_Txt.png";
                imageVcf = "/assets/Prueba_Vcf.png";
                imageWav = "/assets/Prueba_Wav.png";
                imageWmv = "/assets/Prueba_Wmv.png";
                imageXlsXlsx = "/assets/Prueba_XlsXlsx.png";
                imageXml = "/assets/Prueba_Xml.png";
                imageZip = "/assets/Prueba_Zip.png";
            }

            resultado.Append("[");
            if (!System.IO.Directory.Exists(System.IO.Path.Combine(this._env.WebRootPath, carpeta)))
            {
                System.IO.Directory.CreateDirectory(System.IO.Path.Combine(this._env.WebRootPath, carpeta));
            }

            string[] lista = System.IO.Directory.GetDirectories(System.IO.Path.Combine(this._env.WebRootPath, carpeta));
            foreach (string elemento in lista)
            {
                resultado.Append("{");
                resultado.Append("\"name\":" + Newtonsoft.Json.JsonConvert.SerializeObject(new System.IO.DirectoryInfo(elemento).Name) + ",");
                resultado.Append("\"shortName\":" + Newtonsoft.Json.JsonConvert.SerializeObject(new System.IO.DirectoryInfo(elemento).Name.Length > 10 ? new System.IO.DirectoryInfo(elemento).Name.Substring(0, 10) + "..." : new System.IO.DirectoryInfo(elemento).Name) + ",");
                resultado.Append("\"fullPath\":" + Newtonsoft.Json.JsonConvert.SerializeObject(elemento.Replace(this._env.WebRootPath + "\\", "").Replace("\\", "/")) + ",");
                resultado.Append("\"size\":0,");
                resultado.Append("\"image\":\"" + imageFolder + "\",");
                resultado.Append("\"isFolder\":true");
                resultado.Append("},");
            }

            string[] listaFiles = System.IO.Directory.GetFiles(System.IO.Path.Combine(this._env.WebRootPath, carpeta));
            foreach (string elemento in listaFiles)
            {
                string imageFile = imagePredefinedFile;
                if (image3Ds != "" && (new System.IO.FileInfo(elemento).Extension == ".3ds"))
                {
                    imageFile = image3Ds;
                }

                if (imageAi != "" && (new System.IO.FileInfo(elemento).Extension == ".ai"))
                {
                    imageFile = imageAi;
                }

                if (imageAsp != "" && (new System.IO.FileInfo(elemento).Extension == ".asp"))
                {
                    imageFile = imageAsp;
                }

                if (imageAvi != "" && (new System.IO.FileInfo(elemento).Extension == ".avi"))
                {
                    imageFile = imageAvi;
                }

                if (imageBin != "" && (new System.IO.FileInfo(elemento).Extension == ".bin"))
                {
                    imageFile = imageBin;
                }

                if (imageCom != "" && (new System.IO.FileInfo(elemento).Extension == ".com"))
                {
                    imageFile = imageCom;
                }

                if (imageCss != "" && (new System.IO.FileInfo(elemento).Extension == ".css"))
                {
                    imageFile = imageCss;
                }

                if (imageCsv != "" && (new System.IO.FileInfo(elemento).Extension == ".csv"))
                {
                    imageFile = imageCsv;
                }

                if (imageDbf != "" && (new System.IO.FileInfo(elemento).Extension == ".dbf"))
                {
                    imageFile = imageDbf;
                }

                if (imageDll != "" && (new System.IO.FileInfo(elemento).Extension == ".dll"))
                {
                    imageFile = imageDll;
                }

                if (imageDocDocx != "" && (new System.IO.FileInfo(elemento).Extension == ".doc" || new System.IO.FileInfo(elemento).Extension == ".docx"))
                {
                    imageFile = imageDocDocx;
                }

                if (imageDwg != "" && (new System.IO.FileInfo(elemento).Extension == ".dwg"))
                {
                    imageFile = imageDwg;
                }

                if (imageEml != "" && (new System.IO.FileInfo(elemento).Extension == ".eml"))
                {
                    imageFile = imageEml;
                }

                if (imageEps != "" && (new System.IO.FileInfo(elemento).Extension == ".eps"))
                {
                    imageFile = imageEps;
                }

                if (imageExe != "" && (new System.IO.FileInfo(elemento).Extension == ".exe"))
                {
                    imageFile = imageExe;
                }

                if (imageFla != "" && (new System.IO.FileInfo(elemento).Extension == ".fla"))
                {
                    imageFile = imageFla;
                }

                if (imageGif != "" && (new System.IO.FileInfo(elemento).Extension == ".gif"))
                {
                    imageFile = imageGif;
                }

                if (imageHtmHtml != "" && (new System.IO.FileInfo(elemento).Extension == ".htm" || new System.IO.FileInfo(elemento).Extension == ".html"))
                {
                    imageFile = imageHtmHtml;
                }

                if (imageIco != "" && (new System.IO.FileInfo(elemento).Extension == ".ico"))
                {
                    imageFile = imageIco;
                }

                if (imageIni != "" && (new System.IO.FileInfo(elemento).Extension == ".ini"))
                {
                    imageFile = imageIni;
                }

                if (imageIso != "" && (new System.IO.FileInfo(elemento).Extension == ".iso"))
                {
                    imageFile = imageIso;
                }

                if (imageJar != "" && (new System.IO.FileInfo(elemento).Extension == ".jar"))
                {
                    imageFile = imageJar;
                }

                if (imageJpgJpeg != "" && (new System.IO.FileInfo(elemento).Extension == ".jpg" || new System.IO.FileInfo(elemento).Extension == ".jpeg"))
                {
                    imageFile = imageJpgJpeg;
                }

                if (imageJs != "" && (new System.IO.FileInfo(elemento).Extension == ".js"))
                {
                    imageFile = imageJs;
                }

                if (imageMkv != "" && (new System.IO.FileInfo(elemento).Extension == ".mkv"))
                {
                    imageFile = imageMkv;
                }

                if (imageMov != "" && (new System.IO.FileInfo(elemento).Extension == ".mov"))
                {
                    imageFile = imageMov;
                }

                if (imageMp3 != "" && (new System.IO.FileInfo(elemento).Extension == ".mp3"))
                {
                    imageFile = imageMp3;
                }

                if (imageMp4 != "" && (new System.IO.FileInfo(elemento).Extension == ".mp4"))
                {
                    imageFile = imageMp4;
                }

                if (imageNfo != "" && (new System.IO.FileInfo(elemento).Extension == ".nfo"))
                {
                    imageFile = imageNfo;
                }

                if (imageObj != "" && (new System.IO.FileInfo(elemento).Extension == ".obj"))
                {
                    imageFile = imageObj;
                }

                if (imageOtf != "" && (new System.IO.FileInfo(elemento).Extension == ".otf"))
                {
                    imageFile = imageOtf;
                }

                if (imagePdf != "" && (new System.IO.FileInfo(elemento).Extension == ".pdf"))
                {
                    imageFile = imagePdf;
                }

                if (imagePkg != "" && (new System.IO.FileInfo(elemento).Extension == ".pkg"))
                {
                    imageFile = imagePkg;
                }

                if (imagePng != "" && (new System.IO.FileInfo(elemento).Extension == ".png"))
                {
                    imageFile = imagePng;
                }

                if (imagePptPptx != "" && (new System.IO.FileInfo(elemento).Extension == ".ppt" || new System.IO.FileInfo(elemento).Extension == ".pptx"))
                {
                    imageFile = imagePptPptx;
                }

                if (imagePsd != "" && (new System.IO.FileInfo(elemento).Extension == ".psd"))
                {
                    imageFile = imagePsd;
                }

                if (imageRtf != "" && (new System.IO.FileInfo(elemento).Extension == ".rtf"))
                {
                    imageFile = imageRtf;
                }

                if (imageSvg != "" && (new System.IO.FileInfo(elemento).Extension == ".svg"))
                {
                    imageFile = imageSvg;
                }

                if (imageTtf != "" && (new System.IO.FileInfo(elemento).Extension == ".ttf"))
                {
                    imageFile = imageTtf;
                }

                if (imageTxt != "" && (new System.IO.FileInfo(elemento).Extension == ".txt"))
                {
                    imageFile = imageTxt;
                }

                if (imageVcf != "" && (new System.IO.FileInfo(elemento).Extension == ".vcf"))
                {
                    imageFile = imageVcf;
                }

                if (imageWav != "" && (new System.IO.FileInfo(elemento).Extension == ".wav"))
                {
                    imageFile = imageWav;
                }

                if (imageWmv != "" && (new System.IO.FileInfo(elemento).Extension == ".wmv"))
                {
                    imageFile = imageWmv;
                }

                if (imageXlsXlsx != "" && (new System.IO.FileInfo(elemento).Extension == ".xls" || new System.IO.FileInfo(elemento).Extension == ".xlsx"))
                {
                    imageFile = imageXlsXlsx;
                }

                if (imageXml != "" && (new System.IO.FileInfo(elemento).Extension == ".xml"))
                {
                    imageFile = imageXml;
                }

                if (imageZip != "" && (new System.IO.FileInfo(elemento).Extension == ".zip"))
                {
                    imageFile = imageZip;
                }

                resultado.Append("{");
                resultado.Append("\"name\":" + Newtonsoft.Json.JsonConvert.SerializeObject(new System.IO.FileInfo(elemento).Name) + ",");
                resultado.Append("\"shortName\":" + Newtonsoft.Json.JsonConvert.SerializeObject(new System.IO.FileInfo(elemento).Name.Length > 10 ? new System.IO.DirectoryInfo(elemento).Name.Substring(0, 10) + "..." : new System.IO.DirectoryInfo(elemento).Name) + ",");
                resultado.Append("\"fullPath\":" + Newtonsoft.Json.JsonConvert.SerializeObject(elemento.Replace(this._env.WebRootPath + "\\", "").Replace("\\", "/")) + ",");
                resultado.Append("\"size\":" + new System.IO.FileInfo(elemento).Length + ",");
                resultado.Append("\"image\":\"" + imageFile + "\",");
                resultado.Append("\"isFolder\":false");
                resultado.Append("},");
            }

            if (lista.Length > 0 || listaFiles.Length > 0)
            {
                resultado = new System.Text.StringBuilder(resultado.ToString(0, resultado.Length - 1));
            }

            resultado.Append("]");
            return Content(resultado.ToString(), "application/json");
        }

        /// <summary>
        /// Función que recupera los folders del disco.
        /// </summary>
        /// <param name = "carpeta">Variable con el folder a buscar</param>
        /// <returns>Json</returns>
        [Route("Disco/ListaDeCarpetas")]
        [HttpPost]
        public IActionResult ListaDeCarpetas(string carpeta)
        {
            System.Text.StringBuilder resultado = new System.Text.StringBuilder();
            resultado.Append("[");
            if (!System.IO.Directory.Exists(System.IO.Path.Combine(this._env.WebRootPath, carpeta)))
            {
                System.IO.Directory.CreateDirectory(System.IO.Path.Combine(this._env.WebRootPath, carpeta));
            }

            string[] lista = System.IO.Directory.GetDirectories(System.IO.Path.Combine(this._env.WebRootPath, carpeta));
            foreach (string elemento in lista)
            {
                resultado.Append("{");
                resultado.Append("\"name\":" + Newtonsoft.Json.JsonConvert.SerializeObject(new System.IO.DirectoryInfo(elemento).Name) + ",\"expanded\":true,");
                resultado.Append("\"children\":[");
                resultado.Append(CarpetasAnidadas(elemento));
                resultado.Append("]");
                resultado.Append("},");
            }

            if (lista.Length > 0)
            {
                resultado = new System.Text.StringBuilder(resultado.ToString(0, resultado.Length - 1));
            }

            resultado.Append("]");
            return Content(resultado.ToString(), "application/json");
        }
#endregion
    }
}