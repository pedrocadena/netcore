using Codium.Web.Models;

using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Codium.Web.Controllers
{
    /// <summary>
    /// Detalle Productores
    /// </summary>
    [ApiExplorerSettings(IgnoreApi = true)]
    public class DetalleProductores : Controller
    {
#region Variables Privadas
        /// <summary>
        /// Web Host Environment
        /// </summary>        
        private readonly IWebHostEnvironment _env;
        /// <summary>
        /// Razor View To StringRenderer
        /// </summary>
        private readonly IRazorViewToStringRenderer _renderer;
        /// <summary>
        /// Objeto para encriptar y desencriptar valores
        /// </summary>
        private readonly IDataProtector _protector;
        /// <summary>
        /// Acceso a la configuración de la aplicación
        /// </summary>
        private readonly Microsoft.Extensions.Configuration.IConfiguration _configuracion;
        /// <summary>
        /// Variable con el idioma de la aplicación
        /// </summary>
        private readonly Microsoft.Extensions.Localization.IStringLocalizer<PruebaDeExcel.RecursosCompartidos> _idioma;
        /// <summary>
        /// Opciones de Idioma
        /// </summary>
        private readonly Microsoft.Extensions.Options.IOptions<Microsoft.AspNetCore.Builder.RequestLocalizationOptions> _opcionesDeIdioma;
        /// <summary>
        /// MySQL®
        /// </summary>
        private readonly Codium.Web.Models.Mysqlr _mysqlrConexion;
        /// <summary>
        /// MySQL®
        /// </summary>
        private readonly Codium.Web.Models.IMysqlrUnitOfWork _mysqlr;
        /// <summary>
        /// SQLite
        /// </summary>
        private readonly Codium.Web.Models.Sqlite _sqliteConexion;
        /// <summary>
        /// SQLite
        /// </summary>
        private readonly Codium.Web.Models.ISqliteUnitOfWork _sqlite;
#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name = "env">Web Host Environment</param>
        /// <param name = "renderer">Razor View To String Renderer</param>
        /// <param name = "protector">Objeto para encriptar y desencriptar valores</param>
        /// <param name = "configuracion">Acceso a la configuración de la aplicación</param>
        /// <param name = "idioma">Variable con el idioma de la aplicación</param>
        /// <param name = "opcionesDeIdioma">Opciones de Idioma</param>
        /// <param name = "mysqlrConexion">MySQL®</param>
        /// <param name = "mysqlr">MySQL®</param>
        /// <param name = "sqliteConexion">SQLite</param>
        /// <param name = "sqlite">SQLite</param>
        public DetalleProductores(IWebHostEnvironment env, IRazorViewToStringRenderer renderer, IDataProtectionProvider protector, Microsoft.Extensions.Configuration.IConfiguration configuracion, Microsoft.Extensions.Localization.IStringLocalizer<PruebaDeExcel.RecursosCompartidos> idioma, Microsoft.Extensions.Options.IOptions<Microsoft.AspNetCore.Builder.RequestLocalizationOptions> opcionesDeIdioma, Codium.Web.Models.Mysqlr mysqlrConexion, Codium.Web.Models.IMysqlrUnitOfWork mysqlr, Codium.Web.Models.Sqlite sqliteConexion, Codium.Web.Models.ISqliteUnitOfWork sqlite)
        {
            this._env = env;
            this._renderer = renderer;
            this._protector = protector.CreateProtector("URLProtection");
            this._configuracion = configuracion;
            this._idioma = idioma;
            this._opcionesDeIdioma = opcionesDeIdioma;
            this._mysqlrConexion = mysqlrConexion;
            this._mysqlr = mysqlr;
            this._sqliteConexion = sqliteConexion;
            this._sqlite = sqlite;
        }

#endregion
#region Métodos
        /// <summary>
        /// Guardar Productores
        /// </summary>
        /// <param name = "identificador">Identificador</param>
        /// <returns>Json</returns>
        [Route("DetalleProductores/GuardarProductores/{identificador}")]
        [HttpPost]
        public IActionResult GuardarProductores(string identificador)
        {
            try
            {
                System.Text.StringBuilder resultado = new System.Text.StringBuilder();
                resultado.Append("{");
                resultado.Append("\"guardarProductores\":{");
                Codium.Web.Models.MysqlrEntidades.Productores tablaGuardarProductores1 = null;
                using (var transaccion = new System.Transactions.TransactionScope())
                {
                    bool esNuevoTablaGuardarProductores1 = true;
                    long llavePrimaria = PruebaDeExcel.Funciones.TryParse<long>((identificador != null && identificador != "" && identificador != "_" ? _protector.Unprotect(identificador) : ""));
                    tablaGuardarProductores1 = this._mysqlrConexion.Productores.FirstOrDefault(x => x.LlavePrimaria == llavePrimaria);
                    if (tablaGuardarProductores1 == null)
                    {
                        tablaGuardarProductores1 = new Codium.Web.Models.MysqlrEntidades.Productores();
                        this._mysqlrConexion.Productores.Add(tablaGuardarProductores1);
                    }
                    else
                    {
                        esNuevoTablaGuardarProductores1 = false;
                    }

                    if (esNuevoTablaGuardarProductores1)
                    {
                        string curpProductorCuadernilloSaderUnico = PruebaDeExcel.Funciones.TryParse<string>(Request.Form["editarCurpProductorCuadernilloSaderProductoresDetalleProductores"]);
                        if (this._mysqlrConexion.Productores.Count(x => x.CurpProductorCuadernilloSader == curpProductorCuadernilloSaderUnico) > 0)
                        {
                            throw new Exception("NotUnique");
                        }
                    }

                    tablaGuardarProductores1.ApellidoMaterno = PruebaDeExcel.Funciones.TryParse<string>(Request.Form["editarApellidoMaternoProductoresDetalleProductores"]);
                    tablaGuardarProductores1.ApellidoPaterno = PruebaDeExcel.Funciones.TryParse<string>(Request.Form["editarApellidoPaternoProductoresDetalleProductores"]);
                    tablaGuardarProductores1.CurpProductorCuadernilloSader = PruebaDeExcel.Funciones.TryParse<string>(Request.Form["editarCurpProductorCuadernilloSaderProductoresDetalleProductores"]);
                    tablaGuardarProductores1.FechaDeRegistro = PruebaDeExcel.Funciones.TryParse<System.DateTime?>(Request.Form["editarFechaDeRegistroProductoresDetalleProductores"]);
                    tablaGuardarProductores1.FolioCuadernilloSader = PruebaDeExcel.Funciones.TryParse<string>(Request.Form["editarFolioCuadernilloSaderProductoresDetalleProductores"]);
                    tablaGuardarProductores1.IdCedas = PruebaDeExcel.Funciones.TryParse<long>(Request.Form["cedasDetalleDetalleProductores"]);
                    tablaGuardarProductores1.NoDeIdentificacion = PruebaDeExcel.Funciones.TryParse<string>(Request.Form["editarNoDeIdentificacionProductoresDetalleProductores"]);
                    tablaGuardarProductores1.Nombres = PruebaDeExcel.Funciones.TryParse<string>(Request.Form["editarNombresProductoresDetalleProductores"]);
                    if (esNuevoTablaGuardarProductores1)
                    {
                        tablaGuardarProductores1.Estatus = 1;
                    }

                    if (esNuevoTablaGuardarProductores1)
                    {
                        tablaGuardarProductores1.FechaDeCreacion = PruebaDeExcel.Funciones.TryParse<System.DateTime>(System.DateTime.Now);
                    }

                    if (!esNuevoTablaGuardarProductores1)
                    {
                        tablaGuardarProductores1.FechaDeModificacion = PruebaDeExcel.Funciones.TryParse<System.DateTime?>(System.DateTime.Now);
                    }

                    this._mysqlrConexion.SaveChanges();
                    resultado.Append("\"tablaGuardarProductores1\":");
                    resultado.Append(Newtonsoft.Json.JsonConvert.SerializeObject(tablaGuardarProductores1));
                    transaccion.Complete();
                }

                resultado.Append("}");
                resultado.Append("}");
                return Content(resultado.ToString(), "application/json");
            }
            catch (Exception ex)
            {
                return StatusCode(505, ex.Message);
            }
        }
#endregion
    }
}