using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Threading.Tasks;
namespace Codium.Web.Controllers
{
    /// <summary>
    /// amazon codium
    /// </summary>
    [ApiExplorerSettings(IgnoreApi = true)]
    public class AmazonCodium : Controller
    {
#region Variables Privadas
        /// <summary>
        /// Web Host Environment
        /// </summary>        
        private readonly IWebHostEnvironment _env;
        /// <summary>
        /// Razor View To StringRenderer
        /// </summary>
        private readonly IRazorViewToStringRenderer _renderer;
        /// <summary>
        /// Objeto para encriptar y desencriptar valores
        /// </summary>
        private readonly IDataProtector _protector;
        /// <summary>
        /// Acceso a la configuración de la aplicación
        /// </summary>
        private readonly Microsoft.Extensions.Configuration.IConfiguration _configuracion;
        /// <summary>
        /// Variable con el idioma de la aplicación
        /// </summary>
        private readonly Microsoft.Extensions.Localization.IStringLocalizer<PruebaDeExcel.RecursosCompartidos> _idioma;
        /// <summary>
        /// Opciones de Idioma
        /// </summary>
        private readonly Microsoft.Extensions.Options.IOptions<Microsoft.AspNetCore.Builder.RequestLocalizationOptions> _opcionesDeIdioma;
#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name = "env">Web Host Environment</param>
        /// <param name = "renderer">Razor View To String Renderer</param>
        /// <param name = "protector">Objeto para encriptar y desencriptar valores</param>
        /// <param name = "configuracion">Acceso a la configuración de la aplicación</param>
        /// <param name = "idioma">Variable con el idioma de la aplicación</param>
        /// <param name = "opcionesDeIdioma">Opciones de Idioma</param>
        public AmazonCodium(IWebHostEnvironment env, IRazorViewToStringRenderer renderer, IDataProtectionProvider protector, Microsoft.Extensions.Configuration.IConfiguration configuracion, Microsoft.Extensions.Localization.IStringLocalizer<PruebaDeExcel.RecursosCompartidos> idioma, Microsoft.Extensions.Options.IOptions<Microsoft.AspNetCore.Builder.RequestLocalizationOptions> opcionesDeIdioma)
        {
            this._env = env;
            this._renderer = renderer;
            this._protector = protector.CreateProtector("URLProtection");
            this._configuracion = configuracion;
            this._idioma = idioma;
            this._opcionesDeIdioma = opcionesDeIdioma;
        }

#endregion
#region Métodos
        /// <summary>
        /// Función que recupera las subcarpetas de un folder.
        /// </summary>
        /// <param name = "carpeta">Variable con el folder a buscar</param>
        /// <param name = "amazonS3Client">Conexión</param>
        /// <returns>string</returns>
        private string CarpetasAnidadas(string carpeta, Amazon.S3.AmazonS3Client amazonS3Client)
        {
            System.Text.StringBuilder resultado = new System.Text.StringBuilder();
            Amazon.S3.Model.ListObjectsRequest listObjectsRequest = new Amazon.S3.Model.ListObjectsRequest();
            listObjectsRequest.BucketName = this._configuracion["AppSettings:AmazonCodium:Bucket"];
            listObjectsRequest.Prefix = carpeta;
            listObjectsRequest.Delimiter = "";
            List<Amazon.S3.Model.S3Object> lista = amazonS3Client.ListObjectsAsync(listObjectsRequest).Result.S3Objects.Where(x => x.Key.EndsWith("/") && x.Key.Replace(carpeta, "").Count(f => f == '/') == 1 && x.Size == 0).ToList();
            foreach (Amazon.S3.Model.S3Object elemento in lista)
            {
                string key = elemento.Key.Substring(0, elemento.Key.LastIndexOf("/"));
                resultado.Append("{");
                resultado.Append("\"name\":" + Newtonsoft.Json.JsonConvert.SerializeObject(key.Substring(key.LastIndexOf("/") + 1)) + ",\"expanded\":true,");
                resultado.Append("\"children\":[");
                resultado.Append(CarpetasAnidadas(elemento.Key, amazonS3Client));
                resultado.Append("]");
                resultado.Append("},");
            }

            if (lista.Count > 0)
            {
                resultado = new System.Text.StringBuilder(resultado.ToString(0, resultado.Length - 1));
            }

            return resultado.ToString();
        }

        /// <summary>
        /// Función que borrar carpetas o archivos
        /// </summary>
        /// <param name = "toDelete">Listado de archivos o carpetas a borrar</param>
        /// <returns>Json</returns>
        [Route("AmazonCodium/Borrar")]
        [HttpPost]
        public IActionResult Borrar(string toDelete)
        {
            Amazon.RegionEndpoint regionEndpoint = Amazon.RegionEndpoint.EnumerableAllRegions.FirstOrDefault(x => x.SystemName == this._configuracion["AppSettings:AmazonCodium:Region"]);
            Amazon.Runtime.BasicAWSCredentials basicAWSCredentials = new Amazon.Runtime.BasicAWSCredentials(this._configuracion["AppSettings:AmazonCodium:AccessKeyID"], this._configuracion["AppSettings:AmazonCodium:SecretKey"]);
            Amazon.S3.AmazonS3Client amazonS3Client = new Amazon.S3.AmazonS3Client(basicAWSCredentials, regionEndpoint);
            string[] lista = toDelete.Split('|');
            foreach (string elemento in lista)
            {
                if (elemento != "")
                {
                    Amazon.S3.Model.DeleteObjectsRequest deleteObjectsRequest = new Amazon.S3.Model.DeleteObjectsRequest();
                    deleteObjectsRequest.BucketName = this._configuracion["AppSettings:AmazonCodium:Bucket"];
                    Amazon.S3.Model.ListObjectsRequest listObjectsRequest = new Amazon.S3.Model.ListObjectsRequest();
                    listObjectsRequest.BucketName = this._configuracion["AppSettings:AmazonCodium:Bucket"];
                    listObjectsRequest.Prefix = elemento;
                    listObjectsRequest.Delimiter = "";
                    List<Amazon.S3.Model.S3Object> blobs = amazonS3Client.ListObjectsAsync(listObjectsRequest).Result.S3Objects.ToList();
                    foreach (Amazon.S3.Model.S3Object blob in blobs)
                    {
                        deleteObjectsRequest.AddKey(blob.Key);
                    }

                    if (elemento.IndexOf("http") > -1)
                    {
                        Uri uri = new Uri(elemento);
                        deleteObjectsRequest.AddKey(uri.AbsolutePath.Substring(1));
                    }
                    else
                    {
                        deleteObjectsRequest.AddKey(elemento + "/");
                    }

                    try
                    {
                        var deleteObjectsResult = amazonS3Client.DeleteObjectsAsync(deleteObjectsRequest).Result;
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }

            return Json(true);
        }

        /// <summary>
        /// Función para subir archivos
        /// </summary>
        /// <param name = "carpeta">Variable con el folder a buscar</param>
        /// <param name = "file">Archivo a subir</param>
        /// <returns>Json</returns>
        [Route("AmazonCodium/SubirArchivo")]
        [HttpPost]
        public IActionResult SubirArchivo(string carpeta, List<IFormFile> file)
        {
            Amazon.RegionEndpoint regionEndpoint = Amazon.RegionEndpoint.EnumerableAllRegions.FirstOrDefault(x => x.SystemName == this._configuracion["AppSettings:AmazonCodium:Region"]);
            Amazon.Runtime.BasicAWSCredentials basicAWSCredentials = new Amazon.Runtime.BasicAWSCredentials(this._configuracion["AppSettings:AmazonCodium:AccessKeyID"], this._configuracion["AppSettings:AmazonCodium:SecretKey"]);
            Amazon.S3.AmazonS3Client amazonS3Client = new Amazon.S3.AmazonS3Client(basicAWSCredentials, regionEndpoint);
            foreach (var inputFile in file)
            {
                if (inputFile.Length > 0)
                {
                    System.IO.Stream stream = inputFile.OpenReadStream();
                    Amazon.S3.Transfer.TransferUtility transferUtility = new Amazon.S3.Transfer.TransferUtility(amazonS3Client);
                    transferUtility.Upload(stream, this._configuracion["AppSettings:AmazonCodium:Bucket"], carpeta + "/" + inputFile.FileName);
                }
            }

            return Json(true);
        }

        /// <summary>
        /// Función que recupera los archivos del disco.
        /// </summary>
        /// <param name = "carpeta">Variable con el folder a buscar</param>
        /// <param name = "tema">Diseño que se aplicará a la vista.</param>
        /// <returns>Json</returns>
        [Route("AmazonCodium/ListaDeArchivos")]
        [HttpPost]
        public IActionResult ListaDeArchivos(string carpeta, long tema)
        {
            Amazon.RegionEndpoint regionEndpoint = Amazon.RegionEndpoint.EnumerableAllRegions.FirstOrDefault(x => x.SystemName == this._configuracion["AppSettings:AmazonCodium:Region"]);
            Amazon.Runtime.BasicAWSCredentials basicAWSCredentials = new Amazon.Runtime.BasicAWSCredentials(this._configuracion["AppSettings:AmazonCodium:AccessKeyID"], this._configuracion["AppSettings:AmazonCodium:SecretKey"]);
            Amazon.S3.AmazonS3Client amazonS3Client = new Amazon.S3.AmazonS3Client(basicAWSCredentials, regionEndpoint);
            System.Text.StringBuilder resultado = new System.Text.StringBuilder();
            // Iconos
            string imageFolder = "";
            string imagePredefinedFile = "";
            string image3Ds = "";
            string imageAi = "";
            string imageAsp = "";
            string imageAvi = "";
            string imageBin = "";
            string imageCom = "";
            string imageCss = "";
            string imageCsv = "";
            string imageDbf = "";
            string imageDll = "";
            string imageDocDocx = "";
            string imageDwg = "";
            string imageEml = "";
            string imageEps = "";
            string imageExe = "";
            string imageFla = "";
            string imageGif = "";
            string imageHtmHtml = "";
            string imageIco = "";
            string imageIni = "";
            string imageIso = "";
            string imageJar = "";
            string imageJpgJpeg = "";
            string imageJs = "";
            string imageMkv = "";
            string imageMov = "";
            string imageMp3 = "";
            string imageMp4 = "";
            string imageNfo = "";
            string imageObj = "";
            string imageOtf = "";
            string imagePdf = "";
            string imagePkg = "";
            string imagePng = "";
            string imagePptPptx = "";
            string imagePsd = "";
            string imageRtf = "";
            string imageSvg = "";
            string imageTtf = "";
            string imageTxt = "";
            string imageVcf = "";
            string imageWav = "";
            string imageWmv = "";
            string imageXlsXlsx = "";
            string imageXml = "";
            string imageZip = "";
            // Prueba
            if (tema == 1)
            {
                imageFolder = "/assets/Prueba_Closefolder.png";
                imagePredefinedFile = "/assets/Prueba_Defaulticon.png";
                image3Ds = "/assets/Prueba_3Ds.png";
                imageAi = "/assets/Prueba_Ai.png";
                imageAsp = "/assets/Prueba_Asp.png";
                imageAvi = "/assets/Prueba_Avi.png";
                imageBin = "/assets/Prueba_Bin.png";
                imageCom = "/assets/Prueba_Com.png";
                imageCss = "/assets/Prueba_Css.png";
                imageCsv = "/assets/Prueba_Csv.png";
                imageDbf = "/assets/Prueba_Dbf.png";
                imageDll = "/assets/Prueba_Dll.png";
                imageDocDocx = "/assets/Prueba_DocDocx.png";
                imageDwg = "/assets/Prueba_Dwg.png";
                imageEml = "/assets/Prueba_Eml.png";
                imageEps = "/assets/Prueba_Eps.png";
                imageExe = "/assets/Prueba_Exe.png";
                imageFla = "/assets/Prueba_Fla.png";
                imageGif = "/assets/Prueba_Gif.png";
                imageHtmHtml = "/assets/Prueba_HtmHtml.png";
                imageIco = "/assets/Prueba_Ico.png";
                imageIni = "/assets/Prueba_Ini.png";
                imageIso = "/assets/Prueba_Iso.png";
                imageJar = "/assets/Prueba_Jar.png";
                imageJpgJpeg = "/assets/Prueba_JpgJpeg.png";
                imageJs = "/assets/Prueba_Js.png";
                imageMkv = "/assets/Prueba_Mkv.png";
                imageMov = "/assets/Prueba_Mov.png";
                imageMp3 = "/assets/Prueba_Mp3.png";
                imageMp4 = "/assets/Prueba_Mp4.png";
                imageNfo = "/assets/Prueba_Nfo.png";
                imageObj = "/assets/Prueba_Obj.png";
                imageOtf = "/assets/Prueba_Otf.png";
                imagePdf = "/assets/Prueba_Pdf.png";
                imagePkg = "/assets/Prueba_Pkg.png";
                imagePng = "/assets/Prueba_Png.png";
                imagePptPptx = "/assets/Prueba_PptPptx.png";
                imagePsd = "/assets/Prueba_Psd.png";
                imageRtf = "/assets/Prueba_Rtf.png";
                imageSvg = "/assets/Prueba_Svg.png";
                imageTtf = "/assets/Prueba_Ttf.png";
                imageTxt = "/assets/Prueba_Txt.png";
                imageVcf = "/assets/Prueba_Vcf.png";
                imageWav = "/assets/Prueba_Wav.png";
                imageWmv = "/assets/Prueba_Wmv.png";
                imageXlsXlsx = "/assets/Prueba_XlsXlsx.png";
                imageXml = "/assets/Prueba_Xml.png";
                imageZip = "/assets/Prueba_Zip.png";
            }

            resultado.Append("[");
            Amazon.S3.Model.ListObjectsRequest listObjectsRequest = new Amazon.S3.Model.ListObjectsRequest();
            listObjectsRequest.BucketName = this._configuracion["AppSettings:AmazonCodium:Bucket"];
            listObjectsRequest.Prefix = carpeta + "/";
            listObjectsRequest.Delimiter = "";
            List<Amazon.S3.Model.S3Object> lista = amazonS3Client.ListObjectsAsync(listObjectsRequest).Result.S3Objects.ToList();
            int listaCount = 0;
            foreach (Amazon.S3.Model.S3Object elemento in lista)
            {
                if (elemento.Key != carpeta + "/" && elemento.Key.EndsWith("/") && elemento.Key.Replace(carpeta + "/", "").Split(new[]{"/"}, StringSplitOptions.RemoveEmptyEntries).Length == 1)
                {
                    listaCount += 1;
                    string key = elemento.Key.Substring(0, elemento.Key.LastIndexOf("/"));
                    resultado.Append("{");
                    resultado.Append("\"name\":" + Newtonsoft.Json.JsonConvert.SerializeObject(key.Substring(key.LastIndexOf("/") + 1)) + ",");
                    string shortName = key.Substring(key.LastIndexOf("/") + 1);
                    if (shortName.Length > 10)
                    {
                        shortName = shortName.Substring(0, 10) + "...";
                    }

                    resultado.Append("\"shortName\":" + Newtonsoft.Json.JsonConvert.SerializeObject(shortName) + ",");
                    resultado.Append("\"fullPath\":" + Newtonsoft.Json.JsonConvert.SerializeObject(key) + ",");
                    resultado.Append("\"size\":0,");
                    resultado.Append("\"image\":\"" + imageFolder + "\",");
                    resultado.Append("\"isFolder\":true");
                    resultado.Append("},");
                }
            }

            foreach (Amazon.S3.Model.S3Object elemento in lista)
            {
                if (!elemento.Key.EndsWith("/") && elemento.Size > 0 && elemento.Key.Replace(carpeta + "/", "").Split(new[]{"/"}, StringSplitOptions.RemoveEmptyEntries).Length == 1)
                {
                    listaCount += 1;
                    string imageFile = imagePredefinedFile;
                    if (image3Ds != "" && (elemento.Key.Split('.').Last() == "3ds"))
                    {
                        imageFile = image3Ds;
                    }

                    if (imageAi != "" && (elemento.Key.Split('.').Last() == "ai"))
                    {
                        imageFile = imageAi;
                    }

                    if (imageAsp != "" && (elemento.Key.Split('.').Last() == "asp"))
                    {
                        imageFile = imageAsp;
                    }

                    if (imageAvi != "" && (elemento.Key.Split('.').Last() == "avi"))
                    {
                        imageFile = imageAvi;
                    }

                    if (imageBin != "" && (elemento.Key.Split('.').Last() == "bin"))
                    {
                        imageFile = imageBin;
                    }

                    if (imageCom != "" && (elemento.Key.Split('.').Last() == "com"))
                    {
                        imageFile = imageCom;
                    }

                    if (imageCss != "" && (elemento.Key.Split('.').Last() == "css"))
                    {
                        imageFile = imageCss;
                    }

                    if (imageCsv != "" && (elemento.Key.Split('.').Last() == "csv"))
                    {
                        imageFile = imageCsv;
                    }

                    if (imageDbf != "" && (elemento.Key.Split('.').Last() == "dbf"))
                    {
                        imageFile = imageDbf;
                    }

                    if (imageDll != "" && (elemento.Key.Split('.').Last() == "dll"))
                    {
                        imageFile = imageDll;
                    }

                    if (imageDocDocx != "" && (elemento.Key.Split('.').Last() == "doc" || elemento.Key.Split('.').Last() == "docx"))
                    {
                        imageFile = imageDocDocx;
                    }

                    if (imageDwg != "" && (elemento.Key.Split('.').Last() == "dwg"))
                    {
                        imageFile = imageDwg;
                    }

                    if (imageEml != "" && (elemento.Key.Split('.').Last() == "eml"))
                    {
                        imageFile = imageEml;
                    }

                    if (imageEps != "" && (elemento.Key.Split('.').Last() == "eps"))
                    {
                        imageFile = imageEps;
                    }

                    if (imageExe != "" && (elemento.Key.Split('.').Last() == "exe"))
                    {
                        imageFile = imageExe;
                    }

                    if (imageFla != "" && (elemento.Key.Split('.').Last() == "fla"))
                    {
                        imageFile = imageFla;
                    }

                    if (imageGif != "" && (elemento.Key.Split('.').Last() == "gif"))
                    {
                        imageFile = imageGif;
                    }

                    if (imageHtmHtml != "" && (elemento.Key.Split('.').Last() == "htm" || elemento.Key.Split('.').Last() == "html"))
                    {
                        imageFile = imageHtmHtml;
                    }

                    if (imageIco != "" && (elemento.Key.Split('.').Last() == "ico"))
                    {
                        imageFile = imageIco;
                    }

                    if (imageIni != "" && (elemento.Key.Split('.').Last() == "ini"))
                    {
                        imageFile = imageIni;
                    }

                    if (imageIso != "" && (elemento.Key.Split('.').Last() == "iso"))
                    {
                        imageFile = imageIso;
                    }

                    if (imageJar != "" && (elemento.Key.Split('.').Last() == "jar"))
                    {
                        imageFile = imageJar;
                    }

                    if (imageJpgJpeg != "" && (elemento.Key.Split('.').Last() == "jpg" || elemento.Key.Split('.').Last() == "jpeg"))
                    {
                        imageFile = imageJpgJpeg;
                    }

                    if (imageJs != "" && (elemento.Key.Split('.').Last() == "js"))
                    {
                        imageFile = imageJs;
                    }

                    if (imageMkv != "" && (elemento.Key.Split('.').Last() == "mkv"))
                    {
                        imageFile = imageMkv;
                    }

                    if (imageMov != "" && (elemento.Key.Split('.').Last() == "mov"))
                    {
                        imageFile = imageMov;
                    }

                    if (imageMp3 != "" && (elemento.Key.Split('.').Last() == "mp3"))
                    {
                        imageFile = imageMp3;
                    }

                    if (imageMp4 != "" && (elemento.Key.Split('.').Last() == "mp4"))
                    {
                        imageFile = imageMp4;
                    }

                    if (imageNfo != "" && (elemento.Key.Split('.').Last() == "nfo"))
                    {
                        imageFile = imageNfo;
                    }

                    if (imageObj != "" && (elemento.Key.Split('.').Last() == "obj"))
                    {
                        imageFile = imageObj;
                    }

                    if (imageOtf != "" && (elemento.Key.Split('.').Last() == "otf"))
                    {
                        imageFile = imageOtf;
                    }

                    if (imagePdf != "" && (elemento.Key.Split('.').Last() == "pdf"))
                    {
                        imageFile = imagePdf;
                    }

                    if (imagePkg != "" && (elemento.Key.Split('.').Last() == "pkg"))
                    {
                        imageFile = imagePkg;
                    }

                    if (imagePng != "" && (elemento.Key.Split('.').Last() == "png"))
                    {
                        imageFile = imagePng;
                    }

                    if (imagePptPptx != "" && (elemento.Key.Split('.').Last() == "ppt" || elemento.Key.Split('.').Last() == "pptx"))
                    {
                        imageFile = imagePptPptx;
                    }

                    if (imagePsd != "" && (elemento.Key.Split('.').Last() == "psd"))
                    {
                        imageFile = imagePsd;
                    }

                    if (imageRtf != "" && (elemento.Key.Split('.').Last() == "rtf"))
                    {
                        imageFile = imageRtf;
                    }

                    if (imageSvg != "" && (elemento.Key.Split('.').Last() == "svg"))
                    {
                        imageFile = imageSvg;
                    }

                    if (imageTtf != "" && (elemento.Key.Split('.').Last() == "ttf"))
                    {
                        imageFile = imageTtf;
                    }

                    if (imageTxt != "" && (elemento.Key.Split('.').Last() == "txt"))
                    {
                        imageFile = imageTxt;
                    }

                    if (imageVcf != "" && (elemento.Key.Split('.').Last() == "vcf"))
                    {
                        imageFile = imageVcf;
                    }

                    if (imageWav != "" && (elemento.Key.Split('.').Last() == "wav"))
                    {
                        imageFile = imageWav;
                    }

                    if (imageWmv != "" && (elemento.Key.Split('.').Last() == "wmv"))
                    {
                        imageFile = imageWmv;
                    }

                    if (imageXlsXlsx != "" && (elemento.Key.Split('.').Last() == "xls" || elemento.Key.Split('.').Last() == "xlsx"))
                    {
                        imageFile = imageXlsXlsx;
                    }

                    if (imageXml != "" && (elemento.Key.Split('.').Last() == "xml"))
                    {
                        imageFile = imageXml;
                    }

                    if (imageZip != "" && (elemento.Key.Split('.').Last() == "zip"))
                    {
                        imageFile = imageZip;
                    }

                    resultado.Append("{");
                    resultado.Append("\"name\":" + Newtonsoft.Json.JsonConvert.SerializeObject(elemento.Key.Substring(elemento.Key.LastIndexOf("/") + 1)) + ",");
                    string shortName = elemento.Key.Substring(elemento.Key.LastIndexOf("/") + 1);
                    if (shortName.Length > 10)
                    {
                        shortName = shortName.Substring(0, 10) + "...";
                    }

                    resultado.Append("\"shortName\":" + Newtonsoft.Json.JsonConvert.SerializeObject(shortName) + ",");
                    Amazon.S3.Model.GetPreSignedUrlRequest getPreSignedUrlRequest = new Amazon.S3.Model.GetPreSignedUrlRequest{BucketName = this._configuracion["AppSettings:AmazonCodium:Bucket"], Key = elemento.Key, Expires = DateTime.Now.AddSeconds(604800)};
                    resultado.Append("\"fullPath\":" + Newtonsoft.Json.JsonConvert.SerializeObject(amazonS3Client.GetPreSignedURL(getPreSignedUrlRequest)) + ",");
                    resultado.Append("\"size\":" + elemento.Size + ",");
                    resultado.Append("\"image\":\"" + imageFile + "\",");
                    resultado.Append("\"isFolder\":false");
                    resultado.Append("},");
                }
            }

            if (listaCount > 0)
            {
                resultado = new System.Text.StringBuilder(resultado.ToString(0, resultado.Length - 1));
            }

            resultado.Append("]");
            return Content(resultado.ToString(), "application/json");
        }

        /// <summary>
        /// Función que recupera los folders del disco.
        /// </summary>
        /// <param name = "carpeta">Variable con el folder a buscar</param>
        /// <returns>Json</returns>
        [Route("AmazonCodium/ListaDeCarpetas")]
        [HttpPost]
        public IActionResult ListaDeCarpetas(string carpeta)
        {
            Amazon.RegionEndpoint regionEndpoint = Amazon.RegionEndpoint.EnumerableAllRegions.FirstOrDefault(x => x.SystemName == this._configuracion["AppSettings:AmazonCodium:Region"]);
            Amazon.Runtime.BasicAWSCredentials basicAWSCredentials = new Amazon.Runtime.BasicAWSCredentials(this._configuracion["AppSettings:AmazonCodium:AccessKeyID"], this._configuracion["AppSettings:AmazonCodium:SecretKey"]);
            Amazon.S3.AmazonS3Client amazonS3Client = new Amazon.S3.AmazonS3Client(basicAWSCredentials, regionEndpoint);
            System.Text.StringBuilder resultado = new System.Text.StringBuilder();
            resultado.Append("[");
            Amazon.S3.Model.ListObjectsRequest listObjectsRequest = new Amazon.S3.Model.ListObjectsRequest();
            listObjectsRequest.BucketName = this._configuracion["AppSettings:AmazonCodium:Bucket"];
            if (carpeta != null && carpeta != "")
            {
                listObjectsRequest.Prefix = carpeta + "/";
            }

            listObjectsRequest.Delimiter = "/";
            List<Amazon.S3.Model.S3Object> lista = amazonS3Client.ListObjectsAsync(listObjectsRequest).Result.S3Objects.Where(x => x.Key.EndsWith("/") && x.Key.Count(f => f == '/') == 1 && x.Size == 0).ToList();
            foreach (Amazon.S3.Model.S3Object elemento in lista)
            {
                resultado.Append(CarpetasAnidadas(elemento.Key, amazonS3Client));
            }

            resultado.Append("]");
            return Content(resultado.ToString(), "application/json");
        }
#endregion
    }
}