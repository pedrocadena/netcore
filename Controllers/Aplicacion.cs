using Microsoft.AspNetCore.Authorization;

using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Codium.Web.Controllers
{
    /// <summary>
    /// Prueba de Excel
    /// </summary>
    [Route("Aplicacion")]
    [ApiController]
    public class Aplicacion : Controller
    {
#region Variables Privadas
        /// <summary>
        /// Objeto para encriptar y desencriptar valores
        /// </summary>
        private readonly Microsoft.AspNetCore.DataProtection.IDataProtector _protector;
        /// <summary>
        /// Acceso a la configuración de la aplicación
        /// </summary>
        private readonly Microsoft.Extensions.Configuration.IConfiguration _configuracion;
        /// <summary>
        /// Variable con el idioma de la aplicación
        /// </summary>
        private readonly Microsoft.Extensions.Localization.IStringLocalizer<PruebaDeExcel.RecursosCompartidos> _idioma;
        /// <summary>
        /// Opciones de Idioma
        /// </summary>
        private readonly Microsoft.Extensions.Options.IOptions<Microsoft.AspNetCore.Builder.RequestLocalizationOptions> _opcionesDeIdioma;
        /// <summary>
        /// MySQL®
        /// </summary>
        private readonly Codium.Web.Models.IMysqlrUnitOfWork _mysqlr;
        /// <summary>
        /// Prueba de Oracle
        /// </summary>
        private readonly Codium.Web.Models.IPruebaDeOracleUnitOfWork _pruebaDeOracle;
        /// <summary>
        /// SQLite
        /// </summary>
        private readonly Codium.Web.Models.ISqliteUnitOfWork _sqlite;
#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name = "protector">Objeto para encriptar y desencriptar valores</param>
        /// <param name = "configuracion">Acceso a la configuración de la aplicación</param>
        /// <param name = "idioma">Variable con el idioma de la aplicación</param>
        /// <param name = "opcionesDeIdioma">Opciones de Idioma</param>
        /// <param name = "mysqlr">MySQL®</param>
        /// <param name = "pruebaDeOracle">Prueba de Oracle</param>
        /// <param name = "sqlite">SQLite</param>
        public Aplicacion(Microsoft.AspNetCore.DataProtection.IDataProtectionProvider protector, Microsoft.Extensions.Configuration.IConfiguration configuracion, Microsoft.Extensions.Localization.IStringLocalizer<PruebaDeExcel.RecursosCompartidos> idioma, Microsoft.Extensions.Options.IOptions<Microsoft.AspNetCore.Builder.RequestLocalizationOptions> opcionesDeIdioma, Codium.Web.Models.IMysqlrUnitOfWork mysqlr, Codium.Web.Models.IPruebaDeOracleUnitOfWork pruebaDeOracle, Codium.Web.Models.ISqliteUnitOfWork sqlite)
        {
            this._protector = protector.CreateProtector("URLProtection");
            this._configuracion = configuracion;
            this._idioma = idioma;
            this._opcionesDeIdioma = opcionesDeIdioma;
            this._mysqlr = mysqlr;
            this._pruebaDeOracle = pruebaDeOracle;
            this._sqlite = sqlite;
        }

#endregion
#region Métodos
        /// <summary>
        /// Descargar Logotipo
        /// </summary>
        /// <param name = "llavePrimariaParametro">Llave Primaria</param>
        [HttpGet]
        [Route("DescargarLogotipo/{llavePrimariaParametro}")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IActionResult DescargarLogotipo(string llavePrimariaParametro)
        {
            long llavePrimaria = PruebaDeExcel.Funciones.TryParse<long>(_protector.Unprotect(llavePrimariaParametro));
            Codium.Web.Models.MysqlrEntidades.Cedas cedas = this._mysqlr.Cedas.RecuperarCedas(llavePrimaria);
            byte[] arregloDeBytes = cedas.Logotipo;
            return File(arregloDeBytes, cedas.Columna4MimeType, cedas.Columna4FileName);
        }

        /// <summary>
        /// Prueba de descarga
        /// </summary>
        /// <param name = "llavePrimariaParametro">Llave Primaria</param>
        [HttpGet]
        [Route("PruebaDeDescarga/{llavePrimariaParametro}")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IActionResult PruebaDeDescarga(string llavePrimariaParametro)
        {
            Int16 identificador = PruebaDeExcel.Funciones.TryParse<Int16>(_protector.Unprotect(llavePrimariaParametro));
            string contentType;
            string name;
            Codium.Web.Models.MysqlrEntidades.Menus menus = this._mysqlr.Menus.RecuperarMenus(identificador);
            byte[] arregloDeBytes = null;
            Amazon.RegionEndpoint regionEndpoint = Amazon.RegionEndpoint.EnumerableAllRegions.FirstOrDefault(x => x.SystemName == this._configuracion["AppSettings:AmazonCodium:Region"]);
            Amazon.Runtime.BasicAWSCredentials basicAWSCredentials = new Amazon.Runtime.BasicAWSCredentials(this._configuracion["AppSettings:AmazonCodium:AccessKeyID"], this._configuracion["AppSettings:AmazonCodium:SecretKey"]);
            Amazon.S3.AmazonS3Client amazonS3Client = new Amazon.S3.AmazonS3Client(basicAWSCredentials, regionEndpoint);
            Amazon.S3.Model.GetObjectRequest request = new Amazon.S3.Model.GetObjectRequest();
            request.BucketName = this._configuracion["AppSettings:AmazonCodium:Bucket"];
            request.Key = menus.Icono.Replace("+", " ");
            using (Amazon.S3.Model.GetObjectResponse response = amazonS3Client.GetObjectAsync(request).Result)
            {
                using (var memoryStream = new System.IO.MemoryStream())
                {
                    response.ResponseStream.CopyTo(memoryStream);
                    arregloDeBytes = memoryStream.ToArray();
                }
            }

            name = menus.Icono.Split('/').Last();
            var provider = new Microsoft.AspNetCore.StaticFiles.FileExtensionContentTypeProvider();
            if (!provider.TryGetContentType(menus.Icono, out contentType))
            {
                contentType = "application/octet-stream";
            }

            return File(arregloDeBytes, contentType);
        }

        /// <summary>
        /// Prueba de imagen
        /// </summary>
        /// <param name = "llavePrimaria">Llave Primaria</param>
        [HttpGet]
        [Route("PruebaDeImagen/{llavePrimaria}")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IActionResult PruebaDeImagen(string llavePrimaria)
        {
            Int16 identificador = PruebaDeExcel.Funciones.TryParse<Int16>(_protector.Unprotect(llavePrimaria));
            byte[] arregloDeBytes = null;
            Amazon.RegionEndpoint regionEndpoint = Amazon.RegionEndpoint.EnumerableAllRegions.FirstOrDefault(x => x.SystemName == this._configuracion["AppSettings:AmazonCodium:Region"]);
            Amazon.Runtime.BasicAWSCredentials basicAWSCredentials = new Amazon.Runtime.BasicAWSCredentials(this._configuracion["AppSettings:AmazonCodium:AccessKeyID"], this._configuracion["AppSettings:AmazonCodium:SecretKey"]);
            Amazon.S3.AmazonS3Client amazonS3Client = new Amazon.S3.AmazonS3Client(basicAWSCredentials, regionEndpoint);
            Amazon.S3.Model.GetObjectRequest request = new Amazon.S3.Model.GetObjectRequest();
            request.BucketName = this._configuracion["AppSettings:AmazonCodium:Bucket"];
            request.Key = this._mysqlr.Menus.RecuperarMenus(identificador).Icono.Replace("+", " ");
            using (Amazon.S3.Model.GetObjectResponse response = amazonS3Client.GetObjectAsync(request).Result)
            {
                using (var memoryStream = new System.IO.MemoryStream())
                {
                    response.ResponseStream.CopyTo(memoryStream);
                    arregloDeBytes = memoryStream.ToArray();
                }
            }

            // Resize Image
            System.Drawing.Image resizeImage = null;
            using (System.IO.MemoryStream memoryStream = new System.IO.MemoryStream(arregloDeBytes))
            {
                resizeImage = System.Drawing.Image.FromStream(memoryStream);
            }

            resizeImage = PruebaDeExcel.Funciones.ResizeImage(resizeImage, new System.Drawing.Size(200, 200), true);
            using (System.IO.MemoryStream memoryStream = new System.IO.MemoryStream())
            {
                resizeImage.Save(memoryStream, System.Drawing.Imaging.ImageFormat.Png);
                arregloDeBytes = memoryStream.ToArray();
            }

            return File(arregloDeBytes, "image/png");
        }

        /// <summary>
        /// Actualizar Cedas
        /// </summary>
        /// <param name = "estatusCedas1">Estatus (Cedas) Igual a (=?)</param>
        /// <param name = "llavePrimariaCedas2">Llave Primaria (Cedas) Igual a (=?)</param>
        [Authorize(AuthenticationSchemes = Microsoft.AspNetCore.Authentication.JwtBearer.JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost]
        [IgnoreAntiforgeryToken]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public bool ActualizarCedas([FromForm] SByte estatusCedas1, [FromForm] long? llavePrimariaCedas2)
        {
            this._mysqlr.Cedas.ActualizarCedas(PruebaDeExcel.Funciones.TryParse<SByte>(estatusCedas1, 0), PruebaDeExcel.Funciones.TryParse<long?>(llavePrimariaCedas2, null));
            return true;
        }

        /// <summary>
        /// Detalle Cedas
        /// </summary>
        /// <param name = "llavePrimariaCedas">Llave Primaria (Cedas) Igual a (=?)</param>
        /// <param name = "pagina">Página</param>
        /// <param name = "tamanoDePagina">Tamaño de Página</param>
        /// <param name = "ordenarPor">Ordenar por</param>
        /// <param name = "tipo">Tipo</param>
        /// <returns>List</returns>
        [Authorize(AuthenticationSchemes = Microsoft.AspNetCore.Authentication.JwtBearer.JwtBearerDefaults.AuthenticationScheme)]
        [Route("DetalleCedas")]
        [HttpPost]
        [IgnoreAntiforgeryToken]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult DetalleCedas([FromForm] long llavePrimariaCedas, [FromForm] int pagina, [FromForm] int tamanoDePagina, [FromForm] string ordenarPor, [FromForm] string tipo)
        {
            if (tamanoDePagina == 0)
            {
                return Json(new
                {
                total = this._mysqlr.Cedas.DetalleCedasTotal(PruebaDeExcel.Funciones.TryParse<long>(llavePrimariaCedas, 0L)), datos = this._mysqlr.Cedas.DetalleCedas(PruebaDeExcel.Funciones.TryParse<long>(llavePrimariaCedas, 0L), pagina, tamanoDePagina, ordenarPor, tipo)}

                );
            }

            return Json(new
            {
            total = System.Math.Ceiling((double)this._mysqlr.Cedas.DetalleCedasTotal(PruebaDeExcel.Funciones.TryParse<long>(llavePrimariaCedas, 0L)) / (double)tamanoDePagina), datos = this._mysqlr.Cedas.DetalleCedas(PruebaDeExcel.Funciones.TryParse<long>(llavePrimariaCedas, 0L), pagina, tamanoDePagina, ordenarPor, tipo)}

            );
        }

        /// <summary>
        /// Detalle Cedas Original
        /// </summary>
        /// <param name = "llavePrimariaCedasOriginal">Llave Primaria (Cedas Original) Igual a (=?)</param>
        /// <param name = "pagina">Página</param>
        /// <param name = "tamanoDePagina">Tamaño de Página</param>
        /// <param name = "ordenarPor">Ordenar por</param>
        /// <param name = "tipo">Tipo</param>
        /// <returns>List</returns>
        [Route("DetalleCedasOriginal")]
        [HttpPost]
        [IgnoreAntiforgeryToken]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult DetalleCedasOriginal([FromForm] long? llavePrimariaCedasOriginal, [FromForm] int pagina, [FromForm] int tamanoDePagina, [FromForm] string ordenarPor, [FromForm] string tipo)
        {
            if (tamanoDePagina == 0)
            {
                return Json(new
                {
                total = this._mysqlr.CedasOriginal.DetalleCedasOriginalTotal(PruebaDeExcel.Funciones.TryParse<long?>(llavePrimariaCedasOriginal, null)), datos = this._mysqlr.CedasOriginal.DetalleCedasOriginal(PruebaDeExcel.Funciones.TryParse<long?>(llavePrimariaCedasOriginal, null), pagina, tamanoDePagina, ordenarPor, tipo)}

                );
            }

            return Json(new
            {
            total = System.Math.Ceiling((double)this._mysqlr.CedasOriginal.DetalleCedasOriginalTotal(PruebaDeExcel.Funciones.TryParse<long?>(llavePrimariaCedasOriginal, null)) / (double)tamanoDePagina), datos = this._mysqlr.CedasOriginal.DetalleCedasOriginal(PruebaDeExcel.Funciones.TryParse<long?>(llavePrimariaCedasOriginal, null), pagina, tamanoDePagina, ordenarPor, tipo)}

            );
        }

        /// <summary>
        /// Detalle Productores
        /// </summary>
        /// <param name = "llavePrimariaProductores">Llave Primaria (Productores) Igual a (=?)</param>
        /// <param name = "pagina">Página</param>
        /// <param name = "tamanoDePagina">Tamaño de Página</param>
        /// <param name = "ordenarPor">Ordenar por</param>
        /// <param name = "tipo">Tipo</param>
        /// <returns>List</returns>
        [Route("DetalleProductores")]
        [HttpPost]
        [IgnoreAntiforgeryToken]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult DetalleProductores([FromForm] long? llavePrimariaProductores, [FromForm] int pagina, [FromForm] int tamanoDePagina, [FromForm] string ordenarPor, [FromForm] string tipo)
        {
            if (tamanoDePagina == 0)
            {
                return Json(new
                {
                total = this._mysqlr.Productores.DetalleProductoresTotal(PruebaDeExcel.Funciones.TryParse<long?>(llavePrimariaProductores, null)), datos = this._mysqlr.Productores.DetalleProductores(PruebaDeExcel.Funciones.TryParse<long?>(llavePrimariaProductores, null), pagina, tamanoDePagina, ordenarPor, tipo)}

                );
            }

            return Json(new
            {
            total = System.Math.Ceiling((double)this._mysqlr.Productores.DetalleProductoresTotal(PruebaDeExcel.Funciones.TryParse<long?>(llavePrimariaProductores, null)) / (double)tamanoDePagina), datos = this._mysqlr.Productores.DetalleProductores(PruebaDeExcel.Funciones.TryParse<long?>(llavePrimariaProductores, null), pagina, tamanoDePagina, ordenarPor, tipo)}

            );
        }

        /// <summary>
        /// Lista Cedas
        /// </summary>
        /// <param name = "pagina">Página</param>
        /// <param name = "tamanoDePagina">Tamaño de Página</param>
        /// <param name = "ordenarPor">Ordenar por</param>
        /// <param name = "tipo">Tipo</param>
        /// <returns>List</returns>
        [Route("ListaCedas")]
        [HttpPost]
        [IgnoreAntiforgeryToken]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult ListaCedas([FromForm] int pagina, [FromForm] int tamanoDePagina, [FromForm] string ordenarPor, [FromForm] string tipo)
        {
            if (tamanoDePagina == 0)
            {
                return Json(new
                {
                total = this._mysqlr.Cedas.ListaCedasTotal(), datos = this._mysqlr.Cedas.ListaCedas(pagina, tamanoDePagina, ordenarPor, tipo)}

                );
            }

            return Json(new
            {
            total = System.Math.Ceiling((double)this._mysqlr.Cedas.ListaCedasTotal() / (double)tamanoDePagina), datos = this._mysqlr.Cedas.ListaCedas(pagina, tamanoDePagina, ordenarPor, tipo)}

            );
        }

        /// <summary>
        /// Lista Cedas Original
        /// </summary>
        /// <param name = "pagina">Página</param>
        /// <param name = "tamanoDePagina">Tamaño de Página</param>
        /// <param name = "ordenarPor">Ordenar por</param>
        /// <param name = "tipo">Tipo</param>
        /// <returns>List</returns>
        [Route("ListaCedasOriginal")]
        [HttpPost]
        [IgnoreAntiforgeryToken]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult ListaCedasOriginal([FromForm] int pagina, [FromForm] int tamanoDePagina, [FromForm] string ordenarPor, [FromForm] string tipo)
        {
            if (tamanoDePagina == 0)
            {
                return Json(new
                {
                total = this._mysqlr.CedasOriginal.ListaCedasOriginalTotal(), datos = this._mysqlr.CedasOriginal.ListaCedasOriginal(pagina, tamanoDePagina, ordenarPor, tipo)}

                );
            }

            return Json(new
            {
            total = System.Math.Ceiling((double)this._mysqlr.CedasOriginal.ListaCedasOriginalTotal() / (double)tamanoDePagina), datos = this._mysqlr.CedasOriginal.ListaCedasOriginal(pagina, tamanoDePagina, ordenarPor, tipo)}

            );
        }

        /// <summary>
        /// Lista Productores
        /// </summary>
        /// <param name = "pagina">Página</param>
        /// <param name = "tamanoDePagina">Tamaño de Página</param>
        /// <param name = "ordenarPor">Ordenar por</param>
        /// <param name = "tipo">Tipo</param>
        /// <returns>List</returns>
        [Route("ListaProductores")]
        [HttpPost]
        [IgnoreAntiforgeryToken]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult ListaProductores([FromForm] int pagina, [FromForm] int tamanoDePagina, [FromForm] string ordenarPor, [FromForm] string tipo)
        {
            if (tamanoDePagina == 0)
            {
                return Json(new
                {
                total = this._mysqlr.Productores.ListaProductoresTotal(), datos = this._mysqlr.Productores.ListaProductores(pagina, tamanoDePagina, ordenarPor, tipo)}

                );
            }

            return Json(new
            {
            total = System.Math.Ceiling((double)this._mysqlr.Productores.ListaProductoresTotal() / (double)tamanoDePagina), datos = this._mysqlr.Productores.ListaProductores(pagina, tamanoDePagina, ordenarPor, tipo)}

            );
        }

        /// <summary>
        /// Tabla de Datos Cedas
        /// </summary>
        /// <param name = "claveCedasSaderCedas">CLAVE_CEDAS_SADER (Cedas) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "estadoCedas">ESTADO (Cedas) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "idCedaSegalmexCedas">ID CEDA SEGALMEX (Cedas) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "pagina">Página</param>
        /// <param name = "tamanoDePagina">Tamaño de Página</param>
        /// <param name = "ordenarPor">Ordenar por</param>
        /// <param name = "tipo">Tipo</param>
        /// <returns>List</returns>
        [Authorize(AuthenticationSchemes = Microsoft.AspNetCore.Authentication.JwtBearer.JwtBearerDefaults.AuthenticationScheme, Roles = PruebaDeJwt.Admin + "," + PruebaDeJwt.Guest)]
        [Route("TablaDeDatosCedas")]
        [HttpPost]
        [IgnoreAntiforgeryToken]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult TablaDeDatosCedas([FromForm] string claveCedasSaderCedas, [FromForm] string estadoCedas, [FromForm] string idCedaSegalmexCedas, [FromForm] int pagina, [FromForm] int tamanoDePagina, [FromForm] string ordenarPor, [FromForm] string tipo)
        {
            if (tamanoDePagina == 0)
            {
                return Json(new
                {
                total = this._mysqlr.Cedas.TablaDeDatosCedasTotal(PruebaDeExcel.Funciones.TryParse<string>(claveCedasSaderCedas, ""), PruebaDeExcel.Funciones.TryParse<string>(estadoCedas, ""), PruebaDeExcel.Funciones.TryParse<string>(idCedaSegalmexCedas, "")), datos = this._mysqlr.Cedas.TablaDeDatosCedas(PruebaDeExcel.Funciones.TryParse<string>(claveCedasSaderCedas, ""), PruebaDeExcel.Funciones.TryParse<string>(estadoCedas, ""), PruebaDeExcel.Funciones.TryParse<string>(idCedaSegalmexCedas, ""), pagina, tamanoDePagina, ordenarPor, tipo)}

                );
            }

            return Json(new
            {
            total = System.Math.Ceiling((double)this._mysqlr.Cedas.TablaDeDatosCedasTotal(PruebaDeExcel.Funciones.TryParse<string>(claveCedasSaderCedas, ""), PruebaDeExcel.Funciones.TryParse<string>(estadoCedas, ""), PruebaDeExcel.Funciones.TryParse<string>(idCedaSegalmexCedas, "")) / (double)tamanoDePagina), datos = this._mysqlr.Cedas.TablaDeDatosCedas(PruebaDeExcel.Funciones.TryParse<string>(claveCedasSaderCedas, ""), PruebaDeExcel.Funciones.TryParse<string>(estadoCedas, ""), PruebaDeExcel.Funciones.TryParse<string>(idCedaSegalmexCedas, ""), pagina, tamanoDePagina, ordenarPor, tipo)}

            );
        }

        /// <summary>
        /// Tabla de Datos Cedas Original
        /// </summary>
        /// <param name = "claveCedasSaderCedasOriginal">CLAVE_CEDAS_SADER (Cedas Original) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "estadoCedasOriginal">ESTADO (Cedas Original) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "idCedaSegalmexCedasOriginal">ID CEDA SEGALMEX (Cedas Original) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "regionSegalmexCedasOriginal">REGION SEGALMEX (Cedas Original) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "pagina">Página</param>
        /// <param name = "tamanoDePagina">Tamaño de Página</param>
        /// <param name = "ordenarPor">Ordenar por</param>
        /// <param name = "tipo">Tipo</param>
        /// <returns>List</returns>
        [Route("TablaDeDatosCedasOriginal")]
        [HttpPost]
        [IgnoreAntiforgeryToken]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult TablaDeDatosCedasOriginal([FromForm] string claveCedasSaderCedasOriginal, [FromForm] string estadoCedasOriginal, [FromForm] string idCedaSegalmexCedasOriginal, [FromForm] string regionSegalmexCedasOriginal, [FromForm] int pagina, [FromForm] int tamanoDePagina, [FromForm] string ordenarPor, [FromForm] string tipo)
        {
            if (tamanoDePagina == 0)
            {
                return Json(new
                {
                total = this._mysqlr.CedasOriginal.TablaDeDatosCedasOriginalTotal(PruebaDeExcel.Funciones.TryParse<string>(claveCedasSaderCedasOriginal, ""), PruebaDeExcel.Funciones.TryParse<string>(estadoCedasOriginal, ""), PruebaDeExcel.Funciones.TryParse<string>(idCedaSegalmexCedasOriginal, ""), PruebaDeExcel.Funciones.TryParse<string>(regionSegalmexCedasOriginal, "")), datos = this._mysqlr.CedasOriginal.TablaDeDatosCedasOriginal(PruebaDeExcel.Funciones.TryParse<string>(claveCedasSaderCedasOriginal, ""), PruebaDeExcel.Funciones.TryParse<string>(estadoCedasOriginal, ""), PruebaDeExcel.Funciones.TryParse<string>(idCedaSegalmexCedasOriginal, ""), PruebaDeExcel.Funciones.TryParse<string>(regionSegalmexCedasOriginal, ""), pagina, tamanoDePagina, ordenarPor, tipo)}

                );
            }

            return Json(new
            {
            total = System.Math.Ceiling((double)this._mysqlr.CedasOriginal.TablaDeDatosCedasOriginalTotal(PruebaDeExcel.Funciones.TryParse<string>(claveCedasSaderCedasOriginal, ""), PruebaDeExcel.Funciones.TryParse<string>(estadoCedasOriginal, ""), PruebaDeExcel.Funciones.TryParse<string>(idCedaSegalmexCedasOriginal, ""), PruebaDeExcel.Funciones.TryParse<string>(regionSegalmexCedasOriginal, "")) / (double)tamanoDePagina), datos = this._mysqlr.CedasOriginal.TablaDeDatosCedasOriginal(PruebaDeExcel.Funciones.TryParse<string>(claveCedasSaderCedasOriginal, ""), PruebaDeExcel.Funciones.TryParse<string>(estadoCedasOriginal, ""), PruebaDeExcel.Funciones.TryParse<string>(idCedaSegalmexCedasOriginal, ""), PruebaDeExcel.Funciones.TryParse<string>(regionSegalmexCedasOriginal, ""), pagina, tamanoDePagina, ordenarPor, tipo)}

            );
        }

        /// <summary>
        /// Tabla de Datos Productores
        /// </summary>
        /// <param name = "apellidoMaternoProductores">APELLIDO MATERNO (Productores) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "apellidoPaternoProductores">APELLIDO PATERNO (Productores) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "curpProductorCuadernilloSaderProductores">CURP PRODUCTOR CUADERNILLO SADER (Productores) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "folioCuadernilloSaderProductores">FOLIO CUADERNILLO SADER (Productores) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "idCedasProductores">ID CEDAS (Productores) Igual a (=?)</param>
        /// <param name = "idCedaSegalmexCedas">ID CEDA SEGALMEX (Cedas) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "noDeIdentificacionProductores">NO. DE IDENTIFICACI&#211;N (Productores) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "nombresProductores">NOMBRE(S) (Productores) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "pagina">Página</param>
        /// <param name = "tamanoDePagina">Tamaño de Página</param>
        /// <param name = "ordenarPor">Ordenar por</param>
        /// <param name = "tipo">Tipo</param>
        /// <returns>List</returns>
        [Route("TablaDeDatosProductores")]
        [HttpPost]
        [IgnoreAntiforgeryToken]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult TablaDeDatosProductores([FromForm] string apellidoMaternoProductores, [FromForm] string apellidoPaternoProductores, [FromForm] string curpProductorCuadernilloSaderProductores, [FromForm] string folioCuadernilloSaderProductores, [FromForm] long? idCedasProductores, [FromForm] string idCedaSegalmexCedas, [FromForm] string noDeIdentificacionProductores, [FromForm] string nombresProductores, [FromForm] int pagina, [FromForm] int tamanoDePagina, [FromForm] string ordenarPor, [FromForm] string tipo)
        {
            if (tamanoDePagina == 0)
            {
                return Json(new
                {
                total = this._mysqlr.Productores.TablaDeDatosProductoresTotal(PruebaDeExcel.Funciones.TryParse<string>(apellidoMaternoProductores, ""), PruebaDeExcel.Funciones.TryParse<string>(apellidoPaternoProductores, ""), PruebaDeExcel.Funciones.TryParse<string>(curpProductorCuadernilloSaderProductores, ""), PruebaDeExcel.Funciones.TryParse<string>(folioCuadernilloSaderProductores, ""), PruebaDeExcel.Funciones.TryParse<long?>(idCedasProductores, null), PruebaDeExcel.Funciones.TryParse<string>(idCedaSegalmexCedas, ""), PruebaDeExcel.Funciones.TryParse<string>(noDeIdentificacionProductores, ""), PruebaDeExcel.Funciones.TryParse<string>(nombresProductores, "")), datos = this._mysqlr.Productores.TablaDeDatosProductores(PruebaDeExcel.Funciones.TryParse<string>(apellidoMaternoProductores, ""), PruebaDeExcel.Funciones.TryParse<string>(apellidoPaternoProductores, ""), PruebaDeExcel.Funciones.TryParse<string>(curpProductorCuadernilloSaderProductores, ""), PruebaDeExcel.Funciones.TryParse<string>(folioCuadernilloSaderProductores, ""), PruebaDeExcel.Funciones.TryParse<long?>(idCedasProductores, null), PruebaDeExcel.Funciones.TryParse<string>(idCedaSegalmexCedas, ""), PruebaDeExcel.Funciones.TryParse<string>(noDeIdentificacionProductores, ""), PruebaDeExcel.Funciones.TryParse<string>(nombresProductores, ""), pagina, tamanoDePagina, ordenarPor, tipo)}

                );
            }

            return Json(new
            {
            total = System.Math.Ceiling((double)this._mysqlr.Productores.TablaDeDatosProductoresTotal(PruebaDeExcel.Funciones.TryParse<string>(apellidoMaternoProductores, ""), PruebaDeExcel.Funciones.TryParse<string>(apellidoPaternoProductores, ""), PruebaDeExcel.Funciones.TryParse<string>(curpProductorCuadernilloSaderProductores, ""), PruebaDeExcel.Funciones.TryParse<string>(folioCuadernilloSaderProductores, ""), PruebaDeExcel.Funciones.TryParse<long?>(idCedasProductores, null), PruebaDeExcel.Funciones.TryParse<string>(idCedaSegalmexCedas, ""), PruebaDeExcel.Funciones.TryParse<string>(noDeIdentificacionProductores, ""), PruebaDeExcel.Funciones.TryParse<string>(nombresProductores, "")) / (double)tamanoDePagina), datos = this._mysqlr.Productores.TablaDeDatosProductores(PruebaDeExcel.Funciones.TryParse<string>(apellidoMaternoProductores, ""), PruebaDeExcel.Funciones.TryParse<string>(apellidoPaternoProductores, ""), PruebaDeExcel.Funciones.TryParse<string>(curpProductorCuadernilloSaderProductores, ""), PruebaDeExcel.Funciones.TryParse<string>(folioCuadernilloSaderProductores, ""), PruebaDeExcel.Funciones.TryParse<long?>(idCedasProductores, null), PruebaDeExcel.Funciones.TryParse<string>(idCedaSegalmexCedas, ""), PruebaDeExcel.Funciones.TryParse<string>(noDeIdentificacionProductores, ""), PruebaDeExcel.Funciones.TryParse<string>(nombresProductores, ""), pagina, tamanoDePagina, ordenarPor, tipo)}

            );
        }
#endregion
    }
}