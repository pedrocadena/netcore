using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

using System;
namespace Codium.Web
{
    /// <summary>
    /// Clase Programa
    /// </summary>
    public class Program
    {
#region Métodos
        /// <summary>
        /// Método Principal
        /// </summary>
        /// <param name = "args">Argumentos</param>
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        /// <summary>
        /// Crear Web Host
        /// </summary>
        /// <param name = "args">Argumentos</param>
        /// <returns>Web Host</returns>
        public static IWebHostBuilder CreateWebHostBuilder(string[] args) => WebHost.CreateDefaultBuilder(args).UseKestrel().UseStartup<Startup>();
#endregion
    }
}