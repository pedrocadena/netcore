namespace Codium.Web
{
    /// <summary>
    /// Prueba de JWT
    /// </summary>
    public static class PruebaDeJwt
    {
#region Roles
        /// <summary>
        /// Admin
        /// </summary>
        public const string Admin = "Admin";
        /// <summary>
        /// Guest
        /// </summary>
        public const string Guest = "Guest";
        /// <summary>
        /// read-only-admin
        /// </summary>
        public const string ReadOnlyAdmin = "read-only-admin";
#endregion
    }
}