using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.Facebook;

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
namespace Codium.Web
{
    /// <summary>
    /// Clase de Inicio
    /// </summary>
    public class Startup
    {
#region Constructor    
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name = "configuration">Configuración</param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

#endregion
#region Propiedades Públicas
        /// <summary>
        /// Configuración
        /// </summary>
        public static IConfiguration Configuration { get; private set; }

#endregion
#region Métodos
        /// <summary>
        /// Este método es llamado por el tiempo de ejecución. Utilice este método para agregar servicios al contenedor.
        /// </summary>
        /// <param name = "services">Colección de Servicios</param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // Esta lambda determina si se necesita el consentimiento del usuario para cookies no esenciales para una solicitud determinada.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            services.AddLocalization(options => options.ResourcesPath = "Resources");
            services.AddControllersWithViews(options =>
            {
                options.Filters.Add(new AutoValidateAntiforgeryTokenAttribute());
            }).AddDataAnnotationsLocalization(options =>
            {
                options.DataAnnotationLocalizerProvider = (type, factory) =>
                {
                    return factory.Create(typeof(PruebaDeExcel.RecursosCompartidos));
                };
            });
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings{ReferenceLoopHandling = ReferenceLoopHandling.Serialize, PreserveReferencesHandling = PreserveReferencesHandling.Objects, ContractResolver = new Newtonsoft.Json.Serialization.DefaultContractResolver{NamingStrategy = new Newtonsoft.Json.Serialization.CamelCaseNamingStrategy()}};
            services.AddTransient<IRazorViewToStringRenderer, RazorViewToStringRenderer>();
            services.AddMemoryCache();
            services.AddSession(options =>
            {
                options.Cookie.IsEssential = true;
                options.Cookie.HttpOnly = true;
                options.Cookie.SameSite = SameSiteMode.None;
                options.Cookie.SecurePolicy = CookieSecurePolicy.SameAsRequest;
            });
            services.AddSingleton<IConfiguration>(Configuration);
            services.AddHttpContextAccessor();
            services.AddAntiforgery();
            IList<CultureInfo> supportedCultures = new List<CultureInfo>{new CultureInfo("en-US"), new CultureInfo("es-MX")};
            services.AddDataProtection().PersistKeysToFileSystem(new System.IO.DirectoryInfo(System.IO.Directory.GetCurrentDirectory() + System.IO.Path.DirectorySeparatorChar + "DataProtection")).SetApplicationName("PruebaDeExcel");
            services.Configure<RequestLocalizationOptions>(options =>
            {
                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;
                options.RequestCultureProviders = new[]{new CookieRequestCultureProvider()};
            });
            // Prueba de JWT
            services.AddAuthentication(options =>
            {
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters{ValidateIssuer = true, ValidIssuer = Configuration["PruebaDeJwt:EmisorValido"], ValidateAudience = true, ValidAudience = Configuration["PruebaDeJwt:AudienciaValida"], ValidateLifetime = true, ValidateIssuerSigningKey = true, IssuerSigningKey = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(Configuration["PruebaDeJwt:ClaveSecreta"]))};
            }).AddCookie("keycloak").AddOpenIdConnect(options =>
            {
                options.Authority = Configuration["AppSettings:Keycloak:Authority"];
                options.ClientId = Configuration["AppSettings:Keycloak:ClientId"];
                options.ClientSecret = Configuration["AppSettings:Keycloak:ClientSecret"];
                options.RequireHttpsMetadata = false;
                options.ResponseType = Microsoft.IdentityModel.Protocols.OpenIdConnect.OpenIdConnectResponseType.Code;
                options.ResponseMode = Microsoft.IdentityModel.Protocols.OpenIdConnect.OpenIdConnectResponseMode.FormPost;
                options.UsePkce = true;
                options.SignInScheme = "keycloak";
                options.GetClaimsFromUserInfoEndpoint = true;
            }).AddCookie("pruebadefacebooklogin").AddFacebook(options =>
            {
                options.AppId = Configuration["AppSettings:PruebaDeFacebookLogin:AppId"];
                options.AppSecret = Configuration["AppSettings:PruebaDeFacebookLogin:AppSecret"];
                options.SignInScheme = "pruebadefacebooklogin";
            });
            services.AddDbContext<Codium.Web.Models.MicrosoftrSqlServer>(options => options.UseSqlServer(Configuration["ConnectionStrings:MicrosoftrSqlServer"]));
            services.AddDbContext<Codium.Web.Models.Mysql2>(options => options.UseMySQL(Configuration["ConnectionStrings:Mysql2"]));
            services.AddDbContext<Codium.Web.Models.Mysqlr>(options => options.UseMySQL(Configuration["ConnectionStrings:Mysqlr"]));
            services.AddDbContext<Codium.Web.Models.Sqlite>(options => options.UseSqlite(Configuration["ConnectionStrings:Sqlite"]));
            services.AddScoped<Codium.Web.Models.IMysqlrUnitOfWork, Codium.Web.Models.MysqlrUnitOfWork>();
            services.AddScoped<Codium.Web.Models.IPruebaDeOracleUnitOfWork, Codium.Web.Models.PruebaDeOracleUnitOfWork>();
            services.AddScoped<Codium.Web.Models.ISqliteUnitOfWork, Codium.Web.Models.SqliteUnitOfWork>();
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie(options =>
            {
                options.LoginPath = "/inicio";
            });
            // Register the Swagger services
            services.AddSwaggerDocument(config =>
            {
                config.PostProcess = document =>
                {
                    document.Info.Version = "1.0";
                    document.Info.Title = "Prueba de Excel";
                    document.Info.Description = "Prueba de Excel";
                    document.Info.TermsOfService = "";
                    document.Info.Contact = new NSwag.OpenApiContact{Name = " ", Email = "test.new@codium.mx", Url = ""};
                    document.Info.License = new NSwag.OpenApiLicense{Name = "", Url = ""};
                };
            });
        }

        /// <summary>
        /// Este método es llamado por el tiempo de ejecución. Utilice este método para configurar la canalización de solicitud HTTP.
        /// </summary>
        /// <param name = "app">Constructor de Aplicación</param>
        /// <param name = "env">Ambiente de Hosting</param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            IList<CultureInfo> supportedCultures = new List<CultureInfo>{new CultureInfo("en-US"), new CultureInfo("es-MX")};
            app.UseRequestLocalization(new RequestLocalizationOptions{DefaultRequestCulture = new Microsoft.AspNetCore.Localization.RequestCulture("es-MX"), SupportedCultures = supportedCultures, SupportedUICultures = supportedCultures});
            app.Use(async (context, next) =>
            {
                await next.Invoke();
            });
            //app.UseDeveloperExceptionPage();
            app.UseExceptionHandler("/Error");
            // El valor HSTS predeterminado es de 30 días. Es posible que desee cambiar esto para los escenarios de producción, consulte https://aka.ms/aspnetcore-hsts.
            app.UseHsts();
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseSession();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(name: "default", pattern: "{controller=Home}/{action=Index}/{id?}");
            });
            PruebaDeExcel.AppContext.Configure(app.ApplicationServices.GetRequiredService<IHttpContextAccessor>());
            // Register the Swagger generator and the Swagger UI middlewares
            app.UseOpenApi();
            app.UseSwaggerUi3();
        }
#endregion
    }
}