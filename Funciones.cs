using System;
using System.Collections.Generic;

namespace PruebaDeExcel
{
    /// <summary>
    /// Clase con Funciones del Sistema
    /// </summary>
    public static class Funciones
    {
#region Métodos
        /// <summary>
        /// Función para Redimensionar una Imagen
        /// </summary>
        /// <param name = "imageToResize">Imagen a Redimensionar</param>
        /// <param name = "size">Nuevo Tamaño</param>
        /// <param name = "maintainAspect">Mantener Aspecto</param>
        /// <returns>Imagen Redimensionada</returns>
        public static System.Drawing.Image ResizeImage(System.Drawing.Image imageToResize, System.Drawing.Size size, bool maintainAspect)
        {
            int sourceWidth = imageToResize.Width;
            int sourceHeight = imageToResize.Height;
            float percent = 0;
            float percentWidth = 0;
            float percentHeight = 0;
            percentWidth = ((float)size.Width / (float)sourceWidth);
            percentHeight = ((float)size.Height / (float)sourceHeight);
            if (percentHeight < percentWidth)
                percent = percentHeight;
            else
                percent = percentWidth;
            int destinationWidth = (int)(sourceWidth * percent);
            int destinationHeight = (int)(sourceHeight * percent);
            if (!maintainAspect)
            {
                destinationWidth = size.Width;
                destinationHeight = size.Height;
            }

            System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(destinationWidth, destinationHeight);
            System.Drawing.Graphics graphics = System.Drawing.Graphics.FromImage((System.Drawing.Image)bitmap);
            graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBilinear;
            graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            graphics.DrawImage(imageToResize, 0, 0, destinationWidth, destinationHeight);
            graphics.Dispose();
            return (System.Drawing.Image)bitmap;
        }

        /// <summary>
        /// Método Genérico para Convertir Tipos
        /// </summary>
        /// <typeparam name = "T"></typeparam>
        /// <param name = "valor">Valor a convertir</param>
        /// <returns>Valor convertido</returns>
        public static T TryParse<T>(object valor)
        {
            if (valor != null && valor.ToString() == "_")
            {
                valor = "";
            }

            System.ComponentModel.TypeConverter convertidor = System.ComponentModel.TypeDescriptor.GetConverter(typeof(T));
            try
            {
                return (T)convertidor.ConvertFromString(valor.ToString());
            }
            catch
            {
                return default(T);
            }
        }

        /// <summary>
        /// Método Genérico para Convertir Tipos
        /// </summary>
        /// <typeparam name = "T"></typeparam>
        /// <param name = "valor">Valor a convertir</param>
        /// <param name = "predefinido">Valor predeterminado a asignar si existe un error</param>
        /// <returns>Valor convertido</returns>
        public static T TryParse<T>(object valor, object predefinido)
        {
            if (valor != null && valor.ToString() == "_")
            {
                valor = "";
            }

            System.ComponentModel.TypeConverter convertidor = System.ComponentModel.TypeDescriptor.GetConverter(typeof(T));
            try
            {
                var resultado = (T)convertidor.ConvertFromString(valor.ToString());
                if (resultado == null)
                {
                    return (T)predefinido;
                }
                else
                {
                    return resultado;
                }
            }
            catch
            {
                return (T)predefinido;
            }
        }
#endregion
    }
}