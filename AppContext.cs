using Microsoft.AspNetCore.Http;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace PruebaDeExcel
{
    /// <summary>
    /// Contexto de Aplicación
    /// </summary>
    public static class AppContext
    {
        /// <summary>
        /// Acceso a HttpContext
        /// </summary>
        private static IHttpContextAccessor _httpContextAccessor;
        /// <summary>
        /// Configurar
        /// </summary>
        /// <param name = "httpContextAccessor">Acceso a HttpContext</param>
        public static void Configure(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        /// <summary>
        /// Actual
        /// </summary>
        public static HttpContext Current => _httpContextAccessor.HttpContext;
    }
}