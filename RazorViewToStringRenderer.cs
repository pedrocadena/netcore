using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Routing;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Codium.Web
{
    /// <summary>
    /// Razor View To String Renderer
    /// </summary>
    public class RazorViewToStringRenderer : IRazorViewToStringRenderer
    {
        /// <summary>
        /// Private View Engine
        /// </summary>
        private IRazorViewEngine _viewEngine;
        /// <summary>
        /// Private Temp Data Provider
        /// </summary>        
        private ITempDataProvider _tempDataProvider;
        /// <summary>
        /// Private Service Provider
        /// </summary>        
        private IServiceProvider _serviceProvider;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name = "viewEngine">View Engine</param>
        /// <param name = "tempDataProvider">Temp Data Provider</param>
        /// <param name = "serviceProvider">Service Provider</param>
        public RazorViewToStringRenderer(IRazorViewEngine viewEngine, ITempDataProvider tempDataProvider, IServiceProvider serviceProvider)
        {
            _viewEngine = viewEngine;
            _tempDataProvider = tempDataProvider;
            _serviceProvider = serviceProvider;
        }

        /// <summary>
        /// Render View To String Async
        /// </summary>
        /// <param name = "viewName">View Name</param>
        /// <param name = "model">Model</param>
        /// <returns>String</returns>
        public async Task<string> RenderViewToStringAsync<TModel>(string viewName, TModel model)
        {
            var actionContext = GetActionContext();
            var view = FindView(actionContext, viewName);
            using (var output = new StringWriter())
            {
                var viewContext = new ViewContext(actionContext, view, new ViewDataDictionary<TModel>(metadataProvider: new EmptyModelMetadataProvider(), modelState: new ModelStateDictionary())
                {Model = model}, new TempDataDictionary(actionContext.HttpContext, _tempDataProvider), output, new HtmlHelperOptions());
                await view.RenderAsync(viewContext);
                return output.ToString();
            }
        }

        /// <summary>
        /// Find View
        /// </summary>
        /// <param name = "actionContext">Action Context</param>
        /// <param name = "viewName">View Name</param>
        /// <returns>View</returns>
        private IView FindView(ActionContext actionContext, string viewName)
        {
            var getViewResult = _viewEngine.GetView(executingFilePath: null, viewPath: viewName, isMainPage: true);
            if (getViewResult.Success)
            {
                return getViewResult.View;
            }

            var findViewResult = _viewEngine.FindView(actionContext, viewName, isMainPage: true);
            if (findViewResult.Success)
            {
                return findViewResult.View;
            }

            var searchedLocations = getViewResult.SearchedLocations.Concat(findViewResult.SearchedLocations);
            var errorMessage = string.Join(Environment.NewLine, new[]{$"Unable to find view '{viewName}'. The following locations were searched:"}.Concat(searchedLocations));
            ;
            throw new InvalidOperationException(errorMessage);
        }

        /// <summary>
        /// Get Action Context
        /// </summary>
        /// <returns>Action Context</returns>
        private ActionContext GetActionContext()
        {
            var httpContext = new DefaultHttpContext();
            httpContext.RequestServices = _serviceProvider;
            return new ActionContext(httpContext, new RouteData(), new ActionDescriptor());
        }
    }

    /// <summary>
    /// Interface Razor View To String Renderer
    /// </summary>
    public interface IRazorViewToStringRenderer
    {
        /// <summary>
        /// Render View To String Async
        /// </summary>
        /// <param name = "viewName">View Name</param>
        /// <param name = "model">Model</param>
        /// <returns>String</returns>    
        Task<string> RenderViewToStringAsync<TModel>(string viewName, TModel model);
    }
}