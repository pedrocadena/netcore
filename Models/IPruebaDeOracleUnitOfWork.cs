using System;
using System.Threading.Tasks;

namespace Codium.Web.Models
{
    /// <summary>
    /// Prueba de Oracle
    /// </summary>
    public interface IPruebaDeOracleUnitOfWork : IDisposable
    {
#region Variables Privadas
        /// <summary>
        /// tb_menus
        /// </summary>
        PruebaDeOracleRepositorios.ITbMenus TbMenus { get; }
#endregion
    }
}