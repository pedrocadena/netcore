using Microsoft.EntityFrameworkCore;
using System;

namespace Codium.Web.Models
{
    /// <summary>
    /// MySQL®
    /// </summary>
    public class Mysqlr : DbContext
    {
#region Conjuntos
        /// <summary>
        /// CEDAS
        /// </summary>
        public DbSet<MysqlrEntidades.Cedas> Cedas { get; set; }

        /// <summary>
        /// Cedas Imágenes
        /// </summary>
        public DbSet<MysqlrEntidades.CedasImagenes> CedasImagenes { get; set; }

        /// <summary>
        /// Hoja 1
        /// </summary>
        public DbSet<MysqlrEntidades.CedasOriginal> CedasOriginales { get; set; }

        /// <summary>
        /// Estatus
        /// </summary>
        public DbSet<MysqlrEntidades.Estatus> Estatus { get; set; }

        /// <summary>
        /// Menús
        /// </summary>
        public DbSet<MysqlrEntidades.Menus> Menuses { get; set; }

        /// <summary>
        /// PRODUCTORES
        /// </summary>
        public DbSet<MysqlrEntidades.Productores> Productores { get; set; }

        /// <summary>
        /// Roles
        /// </summary>
        public DbSet<MysqlrEntidades.Roles> Roles { get; set; }

        /// <summary>
        /// Usuarios
        /// </summary>
        public DbSet<MysqlrEntidades.Usuarios> Usuarios { get; set; }

#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name = "options">Valores para inicializar el contexto de base de datos</param>
        public Mysqlr(DbContextOptions<Mysqlr> options): base(options)
        {
        }

#endregion
#region Métodos
        /// <summary>
        /// Función de creación de módelo
        /// </summary>  
        /// <param name = "modelBuilder">Constructor de módelo</param>                
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<MysqlrEntidades.Estatus>().HasMany(c => c.EstatusCedas).WithOne(e => e.EstatusEstatus).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<MysqlrEntidades.Cedas>().HasMany(c => c.IdCedaCedasImagenes).WithOne(e => e.IdCedaCedas).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<MysqlrEntidades.Estatus>().HasMany(c => c.EstatusCedasOriginales).WithOne(e => e.EstatusEstatus).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<MysqlrEntidades.Estatus>().HasMany(c => c.EstatusProductores).WithOne(e => e.EstatusEstatus).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<MysqlrEntidades.Cedas>().HasMany(c => c.IdCedasProductores).WithOne(e => e.IdCedasCedas).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<MysqlrEntidades.Roles>().HasMany(c => c.RolUsuarios).WithOne(e => e.RolRoles).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<Codium.Web.Models.MysqlrRepositorios.DTO.Testdeprocedimiento>().HasNoKey();
        }
#endregion
    }
}