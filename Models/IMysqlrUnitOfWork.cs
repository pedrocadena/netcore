using System;
using System.Threading.Tasks;

namespace Codium.Web.Models
{
    /// <summary>
    /// MySQL®
    /// </summary>
    public interface IMysqlrUnitOfWork : IDisposable
    {
#region Variables Privadas
        /// <summary>
        /// MySQL®
        /// </summary>
        MysqlrRepositorios.IProcedimientoAlmacenado ProcedimientoAlmacenado { get; }

        /// <summary>
        /// CEDAS
        /// </summary>
        MysqlrRepositorios.ICedas Cedas { get; }

        /// <summary>
        /// Cedas Imágenes
        /// </summary>
        MysqlrRepositorios.ICedasImagenes CedasImagenes { get; }

        /// <summary>
        /// Hoja 1
        /// </summary>
        MysqlrRepositorios.ICedasOriginal CedasOriginal { get; }

        /// <summary>
        /// Menús
        /// </summary>
        MysqlrRepositorios.IMenus Menus { get; }

        /// <summary>
        /// PRODUCTORES
        /// </summary>
        MysqlrRepositorios.IProductores Productores { get; }

        /// <summary>
        /// Usuarios
        /// </summary>
        MysqlrRepositorios.IUsuarios Usuarios { get; }

#endregion
#region Métodos
        /// <summary>
        /// Guardar Asíncrono
        /// </summary>
        /// <returns>int</returns>
        Task<int> GuardarAsync();
        /// <summary>
        /// Guardar
        /// </summary>
        /// <returns>int</returns>
        int Guardar();
#endregion
    }
}