using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Codium.Web.Models.MysqlrEntidades
{
    /// <summary>
    /// Hoja 1
    /// </summary>
    [Table("TB_SHEET_1")]
    public class CedasOriginal
    {
#region Propiedades
        /// <summary>
        ///  DAP (TON) 
        /// </summary>  
        [Column("FL_BI_COLUMNA_4", TypeName = "bigint")]
        public long? DapTon { get; set; }

        /// <summary>
        ///  UREA (TON) 
        /// </summary>  
        [Column("FL_BI_COLUMNA_3", TypeName = "bigint")]
        public long? UreaTon { get; set; }

        /// <summary>
        /// CLAVE_CEDAS_SADER
        /// </summary>  
        [StringLength(1000)]
        [Column("FL_ST_COLUMNA_2", TypeName = "varchar(1000)")]
        public string ClaveCedasSader { get; set; }

        /// <summary>
        /// ESTADO
        /// </summary>  
        [StringLength(1000)]
        [Column("FL_ST_COLUMNA_6", TypeName = "varchar(1000)")]
        public string Estado { get; set; }

        /// <summary>
        /// Estatus
        /// </summary>  
        [Required]
        [Column("FL_TI_STATUS", TypeName = "tinyint")]
        public SByte Estatus { get; set; }

        /// <summary>
        /// Fecha de Creación
        /// </summary>  
        [Required]
        [Column("FL_DT_CREATED", TypeName = "datetime")]
        public System.DateTime FechaDeCreacion { get; set; }

        /// <summary>
        /// Fecha de Modificación
        /// </summary>  
        [Column("FL_DT_MODIFIED", TypeName = "datetime")]
        public System.DateTime? FechaDeModificacion { get; set; }

        /// <summary>
        /// ID CEDA SEGALMEX
        /// </summary>  
        [StringLength(1000)]
        [Column("FL_ST_COLUMNA_1", TypeName = "varchar(1000)")]
        public string IdCedaSegalmex { get; set; }

        /// <summary>
        /// Llave Primaria
        /// </summary>  
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        [Column("PK_BI_PRIMARY_KEY", TypeName = "bigint")]
        public long LlavePrimaria { get; set; }

        /// <summary>
        /// REGION SEGALMEX
        /// </summary>  
        [StringLength(1000)]
        [Column("FL_ST_COLUMNA_5", TypeName = "varchar(1000)")]
        public string RegionSegalmex { get; set; }

#endregion
#region Relaciones
        /// <summary>
        /// Estatus - Hoja 1
        /// </summary>
        [ForeignKey("Estatus")]
        public Estatus EstatusEstatus { get; set; }

#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        public CedasOriginal()
        {
        }
#endregion
    }
}