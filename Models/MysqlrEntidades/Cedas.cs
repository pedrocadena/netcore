using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Codium.Web.Models.MysqlrEntidades
{
    /// <summary>
    /// CEDAS
    /// </summary>
    [Table("TB_SHEET_2")]
    public class Cedas
    {
#region Propiedades
        /// <summary>
        /// Activo
        /// </summary>  
        [Column("FL_BL_ACTIVO", TypeName = "bit")]
        public bool? Activo { get; set; }

        /// <summary>
        /// CLAVE_CEDAS_SADER
        /// </summary>  
        [StringLength(1000)]
        [Column("FL_ST_COLUMNA_2", TypeName = "varchar(1000)")]
        public string ClaveCedasSader { get; set; }

        /// <summary>
        /// Columna 4 - File name
        /// </summary>  
        [StringLength(200)]
        [Column("FL_ST_FILE_NAME_4", TypeName = "varchar(200)")]
        public string Columna4FileName { get; set; }

        /// <summary>
        /// Columna 4 - Mime Type
        /// </summary>  
        [StringLength(200)]
        [Column("FL_ST_MIME_TYPE_4", TypeName = "varchar(200)")]
        public string Columna4MimeType { get; set; }

        /// <summary>
        /// Columna 4 - Size
        /// </summary>  
        [Column("FL_IN_SIZE_4", TypeName = "int")]
        public int? Columna4Size { get; set; }

        /// <summary>
        /// Decimales
        /// </summary>  
        [Column("FL_DC_DECIMALES", TypeName = "decimal(19,4)")]
        public decimal? Decimales { get; set; }

        /// <summary>
        /// ESTADO
        /// </summary>  
        [StringLength(1000)]
        [Column("FL_ST_COLUMNA_3", TypeName = "varchar(1000)")]
        public string Estado { get; set; }

        /// <summary>
        /// Estatus
        /// </summary>  
        [Required]
        [Column("FL_TI_STATUS", TypeName = "tinyint")]
        public SByte Estatus { get; set; }

        /// <summary>
        /// Fecha de Creación
        /// </summary>  
        [Required]
        [Column("FL_DT_CREATED", TypeName = "datetime")]
        public System.DateTime FechaDeCreacion { get; set; }

        /// <summary>
        /// Fecha de Modificación
        /// </summary>  
        [Column("FL_DT_MODIFIED", TypeName = "datetime")]
        public System.DateTime? FechaDeModificacion { get; set; }

        /// <summary>
        /// ID CEDA SEGALMEX
        /// </summary>  
        [StringLength(1000)]
        [Column("FL_ST_COLUMNA_1", TypeName = "varchar(1000)")]
        public string IdCedaSegalmex { get; set; }

        /// <summary>
        /// Llave Primaria
        /// </summary>  
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        [Column("PK_BI_PRIMARY_KEY", TypeName = "bigint")]
        public long LlavePrimaria { get; set; }

        /// <summary>
        /// Columna 4
        /// </summary>  
        
#nullable enable
        [Newtonsoft.Json.JsonIgnore]
        [Column("FL_FI_COLUMNA_4", TypeName = "longblob")]
        public byte[]? Logotipo { get; set; }

#nullable disable
#endregion
#region Relaciones
        /// <summary>
        /// Estatus - CEDAS
        /// </summary>
        [ForeignKey("Estatus")]
        public Estatus EstatusEstatus { get; set; }

        /// <summary>
        /// CEDAS - PRODUCTORES
        /// </summary>
        public virtual ICollection<Productores> IdCedasProductores { get; set; } = new HashSet<Productores>();
        /// <summary>
        /// Llave Primaria - Id ceda
        /// </summary>
        public virtual ICollection<CedasImagenes> IdCedaCedasImagenes { get; set; } = new HashSet<CedasImagenes>();
#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        public Cedas()
        {
        }
#endregion
    }
}