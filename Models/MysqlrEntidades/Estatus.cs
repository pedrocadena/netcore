using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Codium.Web.Models.MysqlrEntidades
{
    /// <summary>
    /// Estatus
    /// </summary>
    [Table("TB_STATUS")]
    public class Estatus
    {
#region Propiedades
        /// <summary>
        /// Llave Primaria
        /// </summary>  
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        [Column("PK_TI_PRIMARY_KEY", TypeName = "tinyint")]
        public SByte LlavePrimaria { get; set; }

        /// <summary>
        /// Nombre
        /// </summary>  
        [Required]
        [StringLength(50)]
        [Column("FL_ST_NAME", TypeName = "varchar(50)")]
        public string Nombre { get; set; }

#endregion
#region Relaciones
        /// <summary>
        /// Estatus - CEDAS
        /// </summary>
        public virtual ICollection<Cedas> EstatusCedas { get; set; } = new HashSet<Cedas>();
        /// <summary>
        /// Estatus - Hoja 1
        /// </summary>
        public virtual ICollection<CedasOriginal> EstatusCedasOriginales { get; set; } = new HashSet<CedasOriginal>();
        /// <summary>
        /// Estatus - PRODUCTORES
        /// </summary>
        public virtual ICollection<Productores> EstatusProductores { get; set; } = new HashSet<Productores>();
#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        public Estatus()
        {
        }
#endregion
    }
}