using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Codium.Web.Models.MysqlrEntidades
{
    /// <summary>
    /// Usuarios
    /// </summary>
    [Table("TB_USUARIOS")]
    public class Usuarios
    {
#region Propiedades
        /// <summary>
        /// Contraseña
        /// </summary>  
        [Required]
        [StringLength(100)]
        [Column("FL_ST_CONTRASENA", TypeName = "varchar(100)")]
        public string Contrasena { get; set; }

        /// <summary>
        /// Correo
        /// </summary>  
        [Required]
        [StringLength(100)]
        [Column("FL_ST_CORREO", TypeName = "varchar(100)")]
        public string Correo { get; set; }

        /// <summary>
        /// Identificador
        /// </summary>  
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        [Column("FL_IN_IDENTIFICADOR", TypeName = "int")]
        public int Identificador { get; set; }

        /// <summary>
        /// Nombre
        /// </summary>  
        [Required]
        [StringLength(100)]
        [Column("FL_ST_NOMBRE", TypeName = "varchar(100)")]
        public string Nombre { get; set; }

        /// <summary>
        /// Rol
        /// </summary>  
        [Column("FL_IN_ROL", TypeName = "int")]
        public int? Rol { get; set; }

#endregion
#region Relaciones
        /// <summary>
        /// Identificador - Rol
        /// </summary>
        [ForeignKey("Rol")]
        public Roles RolRoles { get; set; }

#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        public Usuarios()
        {
        }
#endregion
    }
}