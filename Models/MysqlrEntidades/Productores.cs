using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Codium.Web.Models.MysqlrEntidades
{
    /// <summary>
    /// PRODUCTORES
    /// </summary>
    [Table("TB_SHEET_3")]
    public class Productores
    {
#region Propiedades
        /// <summary>
        /// APELLIDO MATERNO
        /// </summary>  
        [StringLength(1000)]
        [Column("FL_ST_COLUMNA_3", TypeName = "varchar(1000)")]
        public string ApellidoMaterno { get; set; }

        /// <summary>
        /// APELLIDO PATERNO
        /// </summary>  
        [StringLength(1000)]
        [Column("FL_ST_COLUMNA_2", TypeName = "varchar(1000)")]
        public string ApellidoPaterno { get; set; }

        /// <summary>
        /// CURP PRODUCTOR CUADERNILLO SADER
        /// </summary>  
        [StringLength(1000)]
        [Column("FL_ST_COLUMNA_6", TypeName = "varchar(1000)")]
        public string CurpProductorCuadernilloSader { get; set; }

        /// <summary>
        /// Estatus
        /// </summary>  
        [Required]
        [Column("FL_TI_STATUS", TypeName = "tinyint")]
        public SByte Estatus { get; set; }

        /// <summary>
        /// Fecha de Creación
        /// </summary>  
        [Required]
        [Column("FL_DT_CREATED", TypeName = "datetime")]
        public System.DateTime FechaDeCreacion { get; set; }

        /// <summary>
        /// Fecha de Modificación
        /// </summary>  
        [Column("FL_DT_MODIFIED", TypeName = "datetime")]
        public System.DateTime? FechaDeModificacion { get; set; }

        /// <summary>
        /// Fecha de Registro
        /// </summary>  
        [Column("FL_DT_FECHA_DE_REGISTRO", TypeName = "datetime")]
        public System.DateTime? FechaDeRegistro { get; set; }

        /// <summary>
        /// FOLIO CUADERNILLO SADER
        /// </summary>  
        [Required]
        [StringLength(1000)]
        [Column("FL_ST_COLUMNA_5", TypeName = "varchar(1000)")]
        public string FolioCuadernilloSader { get; set; }

        /// <summary>
        /// ID CEDAS
        /// </summary>  
        [Required]
        [Column("FL_BI_COLUMNA_7", TypeName = "bigint")]
        public long IdCedas { get; set; }

        /// <summary>
        /// Llave Primaria
        /// </summary>  
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        [Column("PK_BI_PRIMARY_KEY", TypeName = "bigint")]
        public long LlavePrimaria { get; set; }

        /// <summary>
        /// NO. DE IDENTIFICACIÓN
        /// </summary>  
        [StringLength(1000)]
        [Column("FL_ST_COLUMNA_1", TypeName = "varchar(1000)")]
        public string NoDeIdentificacion { get; set; }

        /// <summary>
        /// NOMBRE(S)
        /// </summary>  
        [StringLength(1000)]
        [Column("FL_ST_COLUMNA_4", TypeName = "varchar(1000)")]
        public string Nombres { get; set; }

#endregion
#region Relaciones
        /// <summary>
        /// Estatus - PRODUCTORES
        /// </summary>
        [ForeignKey("Estatus")]
        public Estatus EstatusEstatus { get; set; }

        /// <summary>
        /// CEDAS - PRODUCTORES
        /// </summary>
        [ForeignKey("IdCedas")]
        public Cedas IdCedasCedas { get; set; }

#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        public Productores()
        {
        }
#endregion
    }
}