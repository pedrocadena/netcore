using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Codium.Web.Models.MysqlrEntidades
{
    /// <summary>
    /// Menús
    /// </summary>
    [Table("TB_MENUS")]
    public class Menus
    {
#region Propiedades
        /// <summary>
        /// Activo
        /// </summary>  
        [Column("FL_BL_ACTIVO", TypeName = "bit")]
        public bool? Activo { get; set; }

        /// <summary>
        /// Estilo
        /// </summary>  
        [StringLength(20)]
        [Column("FL_ST_ESTILO", TypeName = "varchar(20)")]
        public string Estilo { get; set; }

        /// <summary>
        /// Icono
        /// </summary>  
        [StringLength(200)]
        [Column("FL_ST_ICONO", TypeName = "varchar(200)")]
        public string Icono { get; set; }

        /// <summary>
        /// Identificador
        /// </summary>  
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        [Column("FL_SI_IDENTIFICADOR", TypeName = "smallint")]
        public Int16 Identificador { get; set; }

        /// <summary>
        /// Liga
        /// </summary>  
        [Required]
        [StringLength(100)]
        [Column("FL_ST_LIGA", TypeName = "varchar(100)")]
        public string Liga { get; set; }

        /// <summary>
        /// Nombre
        /// </summary>  
        [Required]
        [StringLength(100)]
        [Column("FL_ST_NOMBRE", TypeName = "varchar(100)")]
        public string Nombre { get; set; }

        /// <summary>
        /// Padre
        /// </summary>  
        [Column("FL_SI_PADRE", TypeName = "smallint")]
        public Int16? Padre { get; set; }

#endregion
#region Relaciones
#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        public Menus()
        {
        }
#endregion
    }
}