using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Codium.Web.Models.MysqlrEntidades
{
    /// <summary>
    /// Cedas Imágenes
    /// </summary>
    [Table("TB_CEDAS_IMAGENES")]
    public class CedasImagenes
    {
#region Propiedades
        /// <summary>
        /// Id ceda
        /// </summary>  
        [Column("FL_BI_ID_CEDA", TypeName = "bigint")]
        public long? IdCeda { get; set; }

        /// <summary>
        /// Id imagen
        /// </summary>  
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        [Column("FL_IN_ID_IMAGEN", TypeName = "int")]
        public int IdImagen { get; set; }

        /// <summary>
        /// Imagen
        /// </summary>  
        [StringLength(1000)]
        [Column("FL_ST_IMAGEN", TypeName = "varchar(1000)")]
        public string Imagen { get; set; }

#endregion
#region Relaciones
        /// <summary>
        /// Llave Primaria - Id ceda
        /// </summary>
        [ForeignKey("IdCeda")]
        public Cedas IdCedaCedas { get; set; }

#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        public CedasImagenes()
        {
        }
#endregion
    }
}