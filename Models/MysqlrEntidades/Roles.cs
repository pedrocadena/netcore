using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Codium.Web.Models.MysqlrEntidades
{
    /// <summary>
    /// Roles
    /// </summary>
    [Table("TB_ROLES")]
    public class Roles
    {
#region Propiedades
        /// <summary>
        /// Identificador
        /// </summary>  
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        [Column("FL_IN_IDENTIFICADOR", TypeName = "int")]
        public int Identificador { get; set; }

        /// <summary>
        /// Nombre
        /// </summary>  
        [Required]
        [StringLength(100)]
        [Column("FL_ST_NOMBRE", TypeName = "varchar(100)")]
        public string Nombre { get; set; }

#endregion
#region Relaciones
        /// <summary>
        /// Identificador - Rol
        /// </summary>
        public virtual ICollection<Usuarios> RolUsuarios { get; set; } = new HashSet<Usuarios>();
#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        public Roles()
        {
        }
#endregion
    }
}