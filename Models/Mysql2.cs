using Microsoft.EntityFrameworkCore;
using System;

namespace Codium.Web.Models
{
    /// <summary>
    /// MySQL®
    /// </summary>
    public class Mysql2 : DbContext
    {
#region Conjuntos
#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name = "options">Valores para inicializar el contexto de base de datos</param>
        public Mysql2(DbContextOptions<Mysql2> options): base(options)
        {
        }

#endregion
#region Métodos
        /// <summary>
        /// Función de creación de módelo
        /// </summary>  
        /// <param name = "modelBuilder">Constructor de módelo</param>                
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
#endregion
    }
}