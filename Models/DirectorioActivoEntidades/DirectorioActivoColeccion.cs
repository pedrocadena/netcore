using System;
using System.Collections.Generic;

namespace Codium.Web.Models.DirectorioActivoEntidades
{
    /// <summary>
    /// Directorio Activo
    /// </summary>
    public class DirectorioActivoColeccion
    {
#region Clases
#endregion
#region Propiedades
        /// <summary>
        /// cn
        /// </summary>
        public string Cn { get; set; }

        /// <summary>
        /// description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// displayName
        /// </summary>
        public string Displayname { get; set; }

        /// <summary>
        /// gidNumber
        /// </summary>
        public int? Gidnumber { get; set; }

        /// <summary>
        /// givenName
        /// </summary>
        public string Givenname { get; set; }

        /// <summary>
        /// homeDirectory
        /// </summary>
        public string Homedirectory { get; set; }

        /// <summary>
        /// initials
        /// </summary>
        public string Initials { get; set; }

        /// <summary>
        /// mail
        /// </summary>
        public string Mail { get; set; }

        /// <summary>
        /// o
        /// </summary>
        public string O { get; set; }

        /// <summary>
        /// sn
        /// </summary>
        public string Sn { get; set; }

        /// <summary>
        /// telephoneNumber
        /// </summary>
        public string Telephonenumber { get; set; }

        /// <summary>
        /// uid
        /// </summary>
        public string Uid { get; set; }

        /// <summary>
        /// uidNumber
        /// </summary>
        public int? Uidnumber { get; set; }

        /// <summary>
        /// userPassword
        /// </summary>
        public byte[]? Userpassword { get; set; }

#endregion
#region Arreglos
#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        public DirectorioActivoColeccion()
        {
        }
#endregion
    }
}