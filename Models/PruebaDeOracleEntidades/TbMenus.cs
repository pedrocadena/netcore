using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Codium.Web.Models.PruebaDeOracleEntidades
{
    /// <summary>
    /// tb_menus
    /// </summary>
    [Table("tb_menus")]
    public class TbMenus
    {
#region Propiedades
        /// <summary>
        /// FL_SI_IDENTIFICADOR
        /// </summary>  
        [Key]
        [Required]
        [Column("FL_SI_IDENTIFICADOR", TypeName = "int")]
        public int FlSiIdentificador { get; set; }

        /// <summary>
        /// FL_SI_PADRE
        /// </summary>  
        [Column("FL_SI_PADRE", TypeName = "int")]
        public int? FlSiPadre { get; set; }

        /// <summary>
        /// FL_ST_ICONO
        /// </summary>  
        [StringLength(50)]
        [Column("FL_ST_ICONO", TypeName = "varchar(50)")]
        public string FlStIcono { get; set; }

        /// <summary>
        /// FL_ST_LIGA
        /// </summary>  
        [Required]
        [StringLength(100)]
        [Column("FL_ST_LIGA", TypeName = "varchar(100)")]
        public string FlStLiga { get; set; }

        /// <summary>
        /// FL_ST_NOMBRE
        /// </summary>  
        [Required]
        [StringLength(100)]
        [Column("FL_ST_NOMBRE", TypeName = "varchar(100)")]
        public string FlStNombre { get; set; }

        /// <summary>
        /// TEST
        /// </summary>  
        [Column("TEST", TypeName = "int")]
        public int? Test { get; set; }

#endregion
#region Relaciones
#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        public TbMenus()
        {
        }
#endregion
    }
}