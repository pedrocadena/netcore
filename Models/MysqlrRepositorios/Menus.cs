using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
namespace Codium.Web.Models.MysqlrRepositorios
{
    /// <summary>
    /// Menús
    /// </summary>
    public class Menus : IMenus
    {
#region Variables Privadas
        /// <summary>
        /// Contexto de Base de Datos
        /// </summary>
        private readonly Mysqlr _mysqlr;
#endregion
#region Métodos
        /// <summary>
        /// Agregar Menus
        /// </summary>
        /// <param name = "menus">Entidad a agregar</param>
        public void AgregarMenus(MysqlrEntidades.Menus menus)
        {
            this._mysqlr.Menuses.Add(menus);
        }

        /// <summary>
        /// Recuperar Menus
        /// </summary>
        /// <param name = "id">Identificador</param>
        /// <returns>MysqlrEntidades.Menus</returns>
        public MysqlrEntidades.Menus RecuperarMenus(Int16 id)
        {
            return this._mysqlr.Menuses.FirstOrDefault(u => u.Identificador == id);
        }

        /// <summary>
        /// Lista Menuses
        /// </summary>
        /// <returns>IEnumerable</returns>
        public IEnumerable<MysqlrEntidades.Menus> ListaMenuses()
        {
            return this._mysqlr.Menuses;
        }

        /// <summary>
        /// Borrar Menus
        /// </summary>
        /// <param name = "id">Identificador</param>
        public bool BorrarMenus(Int16 id)
        {
            bool borrado = false;
            MysqlrEntidades.Menus menus = RecuperarMenus(id);
            if (menus != null)
            {
                borrado = true;
                this._mysqlr.Menuses.Remove(menus);
            }

            return borrado;
        }

        /// <summary>
        /// Menús
        /// </summary>
        /// <param name = "pagina">Página</param>
        /// <param name = "tamanoDePagina">Tamaño de Página</param>
        /// <param name = "ordenarPor">Ordenar por</param>
        /// <param name = "tipo">Tipo</param>
        /// <returns>List</returns>
        public List<DTO.ListaDeMenus> ListaDeMenus(int pagina, int tamanoDePagina, string ordenarPor, string tipo)
        {
            if (pagina == 0)
            {
                pagina = 1;
            }

            if (tamanoDePagina == 0)
            {
                tamanoDePagina = Int32.MaxValue;
            }

            var consulta =
                from Menus in _mysqlr.Menuses.AsQueryable()
                where Menus.Identificador != null /* No es nulo (IS NOT NULL) */
                select new DTO.ListaDeMenus{IconoMenus = Menus.Icono, IdentificadorMenus = Menus.Identificador, LigaMenus = Menus.Liga, NombreMenus = Menus.Nombre, PadreMenus = Menus.Padre, EstiloMenus = Menus.Estilo};
            if (tipo == null || tipo == "")
            {
                tipo = "ASC";
            }

            switch (ordenarPor + tipo)
            {
                case "iconoMenusASC":
                    consulta = consulta.OrderBy(u => u.IconoMenus);
                    break;
                case "iconoMenusDESC":
                    consulta = consulta.OrderByDescending(u => u.IconoMenus);
                    break;
                case "identificadorMenusASC":
                    consulta = consulta.OrderBy(u => u.IdentificadorMenus);
                    break;
                case "identificadorMenusDESC":
                    consulta = consulta.OrderByDescending(u => u.IdentificadorMenus);
                    break;
                case "ligaMenusASC":
                    consulta = consulta.OrderBy(u => u.LigaMenus);
                    break;
                case "ligaMenusDESC":
                    consulta = consulta.OrderByDescending(u => u.LigaMenus);
                    break;
                case "nombreMenusASC":
                    consulta = consulta.OrderBy(u => u.NombreMenus);
                    break;
                case "nombreMenusDESC":
                    consulta = consulta.OrderByDescending(u => u.NombreMenus);
                    break;
                case "padreMenusASC":
                    consulta = consulta.OrderBy(u => u.PadreMenus);
                    break;
                case "padreMenusDESC":
                    consulta = consulta.OrderByDescending(u => u.PadreMenus);
                    break;
                case "estiloMenusASC":
                    consulta = consulta.OrderBy(u => u.EstiloMenus);
                    break;
                case "estiloMenusDESC":
                    consulta = consulta.OrderByDescending(u => u.EstiloMenus);
                    break;
                case "pruebaDeImagenMenusASC":
                    consulta = consulta.OrderBy(u => u.IconoMenus);
                    break;
                case "pruebaDeImagenMenusDESC":
                    consulta = consulta.OrderByDescending(u => u.IconoMenus);
                    break;
                case "pruebaDeDescargaMenusASC":
                    consulta = consulta.OrderBy(u => u.NombreMenus);
                    break;
                case "pruebaDeDescargaMenusDESC":
                    consulta = consulta.OrderByDescending(u => u.NombreMenus);
                    break;
                default:
                    consulta = consulta.OrderBy(u => u.NombreMenus);
                    break;
            }

            consulta = consulta.Skip(tamanoDePagina * (pagina - 1)).Take(tamanoDePagina);
            return consulta.ToList();
        }

        /// <summary>
        /// Método que recupera el total de elementos.
        /// </summary>
        /// <returns>int</returns>
        public int ListaDeMenusTotal()
        {
            var consulta =
                from Menus in _mysqlr.Menuses.AsQueryable()
                where Menus.Identificador != null /* No es nulo (IS NOT NULL) */
                select new
                {
                fl_unique = 1
                }

            ;
            return consulta.Count();
        }

#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        public Menus(Mysqlr mysqlr)
        {
            this._mysqlr = mysqlr;
        }
#endregion
    }
}