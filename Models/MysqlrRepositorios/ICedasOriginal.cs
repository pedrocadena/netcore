using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Codium.Web.Models.MysqlrRepositorios
{
    /// <summary>
    /// Hoja 1
    /// </summary>
    public interface ICedasOriginal
    {
#region Métodos
        /// <summary>
        /// Agregar CedasOriginal
        /// </summary>
        /// <param name = "cedasOriginal">Entidad a agregar</param>
        void AgregarCedasOriginal(MysqlrEntidades.CedasOriginal cedasOriginal);
        /// <summary>
        /// Recuperar CedasOriginal
        /// </summary>
        /// <param name = "id">Identificador</param>
        /// <returns>MysqlrEntidades.CedasOriginal</returns>
        MysqlrEntidades.CedasOriginal RecuperarCedasOriginal(long id);
        /// <summary>
        /// Lista CedasOriginales
        /// </summary>
        /// <returns>IEnumerable</returns>
        IEnumerable<MysqlrEntidades.CedasOriginal> ListaCedasOriginales();
        /// <summary>
        /// Borrar CedasOriginal
        /// </summary>
        /// <param name = "id">Identificador</param>
        bool BorrarCedasOriginal(long id);
        /// <summary>
        /// Actualizar Cedas Original
        /// </summary>
        /// <param name = "estatusCedasOriginal1">Estatus (Cedas Original) Igual a (=?)</param>
        /// <param name = "llavePrimariaCedasOriginal2">Llave Primaria (Cedas Original) Igual a (=?)</param>
        void ActualizarCedasOriginal(SByte estatusCedasOriginal1, long? llavePrimariaCedasOriginal2);
        /// <summary>
        /// Detalle Cedas Original
        /// </summary>
        /// <param name = "llavePrimariaCedasOriginal">Llave Primaria (Cedas Original) Igual a (=?)</param>
        /// <param name = "pagina">Página</param>
        /// <param name = "tamanoDePagina">Tamaño de Página</param>
        /// <param name = "ordenarPor">Ordenar por</param>
        /// <param name = "tipo">Tipo</param>
        /// <returns>List</returns>
        List<DTO.DetalleCedasOriginal> DetalleCedasOriginal(long? llavePrimariaCedasOriginal, int pagina, int tamanoDePagina, string ordenarPor, string tipo);
        /// <summary>
        /// Método que recupera el total de elementos.
        /// </summary>
        /// <param name = "llavePrimariaCedasOriginal">Llave Primaria (Cedas Original) Igual a (=?)</param>
        /// <returns>int</returns>
        int DetalleCedasOriginalTotal(long? llavePrimariaCedasOriginal);
        /// <summary>
        /// Lista Cedas Original
        /// </summary>
        /// <param name = "pagina">Página</param>
        /// <param name = "tamanoDePagina">Tamaño de Página</param>
        /// <param name = "ordenarPor">Ordenar por</param>
        /// <param name = "tipo">Tipo</param>
        /// <returns>List</returns>
        List<DTO.ListaCedasOriginal> ListaCedasOriginal(int pagina, int tamanoDePagina, string ordenarPor, string tipo);
        /// <summary>
        /// Método que recupera el total de elementos.
        /// </summary>
        /// <returns>int</returns>
        int ListaCedasOriginalTotal();
        /// <summary>
        /// Tabla de Datos Cedas Original
        /// </summary>
        /// <param name = "claveCedasSaderCedasOriginal">CLAVE_CEDAS_SADER (Cedas Original) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "estadoCedasOriginal">ESTADO (Cedas Original) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "idCedaSegalmexCedasOriginal">ID CEDA SEGALMEX (Cedas Original) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "regionSegalmexCedasOriginal">REGION SEGALMEX (Cedas Original) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "pagina">Página</param>
        /// <param name = "tamanoDePagina">Tamaño de Página</param>
        /// <param name = "ordenarPor">Ordenar por</param>
        /// <param name = "tipo">Tipo</param>
        /// <returns>List</returns>
        List<DTO.TablaDeDatosCedasOriginal> TablaDeDatosCedasOriginal(string claveCedasSaderCedasOriginal, string estadoCedasOriginal, string idCedaSegalmexCedasOriginal, string regionSegalmexCedasOriginal, int pagina, int tamanoDePagina, string ordenarPor, string tipo);
        /// <summary>
        /// Método que recupera el total de elementos.
        /// </summary>
        /// <param name = "claveCedasSaderCedasOriginal">CLAVE_CEDAS_SADER (Cedas Original) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "estadoCedasOriginal">ESTADO (Cedas Original) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "idCedaSegalmexCedasOriginal">ID CEDA SEGALMEX (Cedas Original) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "regionSegalmexCedasOriginal">REGION SEGALMEX (Cedas Original) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <returns>int</returns>
        int TablaDeDatosCedasOriginalTotal(string claveCedasSaderCedasOriginal, string estadoCedasOriginal, string idCedaSegalmexCedasOriginal, string regionSegalmexCedasOriginal);
#endregion
    }
}