using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
namespace Codium.Web.Models.MysqlrRepositorios
{
    /// <summary>
    /// Cedas Imágenes
    /// </summary>
    public class CedasImagenes : ICedasImagenes
    {
#region Variables Privadas
        /// <summary>
        /// Contexto de Base de Datos
        /// </summary>
        private readonly Mysqlr _mysqlr;
#endregion
#region Métodos
        /// <summary>
        /// Agregar CedasImagenes
        /// </summary>
        /// <param name = "cedasImagenes">Entidad a agregar</param>
        public void AgregarCedasImagenes(MysqlrEntidades.CedasImagenes cedasImagenes)
        {
            this._mysqlr.CedasImagenes.Add(cedasImagenes);
        }

        /// <summary>
        /// Recuperar CedasImagenes
        /// </summary>
        /// <param name = "id">Identificador</param>
        /// <returns>MysqlrEntidades.CedasImagenes</returns>
        public MysqlrEntidades.CedasImagenes RecuperarCedasImagenes(int id)
        {
            return this._mysqlr.CedasImagenes.FirstOrDefault(u => u.IdImagen == id);
        }

        /// <summary>
        /// Lista CedasImagenes
        /// </summary>
        /// <returns>IEnumerable</returns>
        public IEnumerable<MysqlrEntidades.CedasImagenes> ListaCedasImagenes()
        {
            return this._mysqlr.CedasImagenes;
        }

        /// <summary>
        /// Borrar CedasImagenes
        /// </summary>
        /// <param name = "id">Identificador</param>
        public bool BorrarCedasImagenes(int id)
        {
            bool borrado = false;
            MysqlrEntidades.CedasImagenes cedasImagenes = RecuperarCedasImagenes(id);
            if (cedasImagenes != null)
            {
                borrado = true;
                this._mysqlr.CedasImagenes.Remove(cedasImagenes);
            }

            return borrado;
        }

        /// <summary>
        /// Imagenes
        /// </summary>
        /// <param name = "idCedaCedasImagenes">Id ceda (Cedas Im&#225;genes) Igual a (=?)</param>
        /// <param name = "pagina">Página</param>
        /// <param name = "tamanoDePagina">Tamaño de Página</param>
        /// <param name = "ordenarPor">Ordenar por</param>
        /// <param name = "tipo">Tipo</param>
        /// <returns>List</returns>
        public List<DTO.Imagenes> Imagenes(long? idCedaCedasImagenes, int pagina, int tamanoDePagina, string ordenarPor, string tipo)
        {
            if (pagina == 0)
            {
                pagina = 1;
            }

            if (tamanoDePagina == 0)
            {
                tamanoDePagina = Int32.MaxValue;
            }

            var consulta =
                from CedasImagenes in _mysqlr.CedasImagenes.AsQueryable()
                where (idCedaCedasImagenes == null || CedasImagenes.IdCeda == idCedaCedasImagenes /* Igual a (=?) */)select new DTO.Imagenes{ImagenCedasImagenes = CedasImagenes.Imagen, IdImagenCedasImagenes = CedasImagenes.IdImagen};
            if (tipo == null || tipo == "")
            {
                tipo = "ASC";
            }

            switch (ordenarPor + tipo)
            {
                case "imagenCedasImagenesASC":
                    consulta = consulta.OrderBy(u => u.ImagenCedasImagenes);
                    break;
                case "imagenCedasImagenesDESC":
                    consulta = consulta.OrderByDescending(u => u.ImagenCedasImagenes);
                    break;
                default:
                    consulta = consulta.OrderBy(u => u.IdImagenCedasImagenes);
                    break;
            }

            consulta = consulta.Skip(tamanoDePagina * (pagina - 1)).Take(tamanoDePagina);
            return consulta.ToList();
        }

        /// <summary>
        /// Método que recupera el total de elementos.
        /// </summary>
        /// <param name = "idCedaCedasImagenes">Id ceda (Cedas Im&#225;genes) Igual a (=?)</param>
        /// <returns>int</returns>
        public int ImagenesTotal(long? idCedaCedasImagenes)
        {
            var consulta =
                from CedasImagenes in _mysqlr.CedasImagenes.AsQueryable()
                where (idCedaCedasImagenes == null || CedasImagenes.IdCeda == idCedaCedasImagenes /* Igual a (=?) */)select new
                {
                fl_unique = 1
                }

            ;
            return consulta.Count();
        }

#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        public CedasImagenes(Mysqlr mysqlr)
        {
            this._mysqlr = mysqlr;
        }
#endregion
    }
}