using Microsoft.AspNetCore.DataProtection;

using System;
using System.Collections.Generic;
using System.Linq;
namespace Codium.Web.Models.MysqlrRepositorios.DTO
{
    /// <summary>
    /// Tabla de Datos Cedas
    /// </summary>
    public class TablaDeDatosCedas
    {
#region Propiedades
        /// <summary>
        /// Llave Primaria
        /// </summary>  
        public long LlavePrimariaCedas { get; set; }

        /// <summary>
        /// Fecha de Creación
        /// </summary>  
        public System.DateTime FechaDeCreacionCedas { get; set; }

        /// <summary>
        /// Estatus
        /// </summary>  
        public SByte EstatusCedas { get; set; }

        /// <summary>
        /// Fecha de Modificación
        /// </summary>  
        public System.DateTime? FechaDeModificacionCedas { get; set; }

        /// <summary>
        /// CLAVE_CEDAS_SADER
        /// </summary>  
        public string ClaveCedasSaderCedas { get; set; }

        /// <summary>
        /// ESTADO
        /// </summary>  
        public string EstadoCedas { get; set; }

        /// <summary>
        /// ID CEDA SEGALMEX
        /// </summary>  
        public string IdCedaSegalmexCedas { get; set; }

        /// <summary>
        /// Decimales
        /// </summary>  
        public decimal? DecimalesCedas { get; set; }

        /// <summary>
        /// Columna 4
        /// </summary>  
        
#nullable enable
        [Newtonsoft.Json.JsonIgnore]
        public byte[]? LogotipoCedas { get; set; }

#nullable disable
        /// <summary>
        /// Columna 4 - Mime Type
        /// </summary>  
        public string Columna4MimeTypeCedas { get; set; }

        /// <summary>
        /// Columna 4 - File name
        /// </summary>  
        public string Columna4FileNameCedas { get; set; }

        /// <summary>
        /// Nombre
        /// </summary>  
        public string NombreEstatus { get; set; }

#endregion
#region Calculados
        /// <summary>
        /// Variable con el idioma de la aplicación
        /// </summary> 
        [Newtonsoft.Json.JsonIgnore]
        public Microsoft.Extensions.Localization.IStringLocalizer<PruebaDeExcel.RecursosCompartidos> Idioma { get; set; }

        /// <summary>
        /// Objeto para encriptar y desencriptar valores
        /// </summary>
        [Newtonsoft.Json.JsonIgnore]
        public Microsoft.AspNetCore.DataProtection.IDataProtector Protector { get; set; }

        /// <summary>
        /// Llave Primaria (Encriptado)
        /// </summary> 
        public string LlavePrimariaEncriptadoCedas
        {
            get
            {
                return this.Protector.Protect(this.LlavePrimariaCedas.ToString());
            }
        }

        /// <summary>
        /// Fecha de Creación (Formateado)
        /// </summary> 
        public string FechaDeCreacionFormateadoCedas
        {
            get
            {
                return this.FechaDeCreacionCedas.ToString(this.Idioma["Shortdatepattern"].Value, new System.Globalization.CultureInfo(this.Idioma["LanguageIdentifier"].Value));
            }
        }

        /// <summary>
        /// Fecha de Modificación (Formateado)
        /// </summary> 
        public string FechaDeModificacionFormateadoCedas
        {
            get
            {
                return this.FechaDeCreacionCedas.ToString(this.Idioma["Shortdatepattern"].Value, new System.Globalization.CultureInfo(this.Idioma["LanguageIdentifier"].Value));
            }
        }

        /// <summary>
        /// Descargar Logotipo
        /// </summary> 
        public string DescargarLogotipoCedas
        {
            get
            {
                if (this.LogotipoCedas != null)
                {
                    return "<a href=\"javascript:;\" onclick=\"location='/Aplicacion/DescargarLogotipo/" + this.Protector.Protect(this.LlavePrimariaCedas.ToString()) + "';\"><img src=\"/assets/Prueba_Download.png\" /></a>";
                }
                else
                {
                    return "";
                }
            }
        }

        /// <summary>
        /// Condicional Logotipo
        /// </summary> 
        public string CondicionalLogotipoCedas
        {
            get
            {
                if (this.LogotipoCedas != null)
                {
                    return this.DescargarLogotipoCedas;
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Formula de prueba
        /// </summary> 
        public decimal FormulaDePruebaCedas
        {
            get
            {
                return System.Convert.ToDecimal(System.Math.Pow((this.DecimalesCedas.HasValue ? Decimal.ToDouble(this.DecimalesCedas.Value) : 0D), 2) + 1.2D - (Enumerable.Range(1, 10).Select(i => this.LlavePrimariaCedas).Sum()) + (Enumerable.Range(1, (int)this.LlavePrimariaCedas).Aggregate(1, (p, item) => p * item)));
            }
        }

#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        public TablaDeDatosCedas()
        {
            this.Protector = ((Microsoft.AspNetCore.DataProtection.IDataProtectionProvider)PruebaDeExcel.AppContext.Current.RequestServices.GetService(typeof(Microsoft.AspNetCore.DataProtection.IDataProtectionProvider))).CreateProtector("URLProtection");
            this.Idioma = (Microsoft.Extensions.Localization.IStringLocalizer<PruebaDeExcel.RecursosCompartidos>)PruebaDeExcel.AppContext.Current.RequestServices.GetService(typeof(Microsoft.Extensions.Localization.IStringLocalizer<PruebaDeExcel.RecursosCompartidos>));
        }
#endregion
    }
}