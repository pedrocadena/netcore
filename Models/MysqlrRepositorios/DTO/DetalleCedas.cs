using Microsoft.AspNetCore.DataProtection;

using System;
using System.Collections.Generic;
using System.Linq;
namespace Codium.Web.Models.MysqlrRepositorios.DTO
{
    /// <summary>
    /// Detalle Cedas
    /// </summary>
    public class DetalleCedas
    {
#region Propiedades
        /// <summary>
        /// Llave Primaria
        /// </summary>  
        public long LlavePrimariaCedas { get; set; }

        /// <summary>
        /// CLAVE_CEDAS_SADER
        /// </summary>  
        public string ClaveCedasSaderCedas { get; set; }

        /// <summary>
        /// ESTADO
        /// </summary>  
        public string EstadoCedas { get; set; }

        /// <summary>
        /// ID CEDA SEGALMEX
        /// </summary>  
        public string IdCedaSegalmexCedas { get; set; }

        /// <summary>
        /// Decimales
        /// </summary>  
        public decimal? DecimalesCedas { get; set; }

        /// <summary>
        /// Fecha de Creación
        /// </summary>  
        public System.DateTime FechaDeCreacionCedas { get; set; }

        /// <summary>
        /// Fecha de Modificación
        /// </summary>  
        public System.DateTime? FechaDeModificacionCedas { get; set; }

        /// <summary>
        /// Activo
        /// </summary>  
        public bool? ActivoCedas { get; set; }

#endregion
#region Calculados
        /// <summary>
        /// Variable con el idioma de la aplicación
        /// </summary> 
        [Newtonsoft.Json.JsonIgnore]
        public Microsoft.Extensions.Localization.IStringLocalizer<PruebaDeExcel.RecursosCompartidos> Idioma { get; set; }

        /// <summary>
        /// Objeto para encriptar y desencriptar valores
        /// </summary>
        [Newtonsoft.Json.JsonIgnore]
        public Microsoft.AspNetCore.DataProtection.IDataProtector Protector { get; set; }

        /// <summary>
        /// Imagenes
        /// </summary> 
        public List<Codium.Web.Models.MysqlrRepositorios.DTO.Imagenes> ImagenesCedas
        {
            get
            {
                Codium.Web.Models.IMysqlrUnitOfWork mysqlr = (Codium.Web.Models.IMysqlrUnitOfWork)PruebaDeExcel.AppContext.Current.RequestServices.GetService(typeof(Codium.Web.Models.IMysqlrUnitOfWork));
                return mysqlr.CedasImagenes.Imagenes(PruebaDeExcel.Funciones.TryParse<long?>(this.LlavePrimariaCedas, 0L), 0, 0, "", "");
            }
        }

#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        public DetalleCedas()
        {
            this.Protector = ((Microsoft.AspNetCore.DataProtection.IDataProtectionProvider)PruebaDeExcel.AppContext.Current.RequestServices.GetService(typeof(Microsoft.AspNetCore.DataProtection.IDataProtectionProvider))).CreateProtector("URLProtection");
            this.Idioma = (Microsoft.Extensions.Localization.IStringLocalizer<PruebaDeExcel.RecursosCompartidos>)PruebaDeExcel.AppContext.Current.RequestServices.GetService(typeof(Microsoft.Extensions.Localization.IStringLocalizer<PruebaDeExcel.RecursosCompartidos>));
        }
#endregion
    }
}