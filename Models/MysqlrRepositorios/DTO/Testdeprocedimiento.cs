using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Codium.Web.Models.MysqlrRepositorios.DTO
{
    /// <summary>
    /// TestDeProcedimiento
    /// </summary>
    public class Testdeprocedimiento
    {
#region Propiedades
        /// <summary>
        /// FL_BL_ACTIVO
        /// </summary>  
        [Column("FL_BL_ACTIVO", TypeName = "bigint")]
        public long? FlBlActivo { get; set; }

        /// <summary>
        /// FL_SI_IDENTIFICADOR
        /// </summary>  
        [Required]
        [Column("FL_SI_IDENTIFICADOR", TypeName = "smallint")]
        public Int16 FlSiIdentificador { get; set; }

        /// <summary>
        /// FL_SI_PADRE
        /// </summary>  
        [Required]
        [Column("FL_SI_PADRE", TypeName = "smallint")]
        public Int16 FlSiPadre { get; set; }

        /// <summary>
        /// FL_ST_ESTILO
        /// </summary>  
        [StringLength(2147483647)]
        [Column("FL_ST_ESTILO", TypeName = "varchar(0)")]
        public string FlStEstilo { get; set; }

        /// <summary>
        /// FL_ST_ICONO
        /// </summary>  
        [Key]
        [StringLength(2147483647)]
        [Column("FL_ST_ICONO", TypeName = "varchar(0)")]
        public string FlStIcono { get; set; }

        /// <summary>
        /// FL_ST_LIGA
        /// </summary>  
        [Required]
        [StringLength(2147483647)]
        [Column("FL_ST_LIGA", TypeName = "varchar(0)")]
        public string FlStLiga { get; set; }

        /// <summary>
        /// FL_ST_NOMBRE
        /// </summary>  
        [Required]
        [StringLength(2147483647)]
        [Column("FL_ST_NOMBRE", TypeName = "varchar(0)")]
        public string FlStNombre { get; set; }

        /// <summary>
        /// TEST
        /// </summary>  
        [Required]
        [Column("TEST", TypeName = "bigint")]
        public long Test { get; set; }

#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        public Testdeprocedimiento()
        {
        }
#endregion
    }
}