using Microsoft.AspNetCore.DataProtection;

using System;
using System.Collections.Generic;
using System.Linq;
namespace Codium.Web.Models.MysqlrRepositorios.DTO
{
    /// <summary>
    /// Lista Cedas Original
    /// </summary>
    public class ListaCedasOriginal
    {
#region Propiedades
        /// <summary>
        /// Llave Primaria
        /// </summary>  
        public long LlavePrimariaCedasOriginal { get; set; }

        /// <summary>
        ///  DAP (TON) 
        /// </summary>  
        public long? DapTonCedasOriginal { get; set; }

        /// <summary>
        ///  UREA (TON) 
        /// </summary>  
        public long? UreaTonCedasOriginal { get; set; }

        /// <summary>
        /// CLAVE_CEDAS_SADER
        /// </summary>  
        public string ClaveCedasSaderCedasOriginal { get; set; }

        /// <summary>
        /// ESTADO
        /// </summary>  
        public string EstadoCedasOriginal { get; set; }

        /// <summary>
        /// ID CEDA SEGALMEX
        /// </summary>  
        public string IdCedaSegalmexCedasOriginal { get; set; }

        /// <summary>
        /// REGION SEGALMEX
        /// </summary>  
        public string RegionSegalmexCedasOriginal { get; set; }

#endregion
#region Calculados
        /// <summary>
        /// Variable con el idioma de la aplicación
        /// </summary> 
        [Newtonsoft.Json.JsonIgnore]
        public Microsoft.Extensions.Localization.IStringLocalizer<PruebaDeExcel.RecursosCompartidos> Idioma { get; set; }

        /// <summary>
        /// Objeto para encriptar y desencriptar valores
        /// </summary>
        [Newtonsoft.Json.JsonIgnore]
        public Microsoft.AspNetCore.DataProtection.IDataProtector Protector { get; set; }

#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        public ListaCedasOriginal()
        {
            this.Protector = ((Microsoft.AspNetCore.DataProtection.IDataProtectionProvider)PruebaDeExcel.AppContext.Current.RequestServices.GetService(typeof(Microsoft.AspNetCore.DataProtection.IDataProtectionProvider))).CreateProtector("URLProtection");
            this.Idioma = (Microsoft.Extensions.Localization.IStringLocalizer<PruebaDeExcel.RecursosCompartidos>)PruebaDeExcel.AppContext.Current.RequestServices.GetService(typeof(Microsoft.Extensions.Localization.IStringLocalizer<PruebaDeExcel.RecursosCompartidos>));
        }
#endregion
    }
}