using Microsoft.AspNetCore.DataProtection;

using System;
using System.Collections.Generic;
using System.Linq;
namespace Codium.Web.Models.MysqlrRepositorios.DTO
{
    /// <summary>
    /// Menús
    /// </summary>
    public class ListaDeMenus
    {
#region Propiedades
        /// <summary>
        /// Icono
        /// </summary>  
        public string IconoMenus { get; set; }

        /// <summary>
        /// Identificador
        /// </summary>  
        public Int16 IdentificadorMenus { get; set; }

        /// <summary>
        /// Liga
        /// </summary>  
        public string LigaMenus { get; set; }

        /// <summary>
        /// Nombre
        /// </summary>  
        public string NombreMenus { get; set; }

        /// <summary>
        /// Padre
        /// </summary>  
        public Int16? PadreMenus { get; set; }

        /// <summary>
        /// Estilo
        /// </summary>  
        public string EstiloMenus { get; set; }

#endregion
#region Calculados
        /// <summary>
        /// Variable con el idioma de la aplicación
        /// </summary> 
        [Newtonsoft.Json.JsonIgnore]
        public Microsoft.Extensions.Localization.IStringLocalizer<PruebaDeExcel.RecursosCompartidos> Idioma { get; set; }

        /// <summary>
        /// Objeto para encriptar y desencriptar valores
        /// </summary>
        [Newtonsoft.Json.JsonIgnore]
        public Microsoft.AspNetCore.DataProtection.IDataProtector Protector { get; set; }

        /// <summary>
        /// Prueba de imagen
        /// </summary> 
        public string PruebaDeImagenMenus
        {
            get
            {
                if (this.IconoMenus != null)
                {
                    return "<img src=\"/Aplicacion/PruebaDeImagen/" + this.Protector.Protect(this.IdentificadorMenus.ToString()) + "\" />";
                }
                else
                {
                    return "<img src=\"/assets/Prueba_Notfound.png\" style=\"max-width:200px;max-height:200px\" />";
                }
            }
        }

        /// <summary>
        /// Prueba de descarga
        /// </summary> 
        public string PruebaDeDescargaMenus
        {
            get
            {
                if (this.IconoMenus != null)
                {
                    return "<a href=\"javascript:;\" onclick=\"location='/Aplicacion/PruebaDeDescarga/" + this.Protector.Protect(this.IdentificadorMenus.ToString()) + "';\"><img src=\"/assets/Prueba_Add.png\" /></a>";
                }
                else
                {
                    return "";
                }
            }
        }

#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        public ListaDeMenus()
        {
            this.Protector = ((Microsoft.AspNetCore.DataProtection.IDataProtectionProvider)PruebaDeExcel.AppContext.Current.RequestServices.GetService(typeof(Microsoft.AspNetCore.DataProtection.IDataProtectionProvider))).CreateProtector("URLProtection");
            this.Idioma = (Microsoft.Extensions.Localization.IStringLocalizer<PruebaDeExcel.RecursosCompartidos>)PruebaDeExcel.AppContext.Current.RequestServices.GetService(typeof(Microsoft.Extensions.Localization.IStringLocalizer<PruebaDeExcel.RecursosCompartidos>));
        }
#endregion
    }
}