using Microsoft.AspNetCore.DataProtection;

using System;
using System.Collections.Generic;
using System.Linq;
namespace Codium.Web.Models.MysqlrRepositorios.DTO
{
    /// <summary>
    /// Lista Productores
    /// </summary>
    public class ListaProductores
    {
#region Propiedades
        /// <summary>
        /// Llave Primaria
        /// </summary>  
        public long LlavePrimariaProductores { get; set; }

        /// <summary>
        /// APELLIDO MATERNO
        /// </summary>  
        public string ApellidoMaternoProductores { get; set; }

        /// <summary>
        /// APELLIDO PATERNO
        /// </summary>  
        public string ApellidoPaternoProductores { get; set; }

        /// <summary>
        /// CURP PRODUCTOR CUADERNILLO SADER
        /// </summary>  
        public string CurpProductorCuadernilloSaderProductores { get; set; }

        /// <summary>
        /// FOLIO CUADERNILLO SADER
        /// </summary>  
        public string FolioCuadernilloSaderProductores { get; set; }

        /// <summary>
        /// NO. DE IDENTIFICACIÓN
        /// </summary>  
        public string NoDeIdentificacionProductores { get; set; }

        /// <summary>
        /// NOMBRE(S)
        /// </summary>  
        public string NombresProductores { get; set; }

        /// <summary>
        /// Fecha de Registro
        /// </summary>  
        public System.DateTime? FechaDeRegistroProductores { get; set; }

#endregion
#region Calculados
        /// <summary>
        /// Variable con el idioma de la aplicación
        /// </summary> 
        [Newtonsoft.Json.JsonIgnore]
        public Microsoft.Extensions.Localization.IStringLocalizer<PruebaDeExcel.RecursosCompartidos> Idioma { get; set; }

        /// <summary>
        /// Objeto para encriptar y desencriptar valores
        /// </summary>
        [Newtonsoft.Json.JsonIgnore]
        public Microsoft.AspNetCore.DataProtection.IDataProtector Protector { get; set; }

#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        public ListaProductores()
        {
            this.Protector = ((Microsoft.AspNetCore.DataProtection.IDataProtectionProvider)PruebaDeExcel.AppContext.Current.RequestServices.GetService(typeof(Microsoft.AspNetCore.DataProtection.IDataProtectionProvider))).CreateProtector("URLProtection");
            this.Idioma = (Microsoft.Extensions.Localization.IStringLocalizer<PruebaDeExcel.RecursosCompartidos>)PruebaDeExcel.AppContext.Current.RequestServices.GetService(typeof(Microsoft.Extensions.Localization.IStringLocalizer<PruebaDeExcel.RecursosCompartidos>));
        }
#endregion
    }
}