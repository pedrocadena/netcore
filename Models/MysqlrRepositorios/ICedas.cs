using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Codium.Web.Models.MysqlrRepositorios
{
    /// <summary>
    /// CEDAS
    /// </summary>
    public interface ICedas
    {
#region Métodos
        /// <summary>
        /// Agregar Cedas
        /// </summary>
        /// <param name = "cedas">Entidad a agregar</param>
        void AgregarCedas(MysqlrEntidades.Cedas cedas);
        /// <summary>
        /// Recuperar Cedas
        /// </summary>
        /// <param name = "id">Identificador</param>
        /// <returns>MysqlrEntidades.Cedas</returns>
        MysqlrEntidades.Cedas RecuperarCedas(long id);
        /// <summary>
        /// Lista Cedas
        /// </summary>
        /// <returns>IEnumerable</returns>
        IEnumerable<MysqlrEntidades.Cedas> ListaCedas();
        /// <summary>
        /// Borrar Cedas
        /// </summary>
        /// <param name = "id">Identificador</param>
        bool BorrarCedas(long id);
        /// <summary>
        /// Actualizar Cedas
        /// </summary>
        /// <param name = "estatusCedas1">Estatus (Cedas) Igual a (=?)</param>
        /// <param name = "llavePrimariaCedas2">Llave Primaria (Cedas) Igual a (=?)</param>
        void ActualizarCedas(SByte estatusCedas1, long? llavePrimariaCedas2);
        /// <summary>
        /// Detalle Cedas
        /// </summary>
        /// <param name = "llavePrimariaCedas">Llave Primaria (Cedas) Igual a (=?)</param>
        /// <param name = "pagina">Página</param>
        /// <param name = "tamanoDePagina">Tamaño de Página</param>
        /// <param name = "ordenarPor">Ordenar por</param>
        /// <param name = "tipo">Tipo</param>
        /// <returns>List</returns>
        List<DTO.DetalleCedas> DetalleCedas(long llavePrimariaCedas, int pagina, int tamanoDePagina, string ordenarPor, string tipo);
        /// <summary>
        /// Método que recupera el total de elementos.
        /// </summary>
        /// <param name = "llavePrimariaCedas">Llave Primaria (Cedas) Igual a (=?)</param>
        /// <returns>int</returns>
        int DetalleCedasTotal(long llavePrimariaCedas);
        /// <summary>
        /// Lista Cedas
        /// </summary>
        /// <param name = "pagina">Página</param>
        /// <param name = "tamanoDePagina">Tamaño de Página</param>
        /// <param name = "ordenarPor">Ordenar por</param>
        /// <param name = "tipo">Tipo</param>
        /// <returns>List</returns>
        List<DTO.ListaCedas> ListaCedas(int pagina, int tamanoDePagina, string ordenarPor, string tipo);
        /// <summary>
        /// Método que recupera el total de elementos.
        /// </summary>
        /// <returns>int</returns>
        int ListaCedasTotal();
        /// <summary>
        /// Tabla de Datos Cedas
        /// </summary>
        /// <param name = "claveCedasSaderCedas">CLAVE_CEDAS_SADER (Cedas) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "estadoCedas">ESTADO (Cedas) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "idCedaSegalmexCedas">ID CEDA SEGALMEX (Cedas) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "pagina">Página</param>
        /// <param name = "tamanoDePagina">Tamaño de Página</param>
        /// <param name = "ordenarPor">Ordenar por</param>
        /// <param name = "tipo">Tipo</param>
        /// <returns>List</returns>
        List<DTO.TablaDeDatosCedas> TablaDeDatosCedas(string claveCedasSaderCedas, string estadoCedas, string idCedaSegalmexCedas, int pagina, int tamanoDePagina, string ordenarPor, string tipo);
        /// <summary>
        /// Método que recupera el total de elementos.
        /// </summary>
        /// <param name = "claveCedasSaderCedas">CLAVE_CEDAS_SADER (Cedas) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "estadoCedas">ESTADO (Cedas) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "idCedaSegalmexCedas">ID CEDA SEGALMEX (Cedas) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <returns>int</returns>
        int TablaDeDatosCedasTotal(string claveCedasSaderCedas, string estadoCedas, string idCedaSegalmexCedas);
#endregion
    }
}