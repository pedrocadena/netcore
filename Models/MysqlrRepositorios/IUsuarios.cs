using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Codium.Web.Models.MysqlrRepositorios
{
    /// <summary>
    /// Usuarios
    /// </summary>
    public interface IUsuarios
    {
#region Métodos
        /// <summary>
        /// Agregar Usuarios
        /// </summary>
        /// <param name = "usuarios">Entidad a agregar</param>
        void AgregarUsuarios(MysqlrEntidades.Usuarios usuarios);
        /// <summary>
        /// Recuperar Usuarios
        /// </summary>
        /// <param name = "id">Identificador</param>
        /// <returns>MysqlrEntidades.Usuarios</returns>
        MysqlrEntidades.Usuarios RecuperarUsuarios(int id);
        /// <summary>
        /// Lista Usuarios
        /// </summary>
        /// <returns>IEnumerable</returns>
        IEnumerable<MysqlrEntidades.Usuarios> ListaUsuarios();
        /// <summary>
        /// Borrar Usuarios
        /// </summary>
        /// <param name = "id">Identificador</param>
        bool BorrarUsuarios(int id);
        /// <summary>
        /// Login
        /// </summary>
        /// <param name = "correoUsuarios">Correo (Usuarios) Igual a (=?)</param>
        /// <param name = "contrasenaUsuarios">Contrase&#241;a (Usuarios) Igual a (=?)</param>
        /// <param name = "pagina">Página</param>
        /// <param name = "tamanoDePagina">Tamaño de Página</param>
        /// <param name = "ordenarPor">Ordenar por</param>
        /// <param name = "tipo">Tipo</param>
        /// <returns>List</returns>
        List<DTO.Login> Login(string correoUsuarios, string contrasenaUsuarios, int pagina, int tamanoDePagina, string ordenarPor, string tipo);
        /// <summary>
        /// Método que recupera el total de elementos.
        /// </summary>
        /// <param name = "correoUsuarios">Correo (Usuarios) Igual a (=?)</param>
        /// <param name = "contrasenaUsuarios">Contrase&#241;a (Usuarios) Igual a (=?)</param>
        /// <returns>int</returns>
        int LoginTotal(string correoUsuarios, string contrasenaUsuarios);
#endregion
    }
}