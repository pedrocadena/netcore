using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
namespace Codium.Web.Models.MysqlrRepositorios
{
    /// <summary>
    /// CEDAS
    /// </summary>
    public class Cedas : ICedas
    {
#region Variables Privadas
        /// <summary>
        /// Contexto de Base de Datos
        /// </summary>
        private readonly Mysqlr _mysqlr;
#endregion
#region Métodos
        /// <summary>
        /// Agregar Cedas
        /// </summary>
        /// <param name = "cedas">Entidad a agregar</param>
        public void AgregarCedas(MysqlrEntidades.Cedas cedas)
        {
            this._mysqlr.Cedas.Add(cedas);
        }

        /// <summary>
        /// Recuperar Cedas
        /// </summary>
        /// <param name = "id">Identificador</param>
        /// <returns>MysqlrEntidades.Cedas</returns>
        public MysqlrEntidades.Cedas RecuperarCedas(long id)
        {
            return this._mysqlr.Cedas.FirstOrDefault(u => u.LlavePrimaria == id);
        }

        /// <summary>
        /// Lista Cedas
        /// </summary>
        /// <returns>IEnumerable</returns>
        public IEnumerable<MysqlrEntidades.Cedas> ListaCedas()
        {
            return this._mysqlr.Cedas;
        }

        /// <summary>
        /// Borrar Cedas
        /// </summary>
        /// <param name = "id">Identificador</param>
        public bool BorrarCedas(long id)
        {
            bool borrado = false;
            MysqlrEntidades.Cedas cedas = RecuperarCedas(id);
            if (cedas != null)
            {
                borrado = true;
                this._mysqlr.Cedas.Remove(cedas);
            }

            return borrado;
        }

        /// <summary>
        /// Actualizar Cedas
        /// </summary>
        /// <param name = "estatusCedas1">Estatus (Cedas) Igual a (=?)</param>
        /// <param name = "llavePrimariaCedas2">Llave Primaria (Cedas) Igual a (=?)</param>
        public void ActualizarCedas(SByte estatusCedas1, long? llavePrimariaCedas2)
        {
            using (var transaccion = new System.Transactions.TransactionScope())
            {
                var cedasLista1 =
                    from Cedas in _mysqlr.Cedas.AsQueryable()
                    where Cedas.LlavePrimaria == llavePrimariaCedas2 /* Igual a (=?) */
                    select Cedas;
                foreach (MysqlrEntidades.Cedas cedas1 in cedasLista1)
                {
                    cedas1.Estatus = estatusCedas1;
                    this._mysqlr.Cedas.Update(cedas1);
                }

                this._mysqlr.SaveChanges();
                transaccion.Complete();
            }
        }

        /// <summary>
        /// Detalle Cedas
        /// </summary>
        /// <param name = "llavePrimariaCedas">Llave Primaria (Cedas) Igual a (=?)</param>
        /// <param name = "pagina">Página</param>
        /// <param name = "tamanoDePagina">Tamaño de Página</param>
        /// <param name = "ordenarPor">Ordenar por</param>
        /// <param name = "tipo">Tipo</param>
        /// <returns>List</returns>
        public List<DTO.DetalleCedas> DetalleCedas(long llavePrimariaCedas, int pagina, int tamanoDePagina, string ordenarPor, string tipo)
        {
            if (pagina == 0)
            {
                pagina = 1;
            }

            if (tamanoDePagina == 0)
            {
                tamanoDePagina = Int32.MaxValue;
            }

            var consulta =
                from Cedas in _mysqlr.Cedas.AsQueryable()
                where Cedas.LlavePrimaria == llavePrimariaCedas /* Igual a (=?) */ && Cedas.LlavePrimaria != null /* No es nulo (IS NOT NULL) */
                select new DTO.DetalleCedas{LlavePrimariaCedas = Cedas.LlavePrimaria, ClaveCedasSaderCedas = Cedas.ClaveCedasSader, EstadoCedas = Cedas.Estado, IdCedaSegalmexCedas = Cedas.IdCedaSegalmex, DecimalesCedas = Cedas.Decimales, FechaDeCreacionCedas = Cedas.FechaDeCreacion, FechaDeModificacionCedas = Cedas.FechaDeModificacion, ActivoCedas = Cedas.Activo};
            if (tipo == null || tipo == "")
            {
                tipo = "ASC";
            }

            switch (ordenarPor + tipo)
            {
                case "llavePrimariaCedasASC":
                    consulta = consulta.OrderBy(u => u.LlavePrimariaCedas);
                    break;
                case "llavePrimariaCedasDESC":
                    consulta = consulta.OrderByDescending(u => u.LlavePrimariaCedas);
                    break;
                case "claveCedasSaderCedasASC":
                    consulta = consulta.OrderBy(u => u.ClaveCedasSaderCedas);
                    break;
                case "claveCedasSaderCedasDESC":
                    consulta = consulta.OrderByDescending(u => u.ClaveCedasSaderCedas);
                    break;
                case "estadoCedasASC":
                    consulta = consulta.OrderBy(u => u.EstadoCedas);
                    break;
                case "estadoCedasDESC":
                    consulta = consulta.OrderByDescending(u => u.EstadoCedas);
                    break;
                case "idCedaSegalmexCedasASC":
                    consulta = consulta.OrderBy(u => u.IdCedaSegalmexCedas);
                    break;
                case "idCedaSegalmexCedasDESC":
                    consulta = consulta.OrderByDescending(u => u.IdCedaSegalmexCedas);
                    break;
                case "decimalesCedasASC":
                    consulta = consulta.OrderBy(u => u.DecimalesCedas);
                    break;
                case "decimalesCedasDESC":
                    consulta = consulta.OrderByDescending(u => u.DecimalesCedas);
                    break;
                case "fechaDeCreacionCedasASC":
                    consulta = consulta.OrderBy(u => u.FechaDeCreacionCedas);
                    break;
                case "fechaDeCreacionCedasDESC":
                    consulta = consulta.OrderByDescending(u => u.FechaDeCreacionCedas);
                    break;
                case "fechaDeModificacionCedasASC":
                    consulta = consulta.OrderBy(u => u.FechaDeModificacionCedas);
                    break;
                case "fechaDeModificacionCedasDESC":
                    consulta = consulta.OrderByDescending(u => u.FechaDeModificacionCedas);
                    break;
                case "activoCedasASC":
                    consulta = consulta.OrderBy(u => u.ActivoCedas);
                    break;
                case "activoCedasDESC":
                    consulta = consulta.OrderByDescending(u => u.ActivoCedas);
                    break;
                default:
                    consulta = consulta.OrderBy(u => u.LlavePrimariaCedas);
                    break;
            }

            consulta = consulta.Skip(tamanoDePagina * (pagina - 1)).Take(tamanoDePagina);
            return consulta.ToList();
        }

        /// <summary>
        /// Método que recupera el total de elementos.
        /// </summary>
        /// <param name = "llavePrimariaCedas">Llave Primaria (Cedas) Igual a (=?)</param>
        /// <returns>int</returns>
        public int DetalleCedasTotal(long llavePrimariaCedas)
        {
            var consulta =
                from Cedas in _mysqlr.Cedas.AsQueryable()
                where Cedas.LlavePrimaria == llavePrimariaCedas /* Igual a (=?) */ && Cedas.LlavePrimaria != null /* No es nulo (IS NOT NULL) */
                select new
                {
                fl_unique = 1
                }

            ;
            return consulta.Count();
        }

        /// <summary>
        /// Lista Cedas
        /// </summary>
        /// <param name = "pagina">Página</param>
        /// <param name = "tamanoDePagina">Tamaño de Página</param>
        /// <param name = "ordenarPor">Ordenar por</param>
        /// <param name = "tipo">Tipo</param>
        /// <returns>List</returns>
        public List<DTO.ListaCedas> ListaCedas(int pagina, int tamanoDePagina, string ordenarPor, string tipo)
        {
            if (pagina == 0)
            {
                pagina = 1;
            }

            if (tamanoDePagina == 0)
            {
                tamanoDePagina = Int32.MaxValue;
            }

            var consulta =
                from Cedas in _mysqlr.Cedas.AsQueryable()
                where Cedas.Estatus == 1 /* Igual a (=?) */ && Cedas.LlavePrimaria != null /* No es nulo (IS NOT NULL) */
                select new DTO.ListaCedas{LlavePrimariaCedas = Cedas.LlavePrimaria, ClaveCedasSaderCedas = Cedas.ClaveCedasSader, EstadoCedas = Cedas.Estado, IdCedaSegalmexCedas = Cedas.IdCedaSegalmex, DecimalesCedas = Cedas.Decimales, FechaDeModificacionCedas = Cedas.FechaDeModificacion, FechaDeCreacionCedas = Cedas.FechaDeCreacion};
            if (tipo == null || tipo == "")
            {
                tipo = "ASC";
            }

            switch (ordenarPor + tipo)
            {
                case "llavePrimariaCedasASC":
                    consulta = consulta.OrderBy(u => u.LlavePrimariaCedas);
                    break;
                case "llavePrimariaCedasDESC":
                    consulta = consulta.OrderByDescending(u => u.LlavePrimariaCedas);
                    break;
                case "claveCedasSaderCedasASC":
                    consulta = consulta.OrderBy(u => u.ClaveCedasSaderCedas);
                    break;
                case "claveCedasSaderCedasDESC":
                    consulta = consulta.OrderByDescending(u => u.ClaveCedasSaderCedas);
                    break;
                case "estadoCedasASC":
                    consulta = consulta.OrderBy(u => u.EstadoCedas);
                    break;
                case "estadoCedasDESC":
                    consulta = consulta.OrderByDescending(u => u.EstadoCedas);
                    break;
                case "idCedaSegalmexCedasASC":
                    consulta = consulta.OrderBy(u => u.IdCedaSegalmexCedas);
                    break;
                case "idCedaSegalmexCedasDESC":
                    consulta = consulta.OrderByDescending(u => u.IdCedaSegalmexCedas);
                    break;
                case "decimalesCedasASC":
                    consulta = consulta.OrderBy(u => u.DecimalesCedas);
                    break;
                case "decimalesCedasDESC":
                    consulta = consulta.OrderByDescending(u => u.DecimalesCedas);
                    break;
                case "fechaDeModificacionCedasASC":
                    consulta = consulta.OrderBy(u => u.FechaDeModificacionCedas);
                    break;
                case "fechaDeModificacionCedasDESC":
                    consulta = consulta.OrderByDescending(u => u.FechaDeModificacionCedas);
                    break;
                case "fechaDeCreacionCedasASC":
                    consulta = consulta.OrderBy(u => u.FechaDeCreacionCedas);
                    break;
                case "fechaDeCreacionCedasDESC":
                    consulta = consulta.OrderByDescending(u => u.FechaDeCreacionCedas);
                    break;
                default:
                    consulta = consulta.OrderBy(u => u.LlavePrimariaCedas);
                    break;
            }

            consulta = consulta.Skip(tamanoDePagina * (pagina - 1)).Take(tamanoDePagina);
            return consulta.ToList();
        }

        /// <summary>
        /// Método que recupera el total de elementos.
        /// </summary>
        /// <returns>int</returns>
        public int ListaCedasTotal()
        {
            var consulta =
                from Cedas in _mysqlr.Cedas.AsQueryable()
                where Cedas.Estatus == 1 /* Igual a (=?) */ && Cedas.LlavePrimaria != null /* No es nulo (IS NOT NULL) */
                select new
                {
                fl_unique = 1
                }

            ;
            return consulta.Count();
        }

        /// <summary>
        /// Tabla de Datos Cedas
        /// </summary>
        /// <param name = "claveCedasSaderCedas">CLAVE_CEDAS_SADER (Cedas) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "estadoCedas">ESTADO (Cedas) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "idCedaSegalmexCedas">ID CEDA SEGALMEX (Cedas) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "pagina">Página</param>
        /// <param name = "tamanoDePagina">Tamaño de Página</param>
        /// <param name = "ordenarPor">Ordenar por</param>
        /// <param name = "tipo">Tipo</param>
        /// <returns>List</returns>
        public List<DTO.TablaDeDatosCedas> TablaDeDatosCedas(string claveCedasSaderCedas, string estadoCedas, string idCedaSegalmexCedas, int pagina, int tamanoDePagina, string ordenarPor, string tipo)
        {
            if (pagina == 0)
            {
                pagina = 1;
            }

            if (tamanoDePagina == 0)
            {
                tamanoDePagina = Int32.MaxValue;
            }

            var consulta =
                from Cedas in _mysqlr.Cedas.AsQueryable()join Estatus in _mysqlr.Estatus on Cedas.Estatus equals Estatus.LlavePrimaria // Estatus Cedas
                where (Cedas.ClaveCedasSader.Contains(claveCedasSaderCedas) /* Contiene (LIKE '%?%') */ || Cedas.Estado.Contains(estadoCedas) /* Contiene (LIKE '%?%') */ || Cedas.IdCedaSegalmex.Contains(idCedaSegalmexCedas) /* Contiene (LIKE '%?%') */) && Cedas.Estatus != 3 /* Diferente a (<>?) */ && Cedas.Estatus != null /* No es nulo (IS NOT NULL) */ && Cedas.LlavePrimaria != null /* No es nulo (IS NOT NULL) */
                select new DTO.TablaDeDatosCedas{LlavePrimariaCedas = Cedas.LlavePrimaria, FechaDeCreacionCedas = Cedas.FechaDeCreacion, EstatusCedas = Cedas.Estatus, FechaDeModificacionCedas = Cedas.FechaDeModificacion, ClaveCedasSaderCedas = Cedas.ClaveCedasSader, EstadoCedas = Cedas.Estado, IdCedaSegalmexCedas = Cedas.IdCedaSegalmex, DecimalesCedas = Cedas.Decimales, LogotipoCedas = Cedas.Logotipo, Columna4MimeTypeCedas = Cedas.Columna4MimeType, Columna4FileNameCedas = Cedas.Columna4FileName, NombreEstatus = Estatus.Nombre};
            if (tipo == null || tipo == "")
            {
                tipo = "ASC";
            }

            switch (ordenarPor + tipo)
            {
                case "llavePrimariaCedasASC":
                    consulta = consulta.OrderBy(u => u.LlavePrimariaCedas);
                    break;
                case "llavePrimariaCedasDESC":
                    consulta = consulta.OrderByDescending(u => u.LlavePrimariaCedas);
                    break;
                case "fechaDeCreacionCedasASC":
                    consulta = consulta.OrderBy(u => u.FechaDeCreacionCedas);
                    break;
                case "fechaDeCreacionCedasDESC":
                    consulta = consulta.OrderByDescending(u => u.FechaDeCreacionCedas);
                    break;
                case "estatusCedasASC":
                    consulta = consulta.OrderBy(u => u.EstatusCedas);
                    break;
                case "estatusCedasDESC":
                    consulta = consulta.OrderByDescending(u => u.EstatusCedas);
                    break;
                case "fechaDeModificacionCedasASC":
                    consulta = consulta.OrderBy(u => u.FechaDeModificacionCedas);
                    break;
                case "fechaDeModificacionCedasDESC":
                    consulta = consulta.OrderByDescending(u => u.FechaDeModificacionCedas);
                    break;
                case "claveCedasSaderCedasASC":
                    consulta = consulta.OrderBy(u => u.ClaveCedasSaderCedas);
                    break;
                case "claveCedasSaderCedasDESC":
                    consulta = consulta.OrderByDescending(u => u.ClaveCedasSaderCedas);
                    break;
                case "estadoCedasASC":
                    consulta = consulta.OrderBy(u => u.EstadoCedas);
                    break;
                case "estadoCedasDESC":
                    consulta = consulta.OrderByDescending(u => u.EstadoCedas);
                    break;
                case "idCedaSegalmexCedasASC":
                    consulta = consulta.OrderBy(u => u.IdCedaSegalmexCedas);
                    break;
                case "idCedaSegalmexCedasDESC":
                    consulta = consulta.OrderByDescending(u => u.IdCedaSegalmexCedas);
                    break;
                case "decimalesCedasASC":
                    consulta = consulta.OrderBy(u => u.DecimalesCedas);
                    break;
                case "decimalesCedasDESC":
                    consulta = consulta.OrderByDescending(u => u.DecimalesCedas);
                    break;
                case "logotipoCedasASC":
                    consulta = consulta.OrderBy(u => u.LogotipoCedas);
                    break;
                case "logotipoCedasDESC":
                    consulta = consulta.OrderByDescending(u => u.LogotipoCedas);
                    break;
                case "columna4MimeTypeCedasASC":
                    consulta = consulta.OrderBy(u => u.Columna4MimeTypeCedas);
                    break;
                case "columna4MimeTypeCedasDESC":
                    consulta = consulta.OrderByDescending(u => u.Columna4MimeTypeCedas);
                    break;
                case "columna4FileNameCedasASC":
                    consulta = consulta.OrderBy(u => u.Columna4FileNameCedas);
                    break;
                case "columna4FileNameCedasDESC":
                    consulta = consulta.OrderByDescending(u => u.Columna4FileNameCedas);
                    break;
                case "llavePrimariaEncriptadoCedasASC":
                    consulta = consulta.OrderBy(u => u.LlavePrimariaCedas);
                    break;
                case "llavePrimariaEncriptadoCedasDESC":
                    consulta = consulta.OrderByDescending(u => u.LlavePrimariaCedas);
                    break;
                case "fechaDeCreacionFormateadoCedasASC":
                    consulta = consulta.OrderBy(u => u.FechaDeCreacionCedas);
                    break;
                case "fechaDeCreacionFormateadoCedasDESC":
                    consulta = consulta.OrderByDescending(u => u.FechaDeCreacionCedas);
                    break;
                case "fechaDeModificacionFormateadoCedasASC":
                    consulta = consulta.OrderBy(u => u.FechaDeCreacionCedas);
                    break;
                case "fechaDeModificacionFormateadoCedasDESC":
                    consulta = consulta.OrderByDescending(u => u.FechaDeCreacionCedas);
                    break;
                case "descargarLogotipoCedasASC":
                    consulta = consulta.OrderBy(u => u.Columna4FileNameCedas);
                    break;
                case "descargarLogotipoCedasDESC":
                    consulta = consulta.OrderByDescending(u => u.Columna4FileNameCedas);
                    break;
                case "condicionalLogotipoCedasASC":
                    consulta = consulta.OrderBy(u => u.Columna4FileNameCedas);
                    break;
                case "condicionalLogotipoCedasDESC":
                    consulta = consulta.OrderByDescending(u => u.Columna4FileNameCedas);
                    break;
                case "nombreEstatusASC":
                    consulta = consulta.OrderBy(u => u.NombreEstatus);
                    break;
                case "nombreEstatusDESC":
                    consulta = consulta.OrderByDescending(u => u.NombreEstatus);
                    break;
                default:
                    consulta = consulta.OrderBy(u => u.LlavePrimariaCedas);
                    break;
            }

            consulta = consulta.Skip(tamanoDePagina * (pagina - 1)).Take(tamanoDePagina);
            return consulta.ToList();
        }

        /// <summary>
        /// Método que recupera el total de elementos.
        /// </summary>
        /// <param name = "claveCedasSaderCedas">CLAVE_CEDAS_SADER (Cedas) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "estadoCedas">ESTADO (Cedas) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "idCedaSegalmexCedas">ID CEDA SEGALMEX (Cedas) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <returns>int</returns>
        public int TablaDeDatosCedasTotal(string claveCedasSaderCedas, string estadoCedas, string idCedaSegalmexCedas)
        {
            var consulta =
                from Cedas in _mysqlr.Cedas.AsQueryable()join Estatus in _mysqlr.Estatus on Cedas.Estatus equals Estatus.LlavePrimaria // Estatus Cedas
                where (Cedas.ClaveCedasSader.Contains(claveCedasSaderCedas) /* Contiene (LIKE '%?%') */ || Cedas.Estado.Contains(estadoCedas) /* Contiene (LIKE '%?%') */ || Cedas.IdCedaSegalmex.Contains(idCedaSegalmexCedas) /* Contiene (LIKE '%?%') */) && Cedas.Estatus != 3 /* Diferente a (<>?) */ && Cedas.Estatus != null /* No es nulo (IS NOT NULL) */ && Cedas.LlavePrimaria != null /* No es nulo (IS NOT NULL) */
                select new
                {
                fl_unique = 1
                }

            ;
            return consulta.Count();
        }

#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        public Cedas(Mysqlr mysqlr)
        {
            this._mysqlr = mysqlr;
        }
#endregion
    }
}