using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
namespace Codium.Web.Models.MysqlrRepositorios
{
    /// <summary>
    /// Hoja 1
    /// </summary>
    public class CedasOriginal : ICedasOriginal
    {
#region Variables Privadas
        /// <summary>
        /// Contexto de Base de Datos
        /// </summary>
        private readonly Mysqlr _mysqlr;
#endregion
#region Métodos
        /// <summary>
        /// Agregar CedasOriginal
        /// </summary>
        /// <param name = "cedasOriginal">Entidad a agregar</param>
        public void AgregarCedasOriginal(MysqlrEntidades.CedasOriginal cedasOriginal)
        {
            this._mysqlr.CedasOriginales.Add(cedasOriginal);
        }

        /// <summary>
        /// Recuperar CedasOriginal
        /// </summary>
        /// <param name = "id">Identificador</param>
        /// <returns>MysqlrEntidades.CedasOriginal</returns>
        public MysqlrEntidades.CedasOriginal RecuperarCedasOriginal(long id)
        {
            return this._mysqlr.CedasOriginales.FirstOrDefault(u => u.LlavePrimaria == id);
        }

        /// <summary>
        /// Lista CedasOriginales
        /// </summary>
        /// <returns>IEnumerable</returns>
        public IEnumerable<MysqlrEntidades.CedasOriginal> ListaCedasOriginales()
        {
            return this._mysqlr.CedasOriginales;
        }

        /// <summary>
        /// Borrar CedasOriginal
        /// </summary>
        /// <param name = "id">Identificador</param>
        public bool BorrarCedasOriginal(long id)
        {
            bool borrado = false;
            MysqlrEntidades.CedasOriginal cedasOriginal = RecuperarCedasOriginal(id);
            if (cedasOriginal != null)
            {
                borrado = true;
                this._mysqlr.CedasOriginales.Remove(cedasOriginal);
            }

            return borrado;
        }

        /// <summary>
        /// Actualizar Cedas Original
        /// </summary>
        /// <param name = "estatusCedasOriginal1">Estatus (Cedas Original) Igual a (=?)</param>
        /// <param name = "llavePrimariaCedasOriginal2">Llave Primaria (Cedas Original) Igual a (=?)</param>
        public void ActualizarCedasOriginal(SByte estatusCedasOriginal1, long? llavePrimariaCedasOriginal2)
        {
            using (var transaccion = new System.Transactions.TransactionScope())
            {
                var cedasOriginalLista1 =
                    from CedasOriginal in _mysqlr.CedasOriginales.AsQueryable()
                    where CedasOriginal.LlavePrimaria == llavePrimariaCedasOriginal2 /* Igual a (=?) */
                    select CedasOriginal;
                foreach (MysqlrEntidades.CedasOriginal cedasOriginal1 in cedasOriginalLista1)
                {
                    cedasOriginal1.Estatus = estatusCedasOriginal1;
                    this._mysqlr.CedasOriginales.Update(cedasOriginal1);
                }

                this._mysqlr.SaveChanges();
                transaccion.Complete();
            }
        }

        /// <summary>
        /// Detalle Cedas Original
        /// </summary>
        /// <param name = "llavePrimariaCedasOriginal">Llave Primaria (Cedas Original) Igual a (=?)</param>
        /// <param name = "pagina">Página</param>
        /// <param name = "tamanoDePagina">Tamaño de Página</param>
        /// <param name = "ordenarPor">Ordenar por</param>
        /// <param name = "tipo">Tipo</param>
        /// <returns>List</returns>
        public List<DTO.DetalleCedasOriginal> DetalleCedasOriginal(long? llavePrimariaCedasOriginal, int pagina, int tamanoDePagina, string ordenarPor, string tipo)
        {
            if (pagina == 0)
            {
                pagina = 1;
            }

            if (tamanoDePagina == 0)
            {
                tamanoDePagina = Int32.MaxValue;
            }

            var consulta =
                from CedasOriginal in _mysqlr.CedasOriginales.AsQueryable()
                where (llavePrimariaCedasOriginal == null || CedasOriginal.LlavePrimaria == llavePrimariaCedasOriginal /* Igual a (=?) */)select new DTO.DetalleCedasOriginal{LlavePrimariaCedasOriginal = CedasOriginal.LlavePrimaria, DapTonCedasOriginal = CedasOriginal.DapTon, UreaTonCedasOriginal = CedasOriginal.UreaTon, ClaveCedasSaderCedasOriginal = CedasOriginal.ClaveCedasSader, EstadoCedasOriginal = CedasOriginal.Estado, IdCedaSegalmexCedasOriginal = CedasOriginal.IdCedaSegalmex, RegionSegalmexCedasOriginal = CedasOriginal.RegionSegalmex};
            if (tipo == null || tipo == "")
            {
                tipo = "ASC";
            }

            switch (ordenarPor + tipo)
            {
                case "llavePrimariaCedasOriginalASC":
                    consulta = consulta.OrderBy(u => u.LlavePrimariaCedasOriginal);
                    break;
                case "llavePrimariaCedasOriginalDESC":
                    consulta = consulta.OrderByDescending(u => u.LlavePrimariaCedasOriginal);
                    break;
                case "dapTonCedasOriginalASC":
                    consulta = consulta.OrderBy(u => u.DapTonCedasOriginal);
                    break;
                case "dapTonCedasOriginalDESC":
                    consulta = consulta.OrderByDescending(u => u.DapTonCedasOriginal);
                    break;
                case "ureaTonCedasOriginalASC":
                    consulta = consulta.OrderBy(u => u.UreaTonCedasOriginal);
                    break;
                case "ureaTonCedasOriginalDESC":
                    consulta = consulta.OrderByDescending(u => u.UreaTonCedasOriginal);
                    break;
                case "claveCedasSaderCedasOriginalASC":
                    consulta = consulta.OrderBy(u => u.ClaveCedasSaderCedasOriginal);
                    break;
                case "claveCedasSaderCedasOriginalDESC":
                    consulta = consulta.OrderByDescending(u => u.ClaveCedasSaderCedasOriginal);
                    break;
                case "estadoCedasOriginalASC":
                    consulta = consulta.OrderBy(u => u.EstadoCedasOriginal);
                    break;
                case "estadoCedasOriginalDESC":
                    consulta = consulta.OrderByDescending(u => u.EstadoCedasOriginal);
                    break;
                case "idCedaSegalmexCedasOriginalASC":
                    consulta = consulta.OrderBy(u => u.IdCedaSegalmexCedasOriginal);
                    break;
                case "idCedaSegalmexCedasOriginalDESC":
                    consulta = consulta.OrderByDescending(u => u.IdCedaSegalmexCedasOriginal);
                    break;
                case "regionSegalmexCedasOriginalASC":
                    consulta = consulta.OrderBy(u => u.RegionSegalmexCedasOriginal);
                    break;
                case "regionSegalmexCedasOriginalDESC":
                    consulta = consulta.OrderByDescending(u => u.RegionSegalmexCedasOriginal);
                    break;
                default:
                    consulta = consulta.OrderBy(u => u.LlavePrimariaCedasOriginal);
                    break;
            }

            consulta = consulta.Skip(tamanoDePagina * (pagina - 1)).Take(tamanoDePagina);
            return consulta.ToList();
        }

        /// <summary>
        /// Método que recupera el total de elementos.
        /// </summary>
        /// <param name = "llavePrimariaCedasOriginal">Llave Primaria (Cedas Original) Igual a (=?)</param>
        /// <returns>int</returns>
        public int DetalleCedasOriginalTotal(long? llavePrimariaCedasOriginal)
        {
            var consulta =
                from CedasOriginal in _mysqlr.CedasOriginales.AsQueryable()
                where (llavePrimariaCedasOriginal == null || CedasOriginal.LlavePrimaria == llavePrimariaCedasOriginal /* Igual a (=?) */)select new
                {
                fl_unique = 1
                }

            ;
            return consulta.Count();
        }

        /// <summary>
        /// Lista Cedas Original
        /// </summary>
        /// <param name = "pagina">Página</param>
        /// <param name = "tamanoDePagina">Tamaño de Página</param>
        /// <param name = "ordenarPor">Ordenar por</param>
        /// <param name = "tipo">Tipo</param>
        /// <returns>List</returns>
        public List<DTO.ListaCedasOriginal> ListaCedasOriginal(int pagina, int tamanoDePagina, string ordenarPor, string tipo)
        {
            if (pagina == 0)
            {
                pagina = 1;
            }

            if (tamanoDePagina == 0)
            {
                tamanoDePagina = Int32.MaxValue;
            }

            var consulta =
                from CedasOriginal in _mysqlr.CedasOriginales.AsQueryable()
                where CedasOriginal.Estatus == 1 /* Igual a (=?) */
                select new DTO.ListaCedasOriginal{LlavePrimariaCedasOriginal = CedasOriginal.LlavePrimaria, DapTonCedasOriginal = CedasOriginal.DapTon, UreaTonCedasOriginal = CedasOriginal.UreaTon, ClaveCedasSaderCedasOriginal = CedasOriginal.ClaveCedasSader, EstadoCedasOriginal = CedasOriginal.Estado, IdCedaSegalmexCedasOriginal = CedasOriginal.IdCedaSegalmex, RegionSegalmexCedasOriginal = CedasOriginal.RegionSegalmex};
            if (tipo == null || tipo == "")
            {
                tipo = "ASC";
            }

            switch (ordenarPor + tipo)
            {
                case "llavePrimariaCedasOriginalASC":
                    consulta = consulta.OrderBy(u => u.LlavePrimariaCedasOriginal);
                    break;
                case "llavePrimariaCedasOriginalDESC":
                    consulta = consulta.OrderByDescending(u => u.LlavePrimariaCedasOriginal);
                    break;
                case "dapTonCedasOriginalASC":
                    consulta = consulta.OrderBy(u => u.DapTonCedasOriginal);
                    break;
                case "dapTonCedasOriginalDESC":
                    consulta = consulta.OrderByDescending(u => u.DapTonCedasOriginal);
                    break;
                case "ureaTonCedasOriginalASC":
                    consulta = consulta.OrderBy(u => u.UreaTonCedasOriginal);
                    break;
                case "ureaTonCedasOriginalDESC":
                    consulta = consulta.OrderByDescending(u => u.UreaTonCedasOriginal);
                    break;
                case "claveCedasSaderCedasOriginalASC":
                    consulta = consulta.OrderBy(u => u.ClaveCedasSaderCedasOriginal);
                    break;
                case "claveCedasSaderCedasOriginalDESC":
                    consulta = consulta.OrderByDescending(u => u.ClaveCedasSaderCedasOriginal);
                    break;
                case "estadoCedasOriginalASC":
                    consulta = consulta.OrderBy(u => u.EstadoCedasOriginal);
                    break;
                case "estadoCedasOriginalDESC":
                    consulta = consulta.OrderByDescending(u => u.EstadoCedasOriginal);
                    break;
                case "idCedaSegalmexCedasOriginalASC":
                    consulta = consulta.OrderBy(u => u.IdCedaSegalmexCedasOriginal);
                    break;
                case "idCedaSegalmexCedasOriginalDESC":
                    consulta = consulta.OrderByDescending(u => u.IdCedaSegalmexCedasOriginal);
                    break;
                case "regionSegalmexCedasOriginalASC":
                    consulta = consulta.OrderBy(u => u.RegionSegalmexCedasOriginal);
                    break;
                case "regionSegalmexCedasOriginalDESC":
                    consulta = consulta.OrderByDescending(u => u.RegionSegalmexCedasOriginal);
                    break;
                default:
                    consulta = consulta.OrderBy(u => u.LlavePrimariaCedasOriginal);
                    break;
            }

            consulta = consulta.Skip(tamanoDePagina * (pagina - 1)).Take(tamanoDePagina);
            return consulta.ToList();
        }

        /// <summary>
        /// Método que recupera el total de elementos.
        /// </summary>
        /// <returns>int</returns>
        public int ListaCedasOriginalTotal()
        {
            var consulta =
                from CedasOriginal in _mysqlr.CedasOriginales.AsQueryable()
                where CedasOriginal.Estatus == 1 /* Igual a (=?) */
                select new
                {
                fl_unique = 1
                }

            ;
            return consulta.Count();
        }

        /// <summary>
        /// Tabla de Datos Cedas Original
        /// </summary>
        /// <param name = "claveCedasSaderCedasOriginal">CLAVE_CEDAS_SADER (Cedas Original) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "estadoCedasOriginal">ESTADO (Cedas Original) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "idCedaSegalmexCedasOriginal">ID CEDA SEGALMEX (Cedas Original) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "regionSegalmexCedasOriginal">REGION SEGALMEX (Cedas Original) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "pagina">Página</param>
        /// <param name = "tamanoDePagina">Tamaño de Página</param>
        /// <param name = "ordenarPor">Ordenar por</param>
        /// <param name = "tipo">Tipo</param>
        /// <returns>List</returns>
        public List<DTO.TablaDeDatosCedasOriginal> TablaDeDatosCedasOriginal(string claveCedasSaderCedasOriginal, string estadoCedasOriginal, string idCedaSegalmexCedasOriginal, string regionSegalmexCedasOriginal, int pagina, int tamanoDePagina, string ordenarPor, string tipo)
        {
            if (pagina == 0)
            {
                pagina = 1;
            }

            if (tamanoDePagina == 0)
            {
                tamanoDePagina = Int32.MaxValue;
            }

            var consulta =
                from CedasOriginal in _mysqlr.CedasOriginales.AsQueryable()join Estatus in _mysqlr.Estatus on CedasOriginal.Estatus equals Estatus.LlavePrimaria // Estatus Cedas Original
                where (CedasOriginal.ClaveCedasSader.Contains(claveCedasSaderCedasOriginal) /* Contiene (LIKE '%?%') */ || CedasOriginal.Estado.Contains(estadoCedasOriginal) /* Contiene (LIKE '%?%') */ || CedasOriginal.IdCedaSegalmex.Contains(idCedaSegalmexCedasOriginal) /* Contiene (LIKE '%?%') */ || CedasOriginal.RegionSegalmex.Contains(regionSegalmexCedasOriginal) /* Contiene (LIKE '%?%') */) && CedasOriginal.Estatus != 3 /* Diferente a (<>?) */
                select new DTO.TablaDeDatosCedasOriginal{LlavePrimariaCedasOriginal = CedasOriginal.LlavePrimaria, FechaDeCreacionCedasOriginal = CedasOriginal.FechaDeCreacion, EstatusCedasOriginal = CedasOriginal.Estatus, FechaDeModificacionCedasOriginal = CedasOriginal.FechaDeModificacion, DapTonCedasOriginal = CedasOriginal.DapTon, UreaTonCedasOriginal = CedasOriginal.UreaTon, ClaveCedasSaderCedasOriginal = CedasOriginal.ClaveCedasSader, EstadoCedasOriginal = CedasOriginal.Estado, IdCedaSegalmexCedasOriginal = CedasOriginal.IdCedaSegalmex, RegionSegalmexCedasOriginal = CedasOriginal.RegionSegalmex, NombreEstatus = Estatus.Nombre};
            if (tipo == null || tipo == "")
            {
                tipo = "ASC";
            }

            switch (ordenarPor + tipo)
            {
                case "llavePrimariaCedasOriginalASC":
                    consulta = consulta.OrderBy(u => u.LlavePrimariaCedasOriginal);
                    break;
                case "llavePrimariaCedasOriginalDESC":
                    consulta = consulta.OrderByDescending(u => u.LlavePrimariaCedasOriginal);
                    break;
                case "fechaDeCreacionCedasOriginalASC":
                    consulta = consulta.OrderBy(u => u.FechaDeCreacionCedasOriginal);
                    break;
                case "fechaDeCreacionCedasOriginalDESC":
                    consulta = consulta.OrderByDescending(u => u.FechaDeCreacionCedasOriginal);
                    break;
                case "estatusCedasOriginalASC":
                    consulta = consulta.OrderBy(u => u.EstatusCedasOriginal);
                    break;
                case "estatusCedasOriginalDESC":
                    consulta = consulta.OrderByDescending(u => u.EstatusCedasOriginal);
                    break;
                case "fechaDeModificacionCedasOriginalASC":
                    consulta = consulta.OrderBy(u => u.FechaDeModificacionCedasOriginal);
                    break;
                case "fechaDeModificacionCedasOriginalDESC":
                    consulta = consulta.OrderByDescending(u => u.FechaDeModificacionCedasOriginal);
                    break;
                case "dapTonCedasOriginalASC":
                    consulta = consulta.OrderBy(u => u.DapTonCedasOriginal);
                    break;
                case "dapTonCedasOriginalDESC":
                    consulta = consulta.OrderByDescending(u => u.DapTonCedasOriginal);
                    break;
                case "ureaTonCedasOriginalASC":
                    consulta = consulta.OrderBy(u => u.UreaTonCedasOriginal);
                    break;
                case "ureaTonCedasOriginalDESC":
                    consulta = consulta.OrderByDescending(u => u.UreaTonCedasOriginal);
                    break;
                case "claveCedasSaderCedasOriginalASC":
                    consulta = consulta.OrderBy(u => u.ClaveCedasSaderCedasOriginal);
                    break;
                case "claveCedasSaderCedasOriginalDESC":
                    consulta = consulta.OrderByDescending(u => u.ClaveCedasSaderCedasOriginal);
                    break;
                case "estadoCedasOriginalASC":
                    consulta = consulta.OrderBy(u => u.EstadoCedasOriginal);
                    break;
                case "estadoCedasOriginalDESC":
                    consulta = consulta.OrderByDescending(u => u.EstadoCedasOriginal);
                    break;
                case "idCedaSegalmexCedasOriginalASC":
                    consulta = consulta.OrderBy(u => u.IdCedaSegalmexCedasOriginal);
                    break;
                case "idCedaSegalmexCedasOriginalDESC":
                    consulta = consulta.OrderByDescending(u => u.IdCedaSegalmexCedasOriginal);
                    break;
                case "regionSegalmexCedasOriginalASC":
                    consulta = consulta.OrderBy(u => u.RegionSegalmexCedasOriginal);
                    break;
                case "regionSegalmexCedasOriginalDESC":
                    consulta = consulta.OrderByDescending(u => u.RegionSegalmexCedasOriginal);
                    break;
                case "llavePrimariaEncriptadoCedasOriginalASC":
                    consulta = consulta.OrderBy(u => u.LlavePrimariaCedasOriginal);
                    break;
                case "llavePrimariaEncriptadoCedasOriginalDESC":
                    consulta = consulta.OrderByDescending(u => u.LlavePrimariaCedasOriginal);
                    break;
                case "fechaDeCreacionFormateadoCedasOriginalASC":
                    consulta = consulta.OrderBy(u => u.FechaDeCreacionCedasOriginal);
                    break;
                case "fechaDeCreacionFormateadoCedasOriginalDESC":
                    consulta = consulta.OrderByDescending(u => u.FechaDeCreacionCedasOriginal);
                    break;
                case "fechaDeModificacionFormateadoCedasOriginalASC":
                    consulta = consulta.OrderBy(u => u.FechaDeCreacionCedasOriginal);
                    break;
                case "fechaDeModificacionFormateadoCedasOriginalDESC":
                    consulta = consulta.OrderByDescending(u => u.FechaDeCreacionCedasOriginal);
                    break;
                case "nombreEstatusASC":
                    consulta = consulta.OrderBy(u => u.NombreEstatus);
                    break;
                case "nombreEstatusDESC":
                    consulta = consulta.OrderByDescending(u => u.NombreEstatus);
                    break;
                default:
                    consulta = consulta.OrderBy(u => u.LlavePrimariaCedasOriginal);
                    break;
            }

            consulta = consulta.Skip(tamanoDePagina * (pagina - 1)).Take(tamanoDePagina);
            return consulta.ToList();
        }

        /// <summary>
        /// Método que recupera el total de elementos.
        /// </summary>
        /// <param name = "claveCedasSaderCedasOriginal">CLAVE_CEDAS_SADER (Cedas Original) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "estadoCedasOriginal">ESTADO (Cedas Original) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "idCedaSegalmexCedasOriginal">ID CEDA SEGALMEX (Cedas Original) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "regionSegalmexCedasOriginal">REGION SEGALMEX (Cedas Original) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <returns>int</returns>
        public int TablaDeDatosCedasOriginalTotal(string claveCedasSaderCedasOriginal, string estadoCedasOriginal, string idCedaSegalmexCedasOriginal, string regionSegalmexCedasOriginal)
        {
            var consulta =
                from CedasOriginal in _mysqlr.CedasOriginales.AsQueryable()join Estatus in _mysqlr.Estatus on CedasOriginal.Estatus equals Estatus.LlavePrimaria // Estatus Cedas Original
                where (CedasOriginal.ClaveCedasSader.Contains(claveCedasSaderCedasOriginal) /* Contiene (LIKE '%?%') */ || CedasOriginal.Estado.Contains(estadoCedasOriginal) /* Contiene (LIKE '%?%') */ || CedasOriginal.IdCedaSegalmex.Contains(idCedaSegalmexCedasOriginal) /* Contiene (LIKE '%?%') */ || CedasOriginal.RegionSegalmex.Contains(regionSegalmexCedasOriginal) /* Contiene (LIKE '%?%') */) && CedasOriginal.Estatus != 3 /* Diferente a (<>?) */
                select new
                {
                fl_unique = 1
                }

            ;
            return consulta.Count();
        }

#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        public CedasOriginal(Mysqlr mysqlr)
        {
            this._mysqlr = mysqlr;
        }
#endregion
    }
}