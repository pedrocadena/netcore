using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Codium.Web.Models.MysqlrRepositorios
{
    /// <summary>
    /// Procedimiento Almacenado
    /// </summary>
    public interface IProcedimientoAlmacenado
    {
#region Métodos
        /// <summary>
        /// TestDeProcedimiento
        /// </summary>
        /// <param name = "pagina">Página</param>
        /// <param name = "tamanoDePagina">Tamaño de Página</param>
        /// <param name = "ordenarPor">Ordenar por</param>
        /// <param name = "tipo">Tipo</param>
        /// <returns>List</returns>
        List<DTO.Testdeprocedimiento> Testdeprocedimiento(int pagina, int tamanoDePagina, string ordenarPor, string tipo);
        /// <summary>
        /// Método que recupera el total de elementos.
        /// </summary>
        /// <returns>int</returns>
        int TestdeprocedimientoTotal();
#endregion
    }
}