using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
namespace Codium.Web.Models.MysqlrRepositorios
{
    /// <summary>
    /// PRODUCTORES
    /// </summary>
    public class Productores : IProductores
    {
#region Variables Privadas
        /// <summary>
        /// Contexto de Base de Datos
        /// </summary>
        private readonly Mysqlr _mysqlr;
#endregion
#region Métodos
        /// <summary>
        /// Agregar Productores
        /// </summary>
        /// <param name = "productores">Entidad a agregar</param>
        public void AgregarProductores(MysqlrEntidades.Productores productores)
        {
            this._mysqlr.Productores.Add(productores);
        }

        /// <summary>
        /// Recuperar Productores
        /// </summary>
        /// <param name = "id">Identificador</param>
        /// <returns>MysqlrEntidades.Productores</returns>
        public MysqlrEntidades.Productores RecuperarProductores(long id)
        {
            return this._mysqlr.Productores.FirstOrDefault(u => u.LlavePrimaria == id);
        }

        /// <summary>
        /// Lista Productores
        /// </summary>
        /// <returns>IEnumerable</returns>
        public IEnumerable<MysqlrEntidades.Productores> ListaProductores()
        {
            return this._mysqlr.Productores;
        }

        /// <summary>
        /// Borrar Productores
        /// </summary>
        /// <param name = "id">Identificador</param>
        public bool BorrarProductores(long id)
        {
            bool borrado = false;
            MysqlrEntidades.Productores productores = RecuperarProductores(id);
            if (productores != null)
            {
                borrado = true;
                this._mysqlr.Productores.Remove(productores);
            }

            return borrado;
        }

        /// <summary>
        /// Actualizar Productores
        /// </summary>
        /// <param name = "estatusProductores1">Estatus (Productores) Igual a (=?)</param>
        /// <param name = "llavePrimariaProductores2">Llave Primaria (Productores) Igual a (=?)</param>
        public void ActualizarProductores(SByte estatusProductores1, long? llavePrimariaProductores2)
        {
            using (var transaccion = new System.Transactions.TransactionScope())
            {
                var productoresLista1 =
                    from Productores in _mysqlr.Productores.AsQueryable()
                    where Productores.LlavePrimaria == llavePrimariaProductores2 /* Igual a (=?) */
                    select Productores;
                foreach (MysqlrEntidades.Productores productores1 in productoresLista1)
                {
                    productores1.Estatus = estatusProductores1;
                    this._mysqlr.Productores.Update(productores1);
                }

                this._mysqlr.SaveChanges();
                transaccion.Complete();
            }
        }

        /// <summary>
        /// Detalle Productores
        /// </summary>
        /// <param name = "llavePrimariaProductores">Llave Primaria (Productores) Igual a (=?)</param>
        /// <param name = "pagina">Página</param>
        /// <param name = "tamanoDePagina">Tamaño de Página</param>
        /// <param name = "ordenarPor">Ordenar por</param>
        /// <param name = "tipo">Tipo</param>
        /// <returns>List</returns>
        public List<DTO.DetalleProductores> DetalleProductores(long? llavePrimariaProductores, int pagina, int tamanoDePagina, string ordenarPor, string tipo)
        {
            if (pagina == 0)
            {
                pagina = 1;
            }

            if (tamanoDePagina == 0)
            {
                tamanoDePagina = Int32.MaxValue;
            }

            var consulta =
                from Productores in _mysqlr.Productores.AsQueryable()
                where (llavePrimariaProductores == null || Productores.LlavePrimaria == llavePrimariaProductores /* Igual a (=?) */)select new DTO.DetalleProductores{LlavePrimariaProductores = Productores.LlavePrimaria, ApellidoMaternoProductores = Productores.ApellidoMaterno, ApellidoPaternoProductores = Productores.ApellidoPaterno, CurpProductorCuadernilloSaderProductores = Productores.CurpProductorCuadernilloSader, FolioCuadernilloSaderProductores = Productores.FolioCuadernilloSader, NoDeIdentificacionProductores = Productores.NoDeIdentificacion, NombresProductores = Productores.Nombres, FechaDeRegistroProductores = Productores.FechaDeRegistro, IdCedasProductores = Productores.IdCedas};
            if (tipo == null || tipo == "")
            {
                tipo = "ASC";
            }

            switch (ordenarPor + tipo)
            {
                case "llavePrimariaProductoresASC":
                    consulta = consulta.OrderBy(u => u.LlavePrimariaProductores);
                    break;
                case "llavePrimariaProductoresDESC":
                    consulta = consulta.OrderByDescending(u => u.LlavePrimariaProductores);
                    break;
                case "apellidoMaternoProductoresASC":
                    consulta = consulta.OrderBy(u => u.ApellidoMaternoProductores);
                    break;
                case "apellidoMaternoProductoresDESC":
                    consulta = consulta.OrderByDescending(u => u.ApellidoMaternoProductores);
                    break;
                case "apellidoPaternoProductoresASC":
                    consulta = consulta.OrderBy(u => u.ApellidoPaternoProductores);
                    break;
                case "apellidoPaternoProductoresDESC":
                    consulta = consulta.OrderByDescending(u => u.ApellidoPaternoProductores);
                    break;
                case "curpProductorCuadernilloSaderProductoresASC":
                    consulta = consulta.OrderBy(u => u.CurpProductorCuadernilloSaderProductores);
                    break;
                case "curpProductorCuadernilloSaderProductoresDESC":
                    consulta = consulta.OrderByDescending(u => u.CurpProductorCuadernilloSaderProductores);
                    break;
                case "folioCuadernilloSaderProductoresASC":
                    consulta = consulta.OrderBy(u => u.FolioCuadernilloSaderProductores);
                    break;
                case "folioCuadernilloSaderProductoresDESC":
                    consulta = consulta.OrderByDescending(u => u.FolioCuadernilloSaderProductores);
                    break;
                case "noDeIdentificacionProductoresASC":
                    consulta = consulta.OrderBy(u => u.NoDeIdentificacionProductores);
                    break;
                case "noDeIdentificacionProductoresDESC":
                    consulta = consulta.OrderByDescending(u => u.NoDeIdentificacionProductores);
                    break;
                case "nombresProductoresASC":
                    consulta = consulta.OrderBy(u => u.NombresProductores);
                    break;
                case "nombresProductoresDESC":
                    consulta = consulta.OrderByDescending(u => u.NombresProductores);
                    break;
                case "fechaDeRegistroProductoresASC":
                    consulta = consulta.OrderBy(u => u.FechaDeRegistroProductores);
                    break;
                case "fechaDeRegistroProductoresDESC":
                    consulta = consulta.OrderByDescending(u => u.FechaDeRegistroProductores);
                    break;
                case "idCedasProductoresASC":
                    consulta = consulta.OrderBy(u => u.IdCedasProductores);
                    break;
                case "idCedasProductoresDESC":
                    consulta = consulta.OrderByDescending(u => u.IdCedasProductores);
                    break;
                default:
                    consulta = consulta.OrderBy(u => u.LlavePrimariaProductores);
                    break;
            }

            consulta = consulta.Skip(tamanoDePagina * (pagina - 1)).Take(tamanoDePagina);
            return consulta.ToList();
        }

        /// <summary>
        /// Método que recupera el total de elementos.
        /// </summary>
        /// <param name = "llavePrimariaProductores">Llave Primaria (Productores) Igual a (=?)</param>
        /// <returns>int</returns>
        public int DetalleProductoresTotal(long? llavePrimariaProductores)
        {
            var consulta =
                from Productores in _mysqlr.Productores.AsQueryable()
                where (llavePrimariaProductores == null || Productores.LlavePrimaria == llavePrimariaProductores /* Igual a (=?) */)select new
                {
                fl_unique = 1
                }

            ;
            return consulta.Count();
        }

        /// <summary>
        /// Lista Productores
        /// </summary>
        /// <param name = "pagina">Página</param>
        /// <param name = "tamanoDePagina">Tamaño de Página</param>
        /// <param name = "ordenarPor">Ordenar por</param>
        /// <param name = "tipo">Tipo</param>
        /// <returns>List</returns>
        public List<DTO.ListaProductores> ListaProductores(int pagina, int tamanoDePagina, string ordenarPor, string tipo)
        {
            if (pagina == 0)
            {
                pagina = 1;
            }

            if (tamanoDePagina == 0)
            {
                tamanoDePagina = Int32.MaxValue;
            }

            var consulta =
                from Productores in _mysqlr.Productores.AsQueryable()
                where Productores.Estatus == 1 /* Igual a (=?) */
                select new DTO.ListaProductores{LlavePrimariaProductores = Productores.LlavePrimaria, ApellidoMaternoProductores = Productores.ApellidoMaterno, ApellidoPaternoProductores = Productores.ApellidoPaterno, CurpProductorCuadernilloSaderProductores = Productores.CurpProductorCuadernilloSader, FolioCuadernilloSaderProductores = Productores.FolioCuadernilloSader, NoDeIdentificacionProductores = Productores.NoDeIdentificacion, NombresProductores = Productores.Nombres, FechaDeRegistroProductores = Productores.FechaDeRegistro};
            if (tipo == null || tipo == "")
            {
                tipo = "ASC";
            }

            switch (ordenarPor + tipo)
            {
                case "llavePrimariaProductoresASC":
                    consulta = consulta.OrderBy(u => u.LlavePrimariaProductores);
                    break;
                case "llavePrimariaProductoresDESC":
                    consulta = consulta.OrderByDescending(u => u.LlavePrimariaProductores);
                    break;
                case "apellidoMaternoProductoresASC":
                    consulta = consulta.OrderBy(u => u.ApellidoMaternoProductores);
                    break;
                case "apellidoMaternoProductoresDESC":
                    consulta = consulta.OrderByDescending(u => u.ApellidoMaternoProductores);
                    break;
                case "apellidoPaternoProductoresASC":
                    consulta = consulta.OrderBy(u => u.ApellidoPaternoProductores);
                    break;
                case "apellidoPaternoProductoresDESC":
                    consulta = consulta.OrderByDescending(u => u.ApellidoPaternoProductores);
                    break;
                case "curpProductorCuadernilloSaderProductoresASC":
                    consulta = consulta.OrderBy(u => u.CurpProductorCuadernilloSaderProductores);
                    break;
                case "curpProductorCuadernilloSaderProductoresDESC":
                    consulta = consulta.OrderByDescending(u => u.CurpProductorCuadernilloSaderProductores);
                    break;
                case "folioCuadernilloSaderProductoresASC":
                    consulta = consulta.OrderBy(u => u.FolioCuadernilloSaderProductores);
                    break;
                case "folioCuadernilloSaderProductoresDESC":
                    consulta = consulta.OrderByDescending(u => u.FolioCuadernilloSaderProductores);
                    break;
                case "noDeIdentificacionProductoresASC":
                    consulta = consulta.OrderBy(u => u.NoDeIdentificacionProductores);
                    break;
                case "noDeIdentificacionProductoresDESC":
                    consulta = consulta.OrderByDescending(u => u.NoDeIdentificacionProductores);
                    break;
                case "nombresProductoresASC":
                    consulta = consulta.OrderBy(u => u.NombresProductores);
                    break;
                case "nombresProductoresDESC":
                    consulta = consulta.OrderByDescending(u => u.NombresProductores);
                    break;
                case "fechaDeRegistroProductoresASC":
                    consulta = consulta.OrderBy(u => u.FechaDeRegistroProductores);
                    break;
                case "fechaDeRegistroProductoresDESC":
                    consulta = consulta.OrderByDescending(u => u.FechaDeRegistroProductores);
                    break;
                default:
                    consulta = consulta.OrderBy(u => u.LlavePrimariaProductores);
                    break;
            }

            consulta = consulta.Skip(tamanoDePagina * (pagina - 1)).Take(tamanoDePagina);
            return consulta.ToList();
        }

        /// <summary>
        /// Método que recupera el total de elementos.
        /// </summary>
        /// <returns>int</returns>
        public int ListaProductoresTotal()
        {
            var consulta =
                from Productores in _mysqlr.Productores.AsQueryable()
                where Productores.Estatus == 1 /* Igual a (=?) */
                select new
                {
                fl_unique = 1
                }

            ;
            return consulta.Count();
        }

        /// <summary>
        /// Tabla de Datos Productores
        /// </summary>
        /// <param name = "apellidoMaternoProductores">APELLIDO MATERNO (Productores) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "apellidoPaternoProductores">APELLIDO PATERNO (Productores) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "curpProductorCuadernilloSaderProductores">CURP PRODUCTOR CUADERNILLO SADER (Productores) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "folioCuadernilloSaderProductores">FOLIO CUADERNILLO SADER (Productores) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "idCedasProductores">ID CEDAS (Productores) Igual a (=?)</param>
        /// <param name = "idCedaSegalmexCedas">ID CEDA SEGALMEX (Cedas) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "noDeIdentificacionProductores">NO. DE IDENTIFICACI&#211;N (Productores) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "nombresProductores">NOMBRE(S) (Productores) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "pagina">Página</param>
        /// <param name = "tamanoDePagina">Tamaño de Página</param>
        /// <param name = "ordenarPor">Ordenar por</param>
        /// <param name = "tipo">Tipo</param>
        /// <returns>List</returns>
        public List<DTO.TablaDeDatosProductores> TablaDeDatosProductores(string apellidoMaternoProductores, string apellidoPaternoProductores, string curpProductorCuadernilloSaderProductores, string folioCuadernilloSaderProductores, long? idCedasProductores, string idCedaSegalmexCedas, string noDeIdentificacionProductores, string nombresProductores, int pagina, int tamanoDePagina, string ordenarPor, string tipo)
        {
            if (pagina == 0)
            {
                pagina = 1;
            }

            if (tamanoDePagina == 0)
            {
                tamanoDePagina = Int32.MaxValue;
            }

            var consulta =
                from Productores in _mysqlr.Productores.AsQueryable()join Cedas in _mysqlr.Cedas on Productores.IdCedas equals Cedas.LlavePrimaria // Cedas Productores
                join Estatus in _mysqlr.Estatus on Productores.Estatus equals Estatus.LlavePrimaria // Estatus Productores
                where (Productores.ApellidoMaterno.Contains(apellidoMaternoProductores) /* Contiene (LIKE '%?%') */ || Productores.ApellidoPaterno.Contains(apellidoPaternoProductores) /* Contiene (LIKE '%?%') */ || Productores.CurpProductorCuadernilloSader.Contains(curpProductorCuadernilloSaderProductores) /* Contiene (LIKE '%?%') */ || Productores.FolioCuadernilloSader.Contains(folioCuadernilloSaderProductores) /* Contiene (LIKE '%?%') */ || Cedas.IdCedaSegalmex.Contains(idCedaSegalmexCedas) /* Contiene (LIKE '%?%') */ || Productores.NoDeIdentificacion.Contains(noDeIdentificacionProductores) /* Contiene (LIKE '%?%') */ || Productores.Nombres.Contains(nombresProductores) /* Contiene (LIKE '%?%') */) && (idCedasProductores == null || Productores.IdCedas == idCedasProductores /* Igual a (=?) */) && Productores.Estatus != 3 /* Diferente a (<>?) */
                select new DTO.TablaDeDatosProductores{LlavePrimariaProductores = Productores.LlavePrimaria, FechaDeCreacionProductores = Productores.FechaDeCreacion, EstatusProductores = Productores.Estatus, FechaDeModificacionProductores = Productores.FechaDeModificacion, ApellidoMaternoProductores = Productores.ApellidoMaterno, ApellidoPaternoProductores = Productores.ApellidoPaterno, CurpProductorCuadernilloSaderProductores = Productores.CurpProductorCuadernilloSader, FolioCuadernilloSaderProductores = Productores.FolioCuadernilloSader, NoDeIdentificacionProductores = Productores.NoDeIdentificacion, NombresProductores = Productores.Nombres, FechaDeRegistroProductores = Productores.FechaDeRegistro, IdCedaSegalmexCedas = Cedas.IdCedaSegalmex, NombreEstatus = Estatus.Nombre};
            if (tipo == null || tipo == "")
            {
                tipo = "ASC";
            }

            switch (ordenarPor + tipo)
            {
                case "llavePrimariaProductoresASC":
                    consulta = consulta.OrderBy(u => u.LlavePrimariaProductores);
                    break;
                case "llavePrimariaProductoresDESC":
                    consulta = consulta.OrderByDescending(u => u.LlavePrimariaProductores);
                    break;
                case "fechaDeCreacionProductoresASC":
                    consulta = consulta.OrderBy(u => u.FechaDeCreacionProductores);
                    break;
                case "fechaDeCreacionProductoresDESC":
                    consulta = consulta.OrderByDescending(u => u.FechaDeCreacionProductores);
                    break;
                case "estatusProductoresASC":
                    consulta = consulta.OrderBy(u => u.EstatusProductores);
                    break;
                case "estatusProductoresDESC":
                    consulta = consulta.OrderByDescending(u => u.EstatusProductores);
                    break;
                case "fechaDeModificacionProductoresASC":
                    consulta = consulta.OrderBy(u => u.FechaDeModificacionProductores);
                    break;
                case "fechaDeModificacionProductoresDESC":
                    consulta = consulta.OrderByDescending(u => u.FechaDeModificacionProductores);
                    break;
                case "apellidoMaternoProductoresASC":
                    consulta = consulta.OrderBy(u => u.ApellidoMaternoProductores);
                    break;
                case "apellidoMaternoProductoresDESC":
                    consulta = consulta.OrderByDescending(u => u.ApellidoMaternoProductores);
                    break;
                case "apellidoPaternoProductoresASC":
                    consulta = consulta.OrderBy(u => u.ApellidoPaternoProductores);
                    break;
                case "apellidoPaternoProductoresDESC":
                    consulta = consulta.OrderByDescending(u => u.ApellidoPaternoProductores);
                    break;
                case "curpProductorCuadernilloSaderProductoresASC":
                    consulta = consulta.OrderBy(u => u.CurpProductorCuadernilloSaderProductores);
                    break;
                case "curpProductorCuadernilloSaderProductoresDESC":
                    consulta = consulta.OrderByDescending(u => u.CurpProductorCuadernilloSaderProductores);
                    break;
                case "folioCuadernilloSaderProductoresASC":
                    consulta = consulta.OrderBy(u => u.FolioCuadernilloSaderProductores);
                    break;
                case "folioCuadernilloSaderProductoresDESC":
                    consulta = consulta.OrderByDescending(u => u.FolioCuadernilloSaderProductores);
                    break;
                case "noDeIdentificacionProductoresASC":
                    consulta = consulta.OrderBy(u => u.NoDeIdentificacionProductores);
                    break;
                case "noDeIdentificacionProductoresDESC":
                    consulta = consulta.OrderByDescending(u => u.NoDeIdentificacionProductores);
                    break;
                case "nombresProductoresASC":
                    consulta = consulta.OrderBy(u => u.NombresProductores);
                    break;
                case "nombresProductoresDESC":
                    consulta = consulta.OrderByDescending(u => u.NombresProductores);
                    break;
                case "fechaDeRegistroProductoresASC":
                    consulta = consulta.OrderBy(u => u.FechaDeRegistroProductores);
                    break;
                case "fechaDeRegistroProductoresDESC":
                    consulta = consulta.OrderByDescending(u => u.FechaDeRegistroProductores);
                    break;
                case "llavePrimariaEncriptadoProductoresASC":
                    consulta = consulta.OrderBy(u => u.LlavePrimariaProductores);
                    break;
                case "llavePrimariaEncriptadoProductoresDESC":
                    consulta = consulta.OrderByDescending(u => u.LlavePrimariaProductores);
                    break;
                case "fechaDeCreacionFormateadoProductoresASC":
                    consulta = consulta.OrderBy(u => u.FechaDeCreacionProductores);
                    break;
                case "fechaDeCreacionFormateadoProductoresDESC":
                    consulta = consulta.OrderByDescending(u => u.FechaDeCreacionProductores);
                    break;
                case "fechaDeModificacionFormateadoProductoresASC":
                    consulta = consulta.OrderBy(u => u.FechaDeCreacionProductores);
                    break;
                case "fechaDeModificacionFormateadoProductoresDESC":
                    consulta = consulta.OrderByDescending(u => u.FechaDeCreacionProductores);
                    break;
                case "idCedaSegalmexCedasASC":
                    consulta = consulta.OrderBy(u => u.IdCedaSegalmexCedas);
                    break;
                case "idCedaSegalmexCedasDESC":
                    consulta = consulta.OrderByDescending(u => u.IdCedaSegalmexCedas);
                    break;
                case "nombreEstatusASC":
                    consulta = consulta.OrderBy(u => u.NombreEstatus);
                    break;
                case "nombreEstatusDESC":
                    consulta = consulta.OrderByDescending(u => u.NombreEstatus);
                    break;
                default:
                    consulta = consulta.OrderBy(u => u.LlavePrimariaProductores);
                    break;
            }

            consulta = consulta.Skip(tamanoDePagina * (pagina - 1)).Take(tamanoDePagina);
            return consulta.ToList();
        }

        /// <summary>
        /// Método que recupera el total de elementos.
        /// </summary>
        /// <param name = "apellidoMaternoProductores">APELLIDO MATERNO (Productores) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "apellidoPaternoProductores">APELLIDO PATERNO (Productores) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "curpProductorCuadernilloSaderProductores">CURP PRODUCTOR CUADERNILLO SADER (Productores) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "folioCuadernilloSaderProductores">FOLIO CUADERNILLO SADER (Productores) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "idCedasProductores">ID CEDAS (Productores) Igual a (=?)</param>
        /// <param name = "idCedaSegalmexCedas">ID CEDA SEGALMEX (Cedas) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "noDeIdentificacionProductores">NO. DE IDENTIFICACI&#211;N (Productores) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "nombresProductores">NOMBRE(S) (Productores) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <returns>int</returns>
        public int TablaDeDatosProductoresTotal(string apellidoMaternoProductores, string apellidoPaternoProductores, string curpProductorCuadernilloSaderProductores, string folioCuadernilloSaderProductores, long? idCedasProductores, string idCedaSegalmexCedas, string noDeIdentificacionProductores, string nombresProductores)
        {
            var consulta =
                from Productores in _mysqlr.Productores.AsQueryable()join Cedas in _mysqlr.Cedas on Productores.IdCedas equals Cedas.LlavePrimaria // Cedas Productores
                join Estatus in _mysqlr.Estatus on Productores.Estatus equals Estatus.LlavePrimaria // Estatus Productores
                where (Productores.ApellidoMaterno.Contains(apellidoMaternoProductores) /* Contiene (LIKE '%?%') */ || Productores.ApellidoPaterno.Contains(apellidoPaternoProductores) /* Contiene (LIKE '%?%') */ || Productores.CurpProductorCuadernilloSader.Contains(curpProductorCuadernilloSaderProductores) /* Contiene (LIKE '%?%') */ || Productores.FolioCuadernilloSader.Contains(folioCuadernilloSaderProductores) /* Contiene (LIKE '%?%') */ || Cedas.IdCedaSegalmex.Contains(idCedaSegalmexCedas) /* Contiene (LIKE '%?%') */ || Productores.NoDeIdentificacion.Contains(noDeIdentificacionProductores) /* Contiene (LIKE '%?%') */ || Productores.Nombres.Contains(nombresProductores) /* Contiene (LIKE '%?%') */) && (idCedasProductores == null || Productores.IdCedas == idCedasProductores /* Igual a (=?) */) && Productores.Estatus != 3 /* Diferente a (<>?) */
                select new
                {
                fl_unique = 1
                }

            ;
            return consulta.Count();
        }

#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        public Productores(Mysqlr mysqlr)
        {
            this._mysqlr = mysqlr;
        }
#endregion
    }
}