using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Codium.Web.Models.MysqlrRepositorios
{
    /// <summary>
    /// Cedas Imágenes
    /// </summary>
    public interface ICedasImagenes
    {
#region Métodos
        /// <summary>
        /// Agregar CedasImagenes
        /// </summary>
        /// <param name = "cedasImagenes">Entidad a agregar</param>
        void AgregarCedasImagenes(MysqlrEntidades.CedasImagenes cedasImagenes);
        /// <summary>
        /// Recuperar CedasImagenes
        /// </summary>
        /// <param name = "id">Identificador</param>
        /// <returns>MysqlrEntidades.CedasImagenes</returns>
        MysqlrEntidades.CedasImagenes RecuperarCedasImagenes(int id);
        /// <summary>
        /// Lista CedasImagenes
        /// </summary>
        /// <returns>IEnumerable</returns>
        IEnumerable<MysqlrEntidades.CedasImagenes> ListaCedasImagenes();
        /// <summary>
        /// Borrar CedasImagenes
        /// </summary>
        /// <param name = "id">Identificador</param>
        bool BorrarCedasImagenes(int id);
        /// <summary>
        /// Imagenes
        /// </summary>
        /// <param name = "idCedaCedasImagenes">Id ceda (Cedas Im&#225;genes) Igual a (=?)</param>
        /// <param name = "pagina">Página</param>
        /// <param name = "tamanoDePagina">Tamaño de Página</param>
        /// <param name = "ordenarPor">Ordenar por</param>
        /// <param name = "tipo">Tipo</param>
        /// <returns>List</returns>
        List<DTO.Imagenes> Imagenes(long? idCedaCedasImagenes, int pagina, int tamanoDePagina, string ordenarPor, string tipo);
        /// <summary>
        /// Método que recupera el total de elementos.
        /// </summary>
        /// <param name = "idCedaCedasImagenes">Id ceda (Cedas Im&#225;genes) Igual a (=?)</param>
        /// <returns>int</returns>
        int ImagenesTotal(long? idCedaCedasImagenes);
#endregion
    }
}