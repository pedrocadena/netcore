using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Codium.Web.Models.MysqlrRepositorios
{
    /// <summary>
    /// PRODUCTORES
    /// </summary>
    public interface IProductores
    {
#region Métodos
        /// <summary>
        /// Agregar Productores
        /// </summary>
        /// <param name = "productores">Entidad a agregar</param>
        void AgregarProductores(MysqlrEntidades.Productores productores);
        /// <summary>
        /// Recuperar Productores
        /// </summary>
        /// <param name = "id">Identificador</param>
        /// <returns>MysqlrEntidades.Productores</returns>
        MysqlrEntidades.Productores RecuperarProductores(long id);
        /// <summary>
        /// Lista Productores
        /// </summary>
        /// <returns>IEnumerable</returns>
        IEnumerable<MysqlrEntidades.Productores> ListaProductores();
        /// <summary>
        /// Borrar Productores
        /// </summary>
        /// <param name = "id">Identificador</param>
        bool BorrarProductores(long id);
        /// <summary>
        /// Actualizar Productores
        /// </summary>
        /// <param name = "estatusProductores1">Estatus (Productores) Igual a (=?)</param>
        /// <param name = "llavePrimariaProductores2">Llave Primaria (Productores) Igual a (=?)</param>
        void ActualizarProductores(SByte estatusProductores1, long? llavePrimariaProductores2);
        /// <summary>
        /// Detalle Productores
        /// </summary>
        /// <param name = "llavePrimariaProductores">Llave Primaria (Productores) Igual a (=?)</param>
        /// <param name = "pagina">Página</param>
        /// <param name = "tamanoDePagina">Tamaño de Página</param>
        /// <param name = "ordenarPor">Ordenar por</param>
        /// <param name = "tipo">Tipo</param>
        /// <returns>List</returns>
        List<DTO.DetalleProductores> DetalleProductores(long? llavePrimariaProductores, int pagina, int tamanoDePagina, string ordenarPor, string tipo);
        /// <summary>
        /// Método que recupera el total de elementos.
        /// </summary>
        /// <param name = "llavePrimariaProductores">Llave Primaria (Productores) Igual a (=?)</param>
        /// <returns>int</returns>
        int DetalleProductoresTotal(long? llavePrimariaProductores);
        /// <summary>
        /// Lista Productores
        /// </summary>
        /// <param name = "pagina">Página</param>
        /// <param name = "tamanoDePagina">Tamaño de Página</param>
        /// <param name = "ordenarPor">Ordenar por</param>
        /// <param name = "tipo">Tipo</param>
        /// <returns>List</returns>
        List<DTO.ListaProductores> ListaProductores(int pagina, int tamanoDePagina, string ordenarPor, string tipo);
        /// <summary>
        /// Método que recupera el total de elementos.
        /// </summary>
        /// <returns>int</returns>
        int ListaProductoresTotal();
        /// <summary>
        /// Tabla de Datos Productores
        /// </summary>
        /// <param name = "apellidoMaternoProductores">APELLIDO MATERNO (Productores) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "apellidoPaternoProductores">APELLIDO PATERNO (Productores) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "curpProductorCuadernilloSaderProductores">CURP PRODUCTOR CUADERNILLO SADER (Productores) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "folioCuadernilloSaderProductores">FOLIO CUADERNILLO SADER (Productores) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "idCedasProductores">ID CEDAS (Productores) Igual a (=?)</param>
        /// <param name = "idCedaSegalmexCedas">ID CEDA SEGALMEX (Cedas) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "noDeIdentificacionProductores">NO. DE IDENTIFICACI&#211;N (Productores) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "nombresProductores">NOMBRE(S) (Productores) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "pagina">Página</param>
        /// <param name = "tamanoDePagina">Tamaño de Página</param>
        /// <param name = "ordenarPor">Ordenar por</param>
        /// <param name = "tipo">Tipo</param>
        /// <returns>List</returns>
        List<DTO.TablaDeDatosProductores> TablaDeDatosProductores(string apellidoMaternoProductores, string apellidoPaternoProductores, string curpProductorCuadernilloSaderProductores, string folioCuadernilloSaderProductores, long? idCedasProductores, string idCedaSegalmexCedas, string noDeIdentificacionProductores, string nombresProductores, int pagina, int tamanoDePagina, string ordenarPor, string tipo);
        /// <summary>
        /// Método que recupera el total de elementos.
        /// </summary>
        /// <param name = "apellidoMaternoProductores">APELLIDO MATERNO (Productores) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "apellidoPaternoProductores">APELLIDO PATERNO (Productores) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "curpProductorCuadernilloSaderProductores">CURP PRODUCTOR CUADERNILLO SADER (Productores) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "folioCuadernilloSaderProductores">FOLIO CUADERNILLO SADER (Productores) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "idCedasProductores">ID CEDAS (Productores) Igual a (=?)</param>
        /// <param name = "idCedaSegalmexCedas">ID CEDA SEGALMEX (Cedas) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "noDeIdentificacionProductores">NO. DE IDENTIFICACI&#211;N (Productores) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <param name = "nombresProductores">NOMBRE(S) (Productores) Contiene (LIKE &#39;%?%&#39;)</param>
        /// <returns>int</returns>
        int TablaDeDatosProductoresTotal(string apellidoMaternoProductores, string apellidoPaternoProductores, string curpProductorCuadernilloSaderProductores, string folioCuadernilloSaderProductores, long? idCedasProductores, string idCedaSegalmexCedas, string noDeIdentificacionProductores, string nombresProductores);
#endregion
    }
}