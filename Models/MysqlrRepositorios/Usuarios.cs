using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
namespace Codium.Web.Models.MysqlrRepositorios
{
    /// <summary>
    /// Usuarios
    /// </summary>
    public class Usuarios : IUsuarios
    {
#region Variables Privadas
        /// <summary>
        /// Contexto de Base de Datos
        /// </summary>
        private readonly Mysqlr _mysqlr;
#endregion
#region Métodos
        /// <summary>
        /// Agregar Usuarios
        /// </summary>
        /// <param name = "usuarios">Entidad a agregar</param>
        public void AgregarUsuarios(MysqlrEntidades.Usuarios usuarios)
        {
            this._mysqlr.Usuarios.Add(usuarios);
        }

        /// <summary>
        /// Recuperar Usuarios
        /// </summary>
        /// <param name = "id">Identificador</param>
        /// <returns>MysqlrEntidades.Usuarios</returns>
        public MysqlrEntidades.Usuarios RecuperarUsuarios(int id)
        {
            return this._mysqlr.Usuarios.FirstOrDefault(u => u.Identificador == id);
        }

        /// <summary>
        /// Lista Usuarios
        /// </summary>
        /// <returns>IEnumerable</returns>
        public IEnumerable<MysqlrEntidades.Usuarios> ListaUsuarios()
        {
            return this._mysqlr.Usuarios;
        }

        /// <summary>
        /// Borrar Usuarios
        /// </summary>
        /// <param name = "id">Identificador</param>
        public bool BorrarUsuarios(int id)
        {
            bool borrado = false;
            MysqlrEntidades.Usuarios usuarios = RecuperarUsuarios(id);
            if (usuarios != null)
            {
                borrado = true;
                this._mysqlr.Usuarios.Remove(usuarios);
            }

            return borrado;
        }

        /// <summary>
        /// Login
        /// </summary>
        /// <param name = "correoUsuarios">Correo (Usuarios) Igual a (=?)</param>
        /// <param name = "contrasenaUsuarios">Contrase&#241;a (Usuarios) Igual a (=?)</param>
        /// <param name = "pagina">Página</param>
        /// <param name = "tamanoDePagina">Tamaño de Página</param>
        /// <param name = "ordenarPor">Ordenar por</param>
        /// <param name = "tipo">Tipo</param>
        /// <returns>List</returns>
        public List<DTO.Login> Login(string correoUsuarios, string contrasenaUsuarios, int pagina, int tamanoDePagina, string ordenarPor, string tipo)
        {
            if (pagina == 0)
            {
                pagina = 1;
            }

            if (tamanoDePagina == 0)
            {
                tamanoDePagina = Int32.MaxValue;
            }

            var consulta =
                from Usuarios in _mysqlr.Usuarios.AsQueryable()join Roles in _mysqlr.Roles on Usuarios.Rol equals Roles.Identificador into RolesGroup
                from Roles in RolesGroup.DefaultIfEmpty() // Roles Usuarios
                where Usuarios.Correo == correoUsuarios /* Igual a (=?) */ && Usuarios.Contrasena == contrasenaUsuarios /* Igual a (=?) */ && Usuarios.Identificador != null /* No es nulo (IS NOT NULL) */
                select new DTO.Login{IdentificadorUsuarios = Usuarios.Identificador, NombreRoles = Roles.Nombre};
            if (tipo == null || tipo == "")
            {
                tipo = "ASC";
            }

            switch (ordenarPor + tipo)
            {
                case "identificadorUsuariosASC":
                    consulta = consulta.OrderBy(u => u.IdentificadorUsuarios);
                    break;
                case "identificadorUsuariosDESC":
                    consulta = consulta.OrderByDescending(u => u.IdentificadorUsuarios);
                    break;
                case "nombreRolesASC":
                    consulta = consulta.OrderBy(u => u.NombreRoles);
                    break;
                case "nombreRolesDESC":
                    consulta = consulta.OrderByDescending(u => u.NombreRoles);
                    break;
                default:
                    consulta = consulta.OrderBy(u => u.IdentificadorUsuarios);
                    break;
            }

            consulta = consulta.Skip(tamanoDePagina * (pagina - 1)).Take(tamanoDePagina);
            return consulta.ToList();
        }

        /// <summary>
        /// Método que recupera el total de elementos.
        /// </summary>
        /// <param name = "correoUsuarios">Correo (Usuarios) Igual a (=?)</param>
        /// <param name = "contrasenaUsuarios">Contrase&#241;a (Usuarios) Igual a (=?)</param>
        /// <returns>int</returns>
        public int LoginTotal(string correoUsuarios, string contrasenaUsuarios)
        {
            var consulta =
                from Usuarios in _mysqlr.Usuarios.AsQueryable()join Roles in _mysqlr.Roles on Usuarios.Rol equals Roles.Identificador into RolesGroup
                from Roles in RolesGroup.DefaultIfEmpty() // Roles Usuarios
                where Usuarios.Correo == correoUsuarios /* Igual a (=?) */ && Usuarios.Contrasena == contrasenaUsuarios /* Igual a (=?) */ && Usuarios.Identificador != null /* No es nulo (IS NOT NULL) */
                select new
                {
                fl_unique = 1
                }

            ;
            return consulta.Count();
        }

#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        public Usuarios(Mysqlr mysqlr)
        {
            this._mysqlr = mysqlr;
        }
#endregion
    }
}