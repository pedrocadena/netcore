using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Codium.Web.Models.MysqlrRepositorios
{
    /// <summary>
    /// Menús
    /// </summary>
    public interface IMenus
    {
#region Métodos
        /// <summary>
        /// Agregar Menus
        /// </summary>
        /// <param name = "menus">Entidad a agregar</param>
        void AgregarMenus(MysqlrEntidades.Menus menus);
        /// <summary>
        /// Recuperar Menus
        /// </summary>
        /// <param name = "id">Identificador</param>
        /// <returns>MysqlrEntidades.Menus</returns>
        MysqlrEntidades.Menus RecuperarMenus(Int16 id);
        /// <summary>
        /// Lista Menuses
        /// </summary>
        /// <returns>IEnumerable</returns>
        IEnumerable<MysqlrEntidades.Menus> ListaMenuses();
        /// <summary>
        /// Borrar Menus
        /// </summary>
        /// <param name = "id">Identificador</param>
        bool BorrarMenus(Int16 id);
        /// <summary>
        /// Menús
        /// </summary>
        /// <param name = "pagina">Página</param>
        /// <param name = "tamanoDePagina">Tamaño de Página</param>
        /// <param name = "ordenarPor">Ordenar por</param>
        /// <param name = "tipo">Tipo</param>
        /// <returns>List</returns>
        List<DTO.ListaDeMenus> ListaDeMenus(int pagina, int tamanoDePagina, string ordenarPor, string tipo);
        /// <summary>
        /// Método que recupera el total de elementos.
        /// </summary>
        /// <returns>int</returns>
        int ListaDeMenusTotal();
#endregion
    }
}