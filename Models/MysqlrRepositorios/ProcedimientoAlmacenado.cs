using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
namespace Codium.Web.Models.MysqlrRepositorios
{
    /// <summary>
    /// Procedimiento Almacenado
    /// </summary>
    public class ProcedimientoAlmacenado : IProcedimientoAlmacenado
    {
#region Variables Privadas
        /// <summary>
        /// Contexto de Base de Datos
        /// </summary>
        private readonly Mysqlr _mysqlr;
#endregion
#region Métodos
        /// <summary>
        /// TestDeProcedimiento
        /// </summary>
        /// <param name = "pagina">Página</param>
        /// <param name = "tamanoDePagina">Tamaño de Página</param>
        /// <param name = "ordenarPor">Ordenar por</param>
        /// <param name = "tipo">Tipo</param>
        /// <returns>List</returns>
        public List<DTO.Testdeprocedimiento> Testdeprocedimiento(int pagina, int tamanoDePagina, string ordenarPor, string tipo)
        {
            if (pagina == 0)
            {
                pagina = 1;
            }

            if (tamanoDePagina == 0)
            {
                tamanoDePagina = Int32.MaxValue;
            }

            var x = 1;
            return this._mysqlr.Set<DTO.Testdeprocedimiento>().FromSqlInterpolated($"CALL `TestDeProcedimiento`( {x})").ToList();
        }

        /// <summary>
        /// Método que recupera el total de elementos.
        /// </summary>
        /// <returns>int</returns>
        public int TestdeprocedimientoTotal()
        {
            int total = 0;
            System.Data.Common.DbConnection conexion = this._mysqlr.Database.GetDbConnection();
            conexion.Open();
            System.Data.Common.DbCommand comando = conexion.CreateCommand();
            comando.CommandText = "TestDeProcedimientoCount";
            comando.CommandType = System.Data.CommandType.StoredProcedure;
            total = (int)comando.ExecuteScalar();
            conexion.Close();
            return total;
        }

#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        public ProcedimientoAlmacenado(Mysqlr mysqlr)
        {
            this._mysqlr = mysqlr;
        }
#endregion
    }
}