using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Codium.Web.Models.SqliteEntidades
{
    /// <summary>
    /// Test
    /// </summary>
    [Table("TB_TEST")]
    public class Test
    {
#region Propiedades
        /// <summary>
        /// Id
        /// </summary>  
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        [Column("FL_IN_ID", TypeName = "INTEGER")]
        public int Id { get; set; }

#endregion
#region Relaciones
#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        public Test()
        {
        }
#endregion
    }
}