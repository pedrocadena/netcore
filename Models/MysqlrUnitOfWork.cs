using Microsoft.EntityFrameworkCore;

using System;
using System.Threading.Tasks;
namespace Codium.Web.Models
{
    /// <summary>
    /// MySQL®
    /// </summary>
    public class MysqlrUnitOfWork : IMysqlrUnitOfWork
    {
#region Variables Privadas
        /// <summary>
        /// Contexto de Base de Datos
        /// </summary>
        private readonly Mysqlr _mysqlr;
        /// <summary>
        /// Procedimiento Almacenado
        /// </summary>
        private MysqlrRepositorios.IProcedimientoAlmacenado _procedimientoAlmacenado;
        /// <summary>
        /// CEDAS
        /// </summary>
        private MysqlrRepositorios.ICedas _cedas;
        /// <summary>
        /// Cedas Imágenes
        /// </summary>
        private MysqlrRepositorios.ICedasImagenes _cedasImagenes;
        /// <summary>
        /// Hoja 1
        /// </summary>
        private MysqlrRepositorios.ICedasOriginal _cedasOriginal;
        /// <summary>
        /// Menús
        /// </summary>
        private MysqlrRepositorios.IMenus _menus;
        /// <summary>
        /// PRODUCTORES
        /// </summary>
        private MysqlrRepositorios.IProductores _productores;
        /// <summary>
        /// Usuarios
        /// </summary>
        private MysqlrRepositorios.IUsuarios _usuarios;
#endregion
#region Propiedades Públicas
        /// <summary>
        /// Procedimiento Almacenado
        /// </summary>
        public MysqlrRepositorios.IProcedimientoAlmacenado ProcedimientoAlmacenado
        {
            get
            {
                return _procedimientoAlmacenado;
            }
        }

        /// <summary>
        /// CEDAS
        /// </summary>
        public MysqlrRepositorios.ICedas Cedas
        {
            get
            {
                return _cedas;
            }
        }

        /// <summary>
        /// Cedas Imágenes
        /// </summary>
        public MysqlrRepositorios.ICedasImagenes CedasImagenes
        {
            get
            {
                return _cedasImagenes;
            }
        }

        /// <summary>
        /// Hoja 1
        /// </summary>
        public MysqlrRepositorios.ICedasOriginal CedasOriginal
        {
            get
            {
                return _cedasOriginal;
            }
        }

        /// <summary>
        /// Menús
        /// </summary>
        public MysqlrRepositorios.IMenus Menus
        {
            get
            {
                return _menus;
            }
        }

        /// <summary>
        /// PRODUCTORES
        /// </summary>
        public MysqlrRepositorios.IProductores Productores
        {
            get
            {
                return _productores;
            }
        }

        /// <summary>
        /// Usuarios
        /// </summary>
        public MysqlrRepositorios.IUsuarios Usuarios
        {
            get
            {
                return _usuarios;
            }
        }

#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        public MysqlrUnitOfWork(Mysqlr mysqlr)
        {
            this._mysqlr = mysqlr;
            this._procedimientoAlmacenado = new MysqlrRepositorios.ProcedimientoAlmacenado(this._mysqlr);
            this._cedas = new MysqlrRepositorios.Cedas(this._mysqlr);
            this._cedasImagenes = new MysqlrRepositorios.CedasImagenes(this._mysqlr);
            this._cedasOriginal = new MysqlrRepositorios.CedasOriginal(this._mysqlr);
            this._menus = new MysqlrRepositorios.Menus(this._mysqlr);
            this._productores = new MysqlrRepositorios.Productores(this._mysqlr);
            this._usuarios = new MysqlrRepositorios.Usuarios(this._mysqlr);
        }

#endregion
#region Métodos
        /// <summary>
        /// Guardar Asíncrono
        /// </summary>
        /// <returns>int</returns>
        public async Task<int> GuardarAsync()
        {
            return await _mysqlr.SaveChangesAsync();
        }

        /// <summary>
        /// Guardar
        /// </summary>
        /// <returns>int</returns>
        public int Guardar()
        {
            return _mysqlr.SaveChanges();
        }

        /// <summary>
        /// Dispose
        /// </summary>
        public void Dispose() => _mysqlr.Dispose();
#endregion
    }
}