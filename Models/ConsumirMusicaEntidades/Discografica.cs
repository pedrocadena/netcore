using System;
using System.Collections.Generic;

namespace Codium.Web.Models.ConsumirMusicaEntidades
{
    /// <summary>
    /// Discográfica
    /// </summary>
    public class Discografica
    {
#region Clases
#endregion
#region Propiedades
        /// <summary>
        /// Área
        /// </summary>
        public string Area { get; set; }

        /// <summary>
        /// Código de Discográfica
        /// </summary>
        public int? CodigoDeDiscografica { get; set; }

        /// <summary>
        /// Desambiguación
        /// </summary>
        public string Desambiguacion { get; set; }

        /// <summary>
        /// Fallecido
        /// </summary>
        public bool? Fallecido { get; set; }

        /// <summary>
        /// Fecha de Fallecimiento
        /// </summary>
        public System.DateTime? FechaDeFallecimiento { get; set; }

        /// <summary>
        /// Fecha de Nacimiento
        /// </summary>
        public System.DateTime? FechaDeNacimiento { get; set; }

        /// <summary>
        /// MBID
        /// </summary>
        public Guid Mbid { get; set; }

        /// <summary>
        /// Nombre
        /// </summary>
        public string Nombre { get; set; }

        /// <summary>
        /// Ordenar Nombre
        /// </summary>
        public string OrdenarNombre { get; set; }

        /// <summary>
        /// País
        /// </summary>
        public string Pais { get; set; }

        /// <summary>
        /// Tipo
        /// </summary>
        public string Tipo { get; set; }

#endregion
#region Arreglos
#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        public Discografica()
        {
        }
#endregion
    }
}