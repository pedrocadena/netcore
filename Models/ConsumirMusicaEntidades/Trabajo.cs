using System;
using System.Collections.Generic;

namespace Codium.Web.Models.ConsumirMusicaEntidades
{
    /// <summary>
    /// Trabajo
    /// </summary>
    public class Trabajo
    {
#region Clases
#endregion
#region Propiedades
        /// <summary>
        /// Calificación
        /// </summary>
        public decimal? Calificacion { get; set; }

        /// <summary>
        /// Desambiguación
        /// </summary>
        public string Desambiguacion { get; set; }

        /// <summary>
        /// Idioma
        /// </summary>
        public string Idioma { get; set; }

        /// <summary>
        /// MBID
        /// </summary>
        public Guid Mbid { get; set; }

        /// <summary>
        /// Recuento de Votos
        /// </summary>
        public long? RecuentoDeVotos { get; set; }

        /// <summary>
        /// Tipo
        /// </summary>
        public string Tipo { get; set; }

        /// <summary>
        /// Título
        /// </summary>
        public string Titulo { get; set; }

#endregion
#region Arreglos
#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        public Trabajo()
        {
        }
#endregion
    }
}