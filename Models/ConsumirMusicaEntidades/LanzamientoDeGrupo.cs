using System;
using System.Collections.Generic;

namespace Codium.Web.Models.ConsumirMusicaEntidades
{
    /// <summary>
    /// Lanzamiento de Grupo
    /// </summary>
    public class LanzamientoDeGrupo
    {
#region Clases
#endregion
#region Propiedades
        /// <summary>
        /// Calificación
        /// </summary>
        public decimal? Calificacion { get; set; }

        /// <summary>
        /// Desambiguación
        /// </summary>
        public string Desambiguacion { get; set; }

        /// <summary>
        /// MBID
        /// </summary>
        public Guid Mbid { get; set; }

        /// <summary>
        /// Primera Fecha de Lanzamiento
        /// </summary>
        public System.DateTime? PrimeraFechaDeLanzamiento { get; set; }

        /// <summary>
        /// Recuento de Votos
        /// </summary>
        public long? RecuentoDeVotos { get; set; }

        /// <summary>
        /// Tipo Primario
        /// </summary>
        public string TipoPrimario { get; set; }

        /// <summary>
        /// Título
        /// </summary>
        public string Titulo { get; set; }

#endregion
#region Arreglos
#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        public LanzamientoDeGrupo()
        {
        }
#endregion
    }
}