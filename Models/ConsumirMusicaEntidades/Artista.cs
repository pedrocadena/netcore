using System;
using System.Collections.Generic;

namespace Codium.Web.Models.ConsumirMusicaEntidades
{
    /// <summary>
    /// Artista
    /// </summary>
    public class Artista
    {
#region Clases
#endregion
#region Propiedades
        /// <summary>
        /// Área
        /// </summary>
        public string Area { get; set; }

        /// <summary>
        /// Área de Inicio
        /// </summary>
        public string AreaDeInicio { get; set; }

        /// <summary>
        /// Área final
        /// </summary>
        public string AreaFinal { get; set; }

        /// <summary>
        /// Desambiguación
        /// </summary>
        public string Desambiguacion { get; set; }

        /// <summary>
        /// Fallecido
        /// </summary>
        public bool? Fallecido { get; set; }

        /// <summary>
        /// Fecha de Fallecimiento
        /// </summary>
        public System.DateTime? FechaDeFallecimiento { get; set; }

        /// <summary>
        /// Fecha de Nacimiento
        /// </summary>
        public System.DateTime? FechaDeNacimiento { get; set; }

        /// <summary>
        /// Género
        /// </summary>
        public string Genero { get; set; }

        /// <summary>
        /// MBID
        /// </summary>
        public Guid Mbid { get; set; }

        /// <summary>
        /// Nombre
        /// </summary>
        public string Nombre { get; set; }

        /// <summary>
        /// Ordenar Nombre
        /// </summary>
        public string OrdenarNombre { get; set; }

        /// <summary>
        /// País
        /// </summary>
        public string Pais { get; set; }

        /// <summary>
        /// Tipo
        /// </summary>
        public string Tipo { get; set; }

#endregion
#region Arreglos
#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        public Artista()
        {
        }
#endregion
    }
}