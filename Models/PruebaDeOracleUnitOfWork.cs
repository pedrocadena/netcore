using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

using System;
using System.Threading.Tasks;
namespace Codium.Web.Models
{
    /// <summary>
    /// Prueba de Oracle
    /// </summary>
    public class PruebaDeOracleUnitOfWork : IPruebaDeOracleUnitOfWork
    {
#region Variables Privadas
        /// <summary>
        /// Contexto de Base de Datos
        /// </summary>
        private readonly System.Data.Common.DbConnection _pruebaDeOracle;
        /// <summary>
        /// tb_menus
        /// </summary>
        private PruebaDeOracleRepositorios.ITbMenus _tbMenus;
#endregion
#region Propiedades Públicas
        /// <summary>
        /// tb_menus
        /// </summary>
        public PruebaDeOracleRepositorios.ITbMenus TbMenus
        {
            get
            {
                return _tbMenus;
            }
        }

#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        public PruebaDeOracleUnitOfWork(IConfiguration configuracion)
        {
            _pruebaDeOracle = new Sap.Data.Hana.HanaConnection(configuracion["ConnectionStrings:PruebaDeOracle"]);
            this._tbMenus = new PruebaDeOracleRepositorios.TbMenus(this._pruebaDeOracle);
        }

#endregion
#region Métodos
        /// <summary>
        /// Dispose
        /// </summary>
        public void Dispose() => _pruebaDeOracle.Dispose();
#endregion
    }
}