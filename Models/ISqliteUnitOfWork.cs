using System;
using System.Threading.Tasks;

namespace Codium.Web.Models
{
    /// <summary>
    /// SQLite
    /// </summary>
    public interface ISqliteUnitOfWork : IDisposable
    {
#region Variables Privadas
#endregion
#region Métodos
        /// <summary>
        /// Guardar Asíncrono
        /// </summary>
        /// <returns>int</returns>
        Task<int> GuardarAsync();
        /// <summary>
        /// Guardar
        /// </summary>
        /// <returns>int</returns>
        int Guardar();
#endregion
    }
}