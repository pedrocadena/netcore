using System;
using System.Collections.Generic;

using System.Linq;
namespace Codium.Web.Models
{
    /// <summary>
    /// Consumir Música
    /// </summary>
    public class ConsumirMusicaMetodos
    {
#region Métodos
        /// <summary>
        /// Consulta de Música
        /// </summary>
        /// <param name = "termino">Termino</param>
        /// <param name = "pagina">Página</param>
        /// <param name = "tamanoDePagina">Tamaño de Página</param>
        /// <param name = "ordenarPor">Ordenar por</param>
        /// <param name = "tipo">Tipo</param>
        /// <returns>List</returns>
        public static List<ConsumirMusicaEntidades.Artista> ConsultaDeMusica(string termino, int pagina, int tamanoDePagina, string ordenarPor, string tipo)
        {
            if (pagina == 0)
            {
                pagina = 1;
            }

            if (tamanoDePagina == 0)
            {
                tamanoDePagina = Int32.MaxValue;
            }

            List<ConsumirMusicaEntidades.Artista> lista = new List<ConsumirMusicaEntidades.Artista>();
            MetaBrainz.MusicBrainz.Query query = new MetaBrainz.MusicBrainz.Query();
            Guid parsedGuid;
            if (Guid.TryParse(termino, out parsedGuid))
            {
                var elemento = query.LookupArtist(parsedGuid);
                ConsumirMusicaEntidades.Artista artista = new ConsumirMusicaEntidades.Artista();
                if (elemento.Area != null)
                {
                    artista.Area = elemento.Area.Name;
                }

                if (elemento.BeginArea != null)
                {
                    artista.AreaDeInicio = elemento.BeginArea.Name;
                }

                if (elemento.EndArea != null)
                {
                    artista.AreaFinal = elemento.EndArea.Name;
                }

                artista.Desambiguacion = elemento.Disambiguation;
                if (elemento.LifeSpan != null)
                {
                    artista.Fallecido = elemento.LifeSpan.Ended;
                }

                if (elemento.LifeSpan != null && elemento.LifeSpan.End != null)
                {
                    artista.FechaDeFallecimiento = elemento.LifeSpan.End.NearestDate;
                }

                if (elemento.LifeSpan != null && elemento.LifeSpan.Begin != null)
                {
                    artista.FechaDeNacimiento = elemento.LifeSpan.Begin.NearestDate;
                }

                artista.Genero = elemento.Gender;
                artista.Mbid = elemento.Id;
                artista.Nombre = elemento.Name;
                artista.OrdenarNombre = elemento.SortName;
                artista.Pais = elemento.Country;
                artista.Tipo = elemento.Type;
                lista.Add(artista);
            }
            else
            {
                var resultado = query.FindArtists(termino, tamanoDePagina > 100 ? 100 : tamanoDePagina, 0).Results;
                foreach (var artistaElemento in resultado)
                {
                    var elemento = artistaElemento.Item;
                    ConsumirMusicaEntidades.Artista artista = new ConsumirMusicaEntidades.Artista();
                    if (elemento.Area != null)
                    {
                        artista.Area = elemento.Area.Name;
                    }

                    if (elemento.BeginArea != null)
                    {
                        artista.AreaDeInicio = elemento.BeginArea.Name;
                    }

                    if (elemento.EndArea != null)
                    {
                        artista.AreaFinal = elemento.EndArea.Name;
                    }

                    artista.Desambiguacion = elemento.Disambiguation;
                    if (elemento.LifeSpan != null)
                    {
                        artista.Fallecido = elemento.LifeSpan.Ended;
                    }

                    if (elemento.LifeSpan != null && elemento.LifeSpan.End != null)
                    {
                        artista.FechaDeFallecimiento = elemento.LifeSpan.End.NearestDate;
                    }

                    if (elemento.LifeSpan != null && elemento.LifeSpan.Begin != null)
                    {
                        artista.FechaDeNacimiento = elemento.LifeSpan.Begin.NearestDate;
                    }

                    artista.Genero = elemento.Gender;
                    artista.Mbid = elemento.Id;
                    artista.Nombre = elemento.Name;
                    artista.OrdenarNombre = elemento.SortName;
                    artista.Pais = elemento.Country;
                    artista.Tipo = elemento.Type;
                    lista.Add(artista);
                }
            }

            query.Close();
            switch (ordenarPor + tipo)
            {
                case "areaasc":
                    lista = lista.OrderBy(x => x.Area).ToList();
                    break;
                case "areadesc":
                    lista = lista.OrderByDescending(x => x.Area).ToList();
                    break;
                case "areaDeInicioasc":
                    lista = lista.OrderBy(x => x.AreaDeInicio).ToList();
                    break;
                case "areaDeIniciodesc":
                    lista = lista.OrderByDescending(x => x.AreaDeInicio).ToList();
                    break;
                case "areaFinalasc":
                    lista = lista.OrderBy(x => x.AreaFinal).ToList();
                    break;
                case "areaFinaldesc":
                    lista = lista.OrderByDescending(x => x.AreaFinal).ToList();
                    break;
                case "desambiguacionasc":
                    lista = lista.OrderBy(x => x.Desambiguacion).ToList();
                    break;
                case "desambiguaciondesc":
                    lista = lista.OrderByDescending(x => x.Desambiguacion).ToList();
                    break;
                case "fallecidoasc":
                    lista = lista.OrderBy(x => x.Fallecido).ToList();
                    break;
                case "fallecidodesc":
                    lista = lista.OrderByDescending(x => x.Fallecido).ToList();
                    break;
                case "fechaDeFallecimientoasc":
                    lista = lista.OrderBy(x => x.FechaDeFallecimiento).ToList();
                    break;
                case "fechaDeFallecimientodesc":
                    lista = lista.OrderByDescending(x => x.FechaDeFallecimiento).ToList();
                    break;
                case "fechaDeNacimientoasc":
                    lista = lista.OrderBy(x => x.FechaDeNacimiento).ToList();
                    break;
                case "fechaDeNacimientodesc":
                    lista = lista.OrderByDescending(x => x.FechaDeNacimiento).ToList();
                    break;
                case "generoasc":
                    lista = lista.OrderBy(x => x.Genero).ToList();
                    break;
                case "generodesc":
                    lista = lista.OrderByDescending(x => x.Genero).ToList();
                    break;
                case "mbidasc":
                    lista = lista.OrderBy(x => x.Mbid).ToList();
                    break;
                case "mbiddesc":
                    lista = lista.OrderByDescending(x => x.Mbid).ToList();
                    break;
                case "nombreasc":
                    lista = lista.OrderBy(x => x.Nombre).ToList();
                    break;
                case "nombredesc":
                    lista = lista.OrderByDescending(x => x.Nombre).ToList();
                    break;
                case "ordenarNombreasc":
                    lista = lista.OrderBy(x => x.OrdenarNombre).ToList();
                    break;
                case "ordenarNombredesc":
                    lista = lista.OrderByDescending(x => x.OrdenarNombre).ToList();
                    break;
                case "paisasc":
                    lista = lista.OrderBy(x => x.Pais).ToList();
                    break;
                case "paisdesc":
                    lista = lista.OrderByDescending(x => x.Pais).ToList();
                    break;
                case "tipoasc":
                    lista = lista.OrderBy(x => x.Tipo).ToList();
                    break;
                case "tipodesc":
                    lista = lista.OrderByDescending(x => x.Tipo).ToList();
                    break;
            }

            lista = lista.Skip(tamanoDePagina * (pagina - 1)).Take(tamanoDePagina).ToList();
            return lista;
        }

        /// <summary>
        /// Método que recupera el total de elementos.
        /// </summary>
        /// <param name = "termino">Termino</param>
        /// <returns>int</returns>
        public static int ConsultaDeMusicaTotal(string termino)
        {
            int total = 0;
            MetaBrainz.MusicBrainz.Query query = new MetaBrainz.MusicBrainz.Query();
            Guid parsedGuid;
            if (Guid.TryParse(termino, out parsedGuid))
            {
                total = 1;
            }
            else
            {
                total = query.FindArtists(termino, 100, 0).Results.Count;
            }

            query.Close();
            return total;
        }

        /// <summary>
        /// Prueba de grupo
        /// </summary>
        /// <param name = "termino">Termino</param>
        /// <param name = "pagina">Página</param>
        /// <param name = "tamanoDePagina">Tamaño de Página</param>
        /// <param name = "ordenarPor">Ordenar por</param>
        /// <param name = "tipo">Tipo</param>
        /// <returns>List</returns>
        public static List<ConsumirMusicaEntidades.LanzamientoDeGrupo> PruebaDeGrupo(string termino, int pagina, int tamanoDePagina, string ordenarPor, string tipo)
        {
            if (pagina == 0)
            {
                pagina = 1;
            }

            if (tamanoDePagina == 0)
            {
                tamanoDePagina = Int32.MaxValue;
            }

            List<ConsumirMusicaEntidades.LanzamientoDeGrupo> lista = new List<ConsumirMusicaEntidades.LanzamientoDeGrupo>();
            MetaBrainz.MusicBrainz.Query query = new MetaBrainz.MusicBrainz.Query();
            Guid parsedGuid;
            if (Guid.TryParse(termino, out parsedGuid))
            {
                var elemento = query.LookupReleaseGroup(parsedGuid);
                ConsumirMusicaEntidades.LanzamientoDeGrupo lanzamientoDeGrupo = new ConsumirMusicaEntidades.LanzamientoDeGrupo();
                if (elemento.Rating != null)
                {
                    lanzamientoDeGrupo.Calificacion = elemento.Rating.Value;
                }

                lanzamientoDeGrupo.Desambiguacion = elemento.Disambiguation;
                lanzamientoDeGrupo.Mbid = elemento.Id;
                if (elemento.FirstReleaseDate != null)
                {
                    lanzamientoDeGrupo.PrimeraFechaDeLanzamiento = elemento.FirstReleaseDate.NearestDate;
                }

                if (elemento.Rating != null)
                {
                    lanzamientoDeGrupo.RecuentoDeVotos = elemento.Rating.VoteCount;
                }

                lanzamientoDeGrupo.TipoPrimario = elemento.PrimaryType;
                lanzamientoDeGrupo.Titulo = elemento.Title;
                lista.Add(lanzamientoDeGrupo);
            }
            else
            {
                var resultado = query.FindReleaseGroups(termino, tamanoDePagina > 100 ? 100 : tamanoDePagina, 0).Results;
                foreach (var lanzamientoDeGrupoElemento in resultado)
                {
                    var elemento = lanzamientoDeGrupoElemento.Item;
                    ConsumirMusicaEntidades.LanzamientoDeGrupo lanzamientoDeGrupo = new ConsumirMusicaEntidades.LanzamientoDeGrupo();
                    if (elemento.Rating != null)
                    {
                        lanzamientoDeGrupo.Calificacion = elemento.Rating.Value;
                    }

                    lanzamientoDeGrupo.Desambiguacion = elemento.Disambiguation;
                    lanzamientoDeGrupo.Mbid = elemento.Id;
                    if (elemento.FirstReleaseDate != null)
                    {
                        lanzamientoDeGrupo.PrimeraFechaDeLanzamiento = elemento.FirstReleaseDate.NearestDate;
                    }

                    if (elemento.Rating != null)
                    {
                        lanzamientoDeGrupo.RecuentoDeVotos = elemento.Rating.VoteCount;
                    }

                    lanzamientoDeGrupo.TipoPrimario = elemento.PrimaryType;
                    lanzamientoDeGrupo.Titulo = elemento.Title;
                    lista.Add(lanzamientoDeGrupo);
                }
            }

            query.Close();
            switch (ordenarPor + tipo)
            {
                case "calificacionasc":
                    lista = lista.OrderBy(x => x.Calificacion).ToList();
                    break;
                case "calificaciondesc":
                    lista = lista.OrderByDescending(x => x.Calificacion).ToList();
                    break;
                case "desambiguacionasc":
                    lista = lista.OrderBy(x => x.Desambiguacion).ToList();
                    break;
                case "desambiguaciondesc":
                    lista = lista.OrderByDescending(x => x.Desambiguacion).ToList();
                    break;
                case "mbidasc":
                    lista = lista.OrderBy(x => x.Mbid).ToList();
                    break;
                case "mbiddesc":
                    lista = lista.OrderByDescending(x => x.Mbid).ToList();
                    break;
                case "primeraFechaDeLanzamientoasc":
                    lista = lista.OrderBy(x => x.PrimeraFechaDeLanzamiento).ToList();
                    break;
                case "primeraFechaDeLanzamientodesc":
                    lista = lista.OrderByDescending(x => x.PrimeraFechaDeLanzamiento).ToList();
                    break;
                case "recuentoDeVotosasc":
                    lista = lista.OrderBy(x => x.RecuentoDeVotos).ToList();
                    break;
                case "recuentoDeVotosdesc":
                    lista = lista.OrderByDescending(x => x.RecuentoDeVotos).ToList();
                    break;
                case "tipoPrimarioasc":
                    lista = lista.OrderBy(x => x.TipoPrimario).ToList();
                    break;
                case "tipoPrimariodesc":
                    lista = lista.OrderByDescending(x => x.TipoPrimario).ToList();
                    break;
                case "tituloasc":
                    lista = lista.OrderBy(x => x.Titulo).ToList();
                    break;
                case "titulodesc":
                    lista = lista.OrderByDescending(x => x.Titulo).ToList();
                    break;
            }

            lista = lista.Skip(tamanoDePagina * (pagina - 1)).Take(tamanoDePagina).ToList();
            return lista;
        }

        /// <summary>
        /// Método que recupera el total de elementos.
        /// </summary>
        /// <param name = "termino">Termino</param>
        /// <returns>int</returns>
        public static int PruebaDeGrupoTotal(string termino)
        {
            int total = 0;
            MetaBrainz.MusicBrainz.Query query = new MetaBrainz.MusicBrainz.Query();
            Guid parsedGuid;
            if (Guid.TryParse(termino, out parsedGuid))
            {
                total = 1;
            }
            else
            {
                total = query.FindReleaseGroups(termino, 100, 0).Results.Count;
            }

            query.Close();
            return total;
        }

        /// <summary>
        /// Trabajo
        /// </summary>
        /// <param name = "termino">Termino</param>
        /// <param name = "pagina">Página</param>
        /// <param name = "tamanoDePagina">Tamaño de Página</param>
        /// <param name = "ordenarPor">Ordenar por</param>
        /// <param name = "tipo">Tipo</param>
        /// <returns>List</returns>
        public static List<ConsumirMusicaEntidades.Trabajo> PruebaDeTrabajo(string termino, int pagina, int tamanoDePagina, string ordenarPor, string tipo)
        {
            if (pagina == 0)
            {
                pagina = 1;
            }

            if (tamanoDePagina == 0)
            {
                tamanoDePagina = Int32.MaxValue;
            }

            List<ConsumirMusicaEntidades.Trabajo> lista = new List<ConsumirMusicaEntidades.Trabajo>();
            MetaBrainz.MusicBrainz.Query query = new MetaBrainz.MusicBrainz.Query();
            Guid parsedGuid;
            if (Guid.TryParse(termino, out parsedGuid))
            {
                var elemento = query.LookupWork(parsedGuid);
                ConsumirMusicaEntidades.Trabajo trabajo = new ConsumirMusicaEntidades.Trabajo();
                if (elemento.Rating != null)
                {
                    trabajo.Calificacion = elemento.Rating.Value;
                }

                trabajo.Desambiguacion = elemento.Disambiguation;
                trabajo.Idioma = elemento.Language;
                trabajo.Mbid = elemento.Id;
                if (elemento.Rating != null)
                {
                    trabajo.RecuentoDeVotos = elemento.Rating.VoteCount;
                }

                trabajo.Tipo = elemento.Type;
                trabajo.Titulo = elemento.Title;
                lista.Add(trabajo);
            }
            else
            {
                var resultado = query.FindWorks(termino, tamanoDePagina > 100 ? 100 : tamanoDePagina, 0).Results;
                foreach (var trabajoElemento in resultado)
                {
                    var elemento = trabajoElemento.Item;
                    ConsumirMusicaEntidades.Trabajo trabajo = new ConsumirMusicaEntidades.Trabajo();
                    if (elemento.Rating != null)
                    {
                        trabajo.Calificacion = elemento.Rating.Value;
                    }

                    trabajo.Desambiguacion = elemento.Disambiguation;
                    trabajo.Idioma = elemento.Language;
                    trabajo.Mbid = elemento.Id;
                    if (elemento.Rating != null)
                    {
                        trabajo.RecuentoDeVotos = elemento.Rating.VoteCount;
                    }

                    trabajo.Tipo = elemento.Type;
                    trabajo.Titulo = elemento.Title;
                    lista.Add(trabajo);
                }
            }

            query.Close();
            switch (ordenarPor + tipo)
            {
                case "calificacionasc":
                    lista = lista.OrderBy(x => x.Calificacion).ToList();
                    break;
                case "calificaciondesc":
                    lista = lista.OrderByDescending(x => x.Calificacion).ToList();
                    break;
                case "desambiguacionasc":
                    lista = lista.OrderBy(x => x.Desambiguacion).ToList();
                    break;
                case "desambiguaciondesc":
                    lista = lista.OrderByDescending(x => x.Desambiguacion).ToList();
                    break;
                case "idiomaasc":
                    lista = lista.OrderBy(x => x.Idioma).ToList();
                    break;
                case "idiomadesc":
                    lista = lista.OrderByDescending(x => x.Idioma).ToList();
                    break;
                case "mbidasc":
                    lista = lista.OrderBy(x => x.Mbid).ToList();
                    break;
                case "mbiddesc":
                    lista = lista.OrderByDescending(x => x.Mbid).ToList();
                    break;
                case "recuentoDeVotosasc":
                    lista = lista.OrderBy(x => x.RecuentoDeVotos).ToList();
                    break;
                case "recuentoDeVotosdesc":
                    lista = lista.OrderByDescending(x => x.RecuentoDeVotos).ToList();
                    break;
                case "tipoasc":
                    lista = lista.OrderBy(x => x.Tipo).ToList();
                    break;
                case "tipodesc":
                    lista = lista.OrderByDescending(x => x.Tipo).ToList();
                    break;
                case "tituloasc":
                    lista = lista.OrderBy(x => x.Titulo).ToList();
                    break;
                case "titulodesc":
                    lista = lista.OrderByDescending(x => x.Titulo).ToList();
                    break;
            }

            lista = lista.Skip(tamanoDePagina * (pagina - 1)).Take(tamanoDePagina).ToList();
            return lista;
        }

        /// <summary>
        /// Método que recupera el total de elementos.
        /// </summary>
        /// <param name = "termino">Termino</param>
        /// <returns>int</returns>
        public static int PruebaDeTrabajoTotal(string termino)
        {
            int total = 0;
            MetaBrainz.MusicBrainz.Query query = new MetaBrainz.MusicBrainz.Query();
            Guid parsedGuid;
            if (Guid.TryParse(termino, out parsedGuid))
            {
                total = 1;
            }
            else
            {
                total = query.FindWorks(termino, 100, 0).Results.Count;
            }

            query.Close();
            return total;
        }

        /// <summary>
        /// Recupera grabaciones
        /// </summary>
        /// <param name = "termino">Termino</param>
        /// <param name = "pagina">Página</param>
        /// <param name = "tamanoDePagina">Tamaño de Página</param>
        /// <param name = "ordenarPor">Ordenar por</param>
        /// <param name = "tipo">Tipo</param>
        /// <returns>List</returns>
        public static List<ConsumirMusicaEntidades.Grabacion> RecuperaGrabaciones(string termino, int pagina, int tamanoDePagina, string ordenarPor, string tipo)
        {
            if (pagina == 0)
            {
                pagina = 1;
            }

            if (tamanoDePagina == 0)
            {
                tamanoDePagina = Int32.MaxValue;
            }

            List<ConsumirMusicaEntidades.Grabacion> lista = new List<ConsumirMusicaEntidades.Grabacion>();
            MetaBrainz.MusicBrainz.Query query = new MetaBrainz.MusicBrainz.Query();
            Guid parsedGuid;
            if (Guid.TryParse(termino, out parsedGuid))
            {
                var elemento = query.LookupRecording(parsedGuid);
                ConsumirMusicaEntidades.Grabacion grabacion = new ConsumirMusicaEntidades.Grabacion();
                if (elemento.Rating != null)
                {
                    grabacion.Calificacion = elemento.Rating.Value;
                }

                grabacion.Desambiguacion = elemento.Disambiguation;
                if (elemento.Length.HasValue)
                {
                    grabacion.LargoMaximo = elemento.Length.Value.Ticks;
                }

                grabacion.Mbid = elemento.Id;
                if (elemento.FirstReleaseDate != null)
                {
                    grabacion.PrimeraFechaDeLanzamiento = elemento.FirstReleaseDate.NearestDate;
                }

                if (elemento.Rating != null)
                {
                    grabacion.RecuentoDeVotos = elemento.Rating.VoteCount;
                }

                grabacion.Titulo = elemento.Title;
                grabacion.Video = elemento.Video;
                lista.Add(grabacion);
            }
            else
            {
                var resultado = query.FindRecordings(termino, tamanoDePagina > 100 ? 100 : tamanoDePagina, 0).Results;
                foreach (var grabacionElemento in resultado)
                {
                    var elemento = grabacionElemento.Item;
                    ConsumirMusicaEntidades.Grabacion grabacion = new ConsumirMusicaEntidades.Grabacion();
                    if (elemento.Rating != null)
                    {
                        grabacion.Calificacion = elemento.Rating.Value;
                    }

                    grabacion.Desambiguacion = elemento.Disambiguation;
                    if (elemento.Length.HasValue)
                    {
                        grabacion.LargoMaximo = elemento.Length.Value.Ticks;
                    }

                    grabacion.Mbid = elemento.Id;
                    if (elemento.FirstReleaseDate != null)
                    {
                        grabacion.PrimeraFechaDeLanzamiento = elemento.FirstReleaseDate.NearestDate;
                    }

                    if (elemento.Rating != null)
                    {
                        grabacion.RecuentoDeVotos = elemento.Rating.VoteCount;
                    }

                    grabacion.Titulo = elemento.Title;
                    grabacion.Video = elemento.Video;
                    lista.Add(grabacion);
                }
            }

            query.Close();
            switch (ordenarPor + tipo)
            {
                case "calificacionasc":
                    lista = lista.OrderBy(x => x.Calificacion).ToList();
                    break;
                case "calificaciondesc":
                    lista = lista.OrderByDescending(x => x.Calificacion).ToList();
                    break;
                case "desambiguacionasc":
                    lista = lista.OrderBy(x => x.Desambiguacion).ToList();
                    break;
                case "desambiguaciondesc":
                    lista = lista.OrderByDescending(x => x.Desambiguacion).ToList();
                    break;
                case "largoMaximoasc":
                    lista = lista.OrderBy(x => x.LargoMaximo).ToList();
                    break;
                case "largoMaximodesc":
                    lista = lista.OrderByDescending(x => x.LargoMaximo).ToList();
                    break;
                case "mbidasc":
                    lista = lista.OrderBy(x => x.Mbid).ToList();
                    break;
                case "mbiddesc":
                    lista = lista.OrderByDescending(x => x.Mbid).ToList();
                    break;
                case "primeraFechaDeLanzamientoasc":
                    lista = lista.OrderBy(x => x.PrimeraFechaDeLanzamiento).ToList();
                    break;
                case "primeraFechaDeLanzamientodesc":
                    lista = lista.OrderByDescending(x => x.PrimeraFechaDeLanzamiento).ToList();
                    break;
                case "recuentoDeVotosasc":
                    lista = lista.OrderBy(x => x.RecuentoDeVotos).ToList();
                    break;
                case "recuentoDeVotosdesc":
                    lista = lista.OrderByDescending(x => x.RecuentoDeVotos).ToList();
                    break;
                case "tituloasc":
                    lista = lista.OrderBy(x => x.Titulo).ToList();
                    break;
                case "titulodesc":
                    lista = lista.OrderByDescending(x => x.Titulo).ToList();
                    break;
                case "videoasc":
                    lista = lista.OrderBy(x => x.Video).ToList();
                    break;
                case "videodesc":
                    lista = lista.OrderByDescending(x => x.Video).ToList();
                    break;
            }

            lista = lista.Skip(tamanoDePagina * (pagina - 1)).Take(tamanoDePagina).ToList();
            return lista;
        }

        /// <summary>
        /// Método que recupera el total de elementos.
        /// </summary>
        /// <param name = "termino">Termino</param>
        /// <returns>int</returns>
        public static int RecuperaGrabacionesTotal(string termino)
        {
            int total = 0;
            MetaBrainz.MusicBrainz.Query query = new MetaBrainz.MusicBrainz.Query();
            Guid parsedGuid;
            if (Guid.TryParse(termino, out parsedGuid))
            {
                total = 1;
            }
            else
            {
                total = query.FindRecordings(termino, 100, 0).Results.Count;
            }

            query.Close();
            return total;
        }
#endregion
    }
}