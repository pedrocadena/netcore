using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
namespace Codium.Web.Models.PruebaDeOracleRepositorios
{
    /// <summary>
    /// tb_menus
    /// </summary>
    public class TbMenus : ITbMenus
    {
#region Variables Privadas
        /// <summary>
        /// Contexto de Base de Datos
        /// </summary>
        private readonly System.Data.Common.DbConnection _conexion;
#endregion
#region Métodos
        /// <summary>
        /// Agregar TbMenus
        /// </summary>
        /// <param name = "tbMenus">Entidad a agregar</param>
        public void AgregarTbMenus(PruebaDeOracleEntidades.TbMenus tbMenus)
        {
            System.Data.Common.DbCommand comando = _conexion.CreateCommand();
            comando.CommandText = "INSERT INTO \"tb_menus\"(\"FL_SI_IDENTIFICADOR\",\"FL_SI_PADRE\",\"FL_ST_ICONO\",\"FL_ST_LIGA\",\"FL_ST_NOMBRE\",\"TEST\") VALUES(?,?,?,?,?,?)";
            System.Data.Common.DbParameter flSiIdentificador = comando.CreateParameter();
            flSiIdentificador.Value = tbMenus.FlSiIdentificador;
            comando.Parameters.Add(flSiIdentificador);
            System.Data.Common.DbParameter flSiPadre = comando.CreateParameter();
            flSiPadre.Value = tbMenus.FlSiPadre;
            comando.Parameters.Add(flSiPadre);
            System.Data.Common.DbParameter flStIcono = comando.CreateParameter();
            flStIcono.Value = tbMenus.FlStIcono;
            comando.Parameters.Add(flStIcono);
            System.Data.Common.DbParameter flStLiga = comando.CreateParameter();
            flStLiga.Value = tbMenus.FlStLiga;
            comando.Parameters.Add(flStLiga);
            System.Data.Common.DbParameter flStNombre = comando.CreateParameter();
            flStNombre.Value = tbMenus.FlStNombre;
            comando.Parameters.Add(flStNombre);
            System.Data.Common.DbParameter test = comando.CreateParameter();
            test.Value = tbMenus.Test;
            comando.Parameters.Add(test);
            _conexion.Open();
            comando.ExecuteNonQuery();
            _conexion.Close();
        }

        /// <summary>
        /// Recuperar TbMenus
        /// </summary>
        /// <param name = "id">Identificador</param>
        /// <returns>PruebaDeOracleEntidades.TbMenus</returns>
        public PruebaDeOracleEntidades.TbMenus RecuperarTbMenus(int id)
        {
            PruebaDeOracleEntidades.TbMenus tbMenus = new PruebaDeOracleEntidades.TbMenus();
            System.Data.Common.DbCommand comando = _conexion.CreateCommand();
            comando.CommandText = "SELECT * FROM \"tb_menus\" WHERE \"flSiIdentificador\" = ?";
            System.Data.Common.DbParameter parametro = comando.CreateParameter();
            parametro.Value = id;
            comando.Parameters.Add(parametro);
            _conexion.Open();
            System.Data.Common.DbDataReader lector = comando.ExecuteReader();
            while (lector.Read())
            {
                tbMenus.FlSiIdentificador = (int)(lector.IsDBNull(lector.GetOrdinal("FL_SI_IDENTIFICADOR")) ? null : lector.GetValue(lector.GetOrdinal("FL_SI_IDENTIFICADOR")));
                tbMenus.FlSiPadre = (int? )(lector.IsDBNull(lector.GetOrdinal("FL_SI_PADRE")) ? null : lector.GetValue(lector.GetOrdinal("FL_SI_PADRE")));
                tbMenus.FlStIcono = (string)(lector.IsDBNull(lector.GetOrdinal("FL_ST_ICONO")) ? null : lector.GetValue(lector.GetOrdinal("FL_ST_ICONO")));
                tbMenus.FlStLiga = (string)(lector.IsDBNull(lector.GetOrdinal("FL_ST_LIGA")) ? null : lector.GetValue(lector.GetOrdinal("FL_ST_LIGA")));
                tbMenus.FlStNombre = (string)(lector.IsDBNull(lector.GetOrdinal("FL_ST_NOMBRE")) ? null : lector.GetValue(lector.GetOrdinal("FL_ST_NOMBRE")));
                tbMenus.Test = (int? )(lector.IsDBNull(lector.GetOrdinal("TEST")) ? null : lector.GetValue(lector.GetOrdinal("TEST")));
            }

            lector.Close();
            _conexion.Close();
            return tbMenus;
        }

        /// <summary>
        /// Lista TbMenus
        /// </summary>
        /// <returns>IEnumerable</returns>
        public IEnumerable<PruebaDeOracleEntidades.TbMenus> ListaTbMenus()
        {
            List<PruebaDeOracleEntidades.TbMenus> resultado = new List<PruebaDeOracleEntidades.TbMenus>();
            System.Data.Common.DbCommand comando = _conexion.CreateCommand();
            comando.CommandText = "SELECT * FROM \"tb_menus\"";
            _conexion.Open();
            System.Data.Common.DbDataReader lector = comando.ExecuteReader();
            while (lector.Read())
            {
                PruebaDeOracleEntidades.TbMenus tbMenus = new PruebaDeOracleEntidades.TbMenus();
                tbMenus.FlSiIdentificador = (int)(lector.IsDBNull(lector.GetOrdinal("FL_SI_IDENTIFICADOR")) ? null : lector.GetValue(lector.GetOrdinal("FL_SI_IDENTIFICADOR")));
                tbMenus.FlSiPadre = (int? )(lector.IsDBNull(lector.GetOrdinal("FL_SI_PADRE")) ? null : lector.GetValue(lector.GetOrdinal("FL_SI_PADRE")));
                tbMenus.FlStIcono = (string)(lector.IsDBNull(lector.GetOrdinal("FL_ST_ICONO")) ? null : lector.GetValue(lector.GetOrdinal("FL_ST_ICONO")));
                tbMenus.FlStLiga = (string)(lector.IsDBNull(lector.GetOrdinal("FL_ST_LIGA")) ? null : lector.GetValue(lector.GetOrdinal("FL_ST_LIGA")));
                tbMenus.FlStNombre = (string)(lector.IsDBNull(lector.GetOrdinal("FL_ST_NOMBRE")) ? null : lector.GetValue(lector.GetOrdinal("FL_ST_NOMBRE")));
                tbMenus.Test = (int? )(lector.IsDBNull(lector.GetOrdinal("TEST")) ? null : lector.GetValue(lector.GetOrdinal("TEST")));
                resultado.Add(tbMenus);
            }

            lector.Close();
            _conexion.Close();
            return resultado;
        }

        /// <summary>
        /// Borrar TbMenus
        /// </summary>
        /// <param name = "id">Identificador</param>
        public bool BorrarTbMenus(int id)
        {
            System.Data.Common.DbCommand comando = _conexion.CreateCommand();
            comando.CommandText = "DELETE FROM \"tb_menus\" WHERE \"flSiIdentificador\" = ?";
            System.Data.Common.DbParameter parametro = comando.CreateParameter();
            parametro.Value = id;
            comando.Parameters.Add(parametro);
            _conexion.Open();
            comando.ExecuteNonQuery();
            _conexion.Close();
            return true;
        }

        /// <summary>
        /// Consulta de Oracle
        /// </summary>
        /// <param name = "pagina">Página</param>
        /// <param name = "tamanoDePagina">Tamaño de Página</param>
        /// <param name = "ordenarPor">Ordenar por</param>
        /// <param name = "tipo">Tipo</param>
        /// <returns>List</returns>
        public List<DTO.ConsultaDeOracle> ConsultaDeOracle(int pagina, int tamanoDePagina, string ordenarPor, string tipo)
        {
            if (pagina == 0)
            {
                pagina = 1;
            }

            if (tamanoDePagina == 0)
            {
                tamanoDePagina = Int32.MaxValue;
            }

            List<DTO.ConsultaDeOracle> resultado = new List<DTO.ConsultaDeOracle>();
            string consulta = "SELECT \"TbMenus\".\"FL_SI_IDENTIFICADOR\" AS \"flSiIdentificadorTbMenus\",\"TbMenus\".\"FL_ST_NOMBRE\" AS \"flStNombreTbMenus\",\"TbMenus\".\"FL_ST_LIGA\" AS \"flStLigaTbMenus\" FROM \"tb_menus\" as \"TbMenus\" WHERE \"TbMenus\".\"FL_SI_IDENTIFICADOR\" IS NOT NULL  AND \"TbMenus\".\"FL_SI_PADRE\" IS NULL  ORDER BY ";
            if (tipo == null || tipo == "")
            {
                tipo = "ASC";
            }

            switch (ordenarPor + tipo)
            {
                case "flSiIdentificadorTbMenusASC":
                    consulta += "\"TbMenus\".\"FL_SI_IDENTIFICADOR\" ASC";
                    break;
                case "flSiIdentificadorTbMenusDESC":
                    consulta += "\"TbMenus\".\"FL_SI_IDENTIFICADOR\" DESC";
                    break;
                case "flStNombreTbMenusASC":
                    consulta += "\"TbMenus\".\"FL_ST_NOMBRE\" ASC";
                    break;
                case "flStNombreTbMenusDESC":
                    consulta += "\"TbMenus\".\"FL_ST_NOMBRE\" DESC";
                    break;
                case "flStLigaTbMenusASC":
                    consulta += "\"TbMenus\".\"FL_ST_LIGA\" ASC";
                    break;
                case "flStLigaTbMenusDESC":
                    consulta += "\"TbMenus\".\"FL_ST_LIGA\" DESC";
                    break;
                default:
                    consulta += "\"TbMenus\".\"FL_SI_IDENTIFICADOR\" ASC";
                    break;
            }

            consulta += " LIMIT " + tamanoDePagina.ToString() + " OFFSET " + (tamanoDePagina * (pagina - 1)).ToString() + "";
            System.Data.Common.DbCommand comando = _conexion.CreateCommand();
            comando.CommandText = consulta;
            _conexion.Open();
            System.Data.Common.DbDataReader lector = comando.ExecuteReader();
            while (lector.Read())
            {
                DTO.ConsultaDeOracle consultaDeOracle = new DTO.ConsultaDeOracle();
                consultaDeOracle.FlSiIdentificadorTbMenus = (int)(lector.IsDBNull(lector.GetOrdinal("flSiIdentificadorTbMenus")) ? null : lector.GetValue(lector.GetOrdinal("flSiIdentificadorTbMenus")));
                consultaDeOracle.FlStNombreTbMenus = (string)(lector.IsDBNull(lector.GetOrdinal("flStNombreTbMenus")) ? null : lector.GetValue(lector.GetOrdinal("flStNombreTbMenus")));
                consultaDeOracle.FlStLigaTbMenus = (string)(lector.IsDBNull(lector.GetOrdinal("flStLigaTbMenus")) ? null : lector.GetValue(lector.GetOrdinal("flStLigaTbMenus")));
                resultado.Add(consultaDeOracle);
            }

            lector.Close();
            _conexion.Close();
            return resultado;
        }

        /// <summary>
        /// Método que recupera el total de elementos.
        /// </summary>
        /// <returns>int</returns>
        public long ConsultaDeOracleTotal()
        {
            long resultado;
            string consulta = "SELECT COUNT(1) FROM (SELECT \"TbMenus\".\"FL_SI_IDENTIFICADOR\" AS \"flSiIdentificadorTbMenus\",\"TbMenus\".\"FL_ST_NOMBRE\" AS \"flStNombreTbMenus\",\"TbMenus\".\"FL_ST_LIGA\" AS \"flStLigaTbMenus\" FROM \"tb_menus\" as \"TbMenus\" WHERE \"TbMenus\".\"FL_SI_IDENTIFICADOR\" IS NOT NULL  AND \"TbMenus\".\"FL_SI_PADRE\" IS NULL ) RESULT";
            System.Data.Common.DbCommand comando = _conexion.CreateCommand();
            comando.CommandText = consulta;
            _conexion.Open();
            resultado = (long)comando.ExecuteScalar();
            _conexion.Close();
            return resultado;
        }

#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        public TbMenus(System.Data.Common.DbConnection conexion)
        {
            this._conexion = conexion;
        }
#endregion
    }
}