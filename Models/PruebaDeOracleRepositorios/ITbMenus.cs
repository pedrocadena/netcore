using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Codium.Web.Models.PruebaDeOracleRepositorios
{
    /// <summary>
    /// tb_menus
    /// </summary>
    public interface ITbMenus
    {
#region Métodos
        /// <summary>
        /// Agregar TbMenus
        /// </summary>
        /// <param name = "tbMenus">Entidad a agregar</param>
        void AgregarTbMenus(PruebaDeOracleEntidades.TbMenus tbMenus);
        /// <summary>
        /// Recuperar TbMenus
        /// </summary>
        /// <param name = "id">Identificador</param>
        /// <returns>PruebaDeOracleEntidades.TbMenus</returns>
        PruebaDeOracleEntidades.TbMenus RecuperarTbMenus(int id);
        /// <summary>
        /// Lista TbMenus
        /// </summary>
        /// <returns>IEnumerable</returns>
        IEnumerable<PruebaDeOracleEntidades.TbMenus> ListaTbMenus();
        /// <summary>
        /// Borrar TbMenus
        /// </summary>
        /// <param name = "id">Identificador</param>
        bool BorrarTbMenus(int id);
        /// <summary>
        /// Consulta de Oracle
        /// </summary>
        /// <param name = "pagina">Página</param>
        /// <param name = "tamanoDePagina">Tamaño de Página</param>
        /// <param name = "ordenarPor">Ordenar por</param>
        /// <param name = "tipo">Tipo</param>
        /// <returns>List</returns>
        List<DTO.ConsultaDeOracle> ConsultaDeOracle(int pagina, int tamanoDePagina, string ordenarPor, string tipo);
        /// <summary>
        /// Método que recupera el total de elementos.
        /// </summary>
        /// <returns>int</returns>
        long ConsultaDeOracleTotal();
#endregion
    }
}