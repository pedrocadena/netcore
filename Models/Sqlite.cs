using Microsoft.EntityFrameworkCore;
using System;

namespace Codium.Web.Models
{
    /// <summary>
    /// SQLite
    /// </summary>
    public class Sqlite : DbContext
    {
#region Conjuntos
        /// <summary>
        /// Test
        /// </summary>
        public DbSet<SqliteEntidades.Test> Testes { get; set; }

#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name = "options">Valores para inicializar el contexto de base de datos</param>
        public Sqlite(DbContextOptions<Sqlite> options): base(options)
        {
        }

#endregion
#region Métodos
        /// <summary>
        /// Función de creación de módelo
        /// </summary>  
        /// <param name = "modelBuilder">Constructor de módelo</param>                
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
#endregion
    }
}