using System;

namespace Codium.Web.Models
{
    /// <summary>
    /// Email ShopiliciousRegular
    /// </summary>
    public class Email10039ShopiliciousRegularViewModel
    {
        /// <summary>
        /// Identificador de cuatro letras donde el lenguaje cumple con el ISO 639-1 y el país con ISO 3166-1.
        /// </summary>
        public string LanguageIdentifier { get; set; }

        /// <summary>
        /// Dominio
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Zona PreHeader
        /// </summary>
        public string Preheader { get; set; }

        /// <summary>
        /// Zona Header
        /// </summary>
        public string Header { get; set; }

        /// <summary>
        /// Zona Products
        /// </summary>
        public string Products { get; set; }
    }
}