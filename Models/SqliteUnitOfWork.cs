using Microsoft.EntityFrameworkCore;

using System;
using System.Threading.Tasks;
namespace Codium.Web.Models
{
    /// <summary>
    /// SQLite
    /// </summary>
    public class SqliteUnitOfWork : ISqliteUnitOfWork
    {
#region Variables Privadas
        /// <summary>
        /// Contexto de Base de Datos
        /// </summary>
        private readonly Sqlite _sqlite;
#endregion
#region Propiedades Públicas
#endregion
#region Constructores
        /// <summary>
        /// Constructor
        /// </summary>
        public SqliteUnitOfWork(Sqlite sqlite)
        {
            this._sqlite = sqlite;
        }

#endregion
#region Métodos
        /// <summary>
        /// Guardar Asíncrono
        /// </summary>
        /// <returns>int</returns>
        public async Task<int> GuardarAsync()
        {
            return await _sqlite.SaveChangesAsync();
        }

        /// <summary>
        /// Guardar
        /// </summary>
        /// <returns>int</returns>
        public int Guardar()
        {
            return _sqlite.SaveChanges();
        }

        /// <summary>
        /// Dispose
        /// </summary>
        public void Dispose() => _sqlite.Dispose();
#endregion
    }
}